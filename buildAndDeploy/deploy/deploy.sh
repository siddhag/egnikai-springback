#!/usr/bin/env bash

if [ "$#" -ne 5 ]; then
    echo "usage sh deploy.sh <aws cli profile name> <securityKeyName> <db password> <environment> <aws default region>"
    exit 1
fi

profileName=$1
securityKeyName=$2
dbPassword=$3
environment=$4
region=$5
commitHashOfDeployable=$(git rev-parse --short HEAD)

function retagAndPushBuiltImage() {
    accountId=$(aws sts get-caller-identity --query "Account" --profile ${profileName} --output text)
    commitHash=$(git rev-parse --short HEAD)
    $(aws ecr get-login --no-include-email --region ${region} --profile ${profileName})
    docker tag egnikai-spring-back:${commitHash} \
        ${accountId}.dkr.ecr.${region}.amazonaws.com/egnikai-spring-back:${environment}-${commitHash}
    docker push ${accountId}.dkr.ecr.${region}.amazonaws.com/egnikai-spring-back:${environment}-${commitHash}
}

function createNonProdMySQLStack() {
    cfn-create-or-update \
      --stack-name egnikai-non-prod-mysql-db \
      --template-body file://buildAndDeploy/deploy/mysql-ec2-nonprod.yml \
      --parameters ParameterKey=securityKeyName,ParameterValue=${securityKeyName} \
                   ParameterKey=dbPassword,ParameterValue=${dbPassword} \
      --region ${region} \
      --profile ${profileName} \
      --wait || exit 1
}

function createProdMySQLStack() {
    cfn-create-or-update \
      --stack-name egnikai-prod-mysql-db \
      --template-body file://buildAndDeploy/deploy/mysql-ec2-prod.yml \
      --parameters ParameterKey=securityKeyName,ParameterValue=${securityKeyName} \
                   ParameterKey=dbPassword,ParameterValue=${dbPassword} \
      --region ${region} \
      --profile ${profileName} \
      --wait || exit 1
}

function createElasticIPAndSecurityGroup() {
    cfn-create-or-update \
      --stack-name egnikai-springback-common \
      --template-body file://buildAndDeploy/deploy/api-ec2-common.yml \
      --profile ${profileName} \
      --region ${region} \
      --wait || exit 1
}

function deleteOldAPIDeploymentStackIfExists() {
    aws cloudformation describe-stacks \
        --stack-name egnikai-springback-deployment-${environment} \
        --profile ${profileName} \
        --region ${region}
    doesStackExist=$?
    if [ "$doesStackExist" -eq 0 ]; then
      echo "stack exists going to delete it now"
      aws cloudformation delete-stack \
        --stack-name egnikai-springback-deployment-${environment} \
        --profile ${profileName} \
        --region ${region} || exit 1
      aws cloudformation wait stack-delete-complete \
        --stack-name egnikai-springback-deployment-${environment} \
        --profile ${profileName} \
        --region ${region} || exit 1
    else
      echo "stack does not exist yet"
    fi
}

#Use base64 -w0 buildAndDeploy/deploy/user-data.sh if running in linux system
function createNewAPIDeploymentStack() {
    cfn-create-or-update \
      --stack-name egnikai-springback-deployment-${environment} \
      --template-body file://buildAndDeploy/deploy/api-ec2.yml \
      --parameters ParameterKey=securityKeyName,ParameterValue=${securityKeyName} \
                   ParameterKey=commitHashOfDeployable,ParameterValue=${commitHashOfDeployable} \
                   ParameterKey=dbPassword,ParameterValue=${dbPassword} \
                   ParameterKey=environment,ParameterValue=${environment} \
      --capabilities CAPABILITY_NAMED_IAM \
      --region ${region} \
      --profile ${profileName} \
      --wait || exit 1
}

retagAndPushBuiltImage
if [[ "$environment" == "dev" || "$environment" == "qa" ]]; then
  echo "creating non prod mysql stack"
  createNonProdMySQLStack
else
  echo "creating prod mysql stack"
  createProdMySQLStack
fi
createElasticIPAndSecurityGroup
deleteOldAPIDeploymentStackIfExists
createNewAPIDeploymentStack