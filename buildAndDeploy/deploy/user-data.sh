#!/bin/bash -ex

if [ "$#" -ne 3 ]; then
    echo "usage sh deploy.sh <DB Password> <environment> <Commit hash>"
    exit 1
fi

${dbPassword}=$1
${environment}=$2
${commitHashOfDeployable}=$3

yum update -y
yum install -y jq
yum install -y docker
usermod -a -G docker ec2-user
curl -L https://github.com/docker/compose/releases/download/1.20.1/docker-compose-`uname -s`-`uname -m` | sudo tee /usr/local/bin/docker-compose > /dev/null
chmod +x /usr/local/bin/docker-compose
service docker start
chkconfig docker on
su - ec2-user
cd /home/ec2-user
mkdir -p app
accountId=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document/ | jq -r ".accountId")
region=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document/ | jq -r ".region")
cd app
cat > docker-compose.yml <<EOL
---
version: '2.1'
services:
db:
  image: regsethu/egnikaidb-image:latest
  ports:
  - "3306"
  environment:
    - MYSQL_ROOT_PASSWORD=${dbPassword}
  healthcheck:
    test: ["CMD", "mysqladmin" ,"ping", "-h", "localhost"]
    timeout: 20s
    retries: 10

spring-boot:
  image: ${accountId}.dkr.ecr.${region}.amazonaws.com/egnikai-spring-back:${environment}-${commitHashOfDeployable}
  ports:
    - "8080:8080"
  environment:
    - spring.profiles.active=${environment}
  depends_on:
    db:
      condition: service_healthy
EOL

/usr/local/bin/docker-compose up -d
