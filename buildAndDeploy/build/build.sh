#!/usr/bin/env bash
set -e

if [ "$#" -ne 2 ]; then
    echo "usage sh build.sh <aws cli profile name> <aws region name>"
    exit 1
fi

profileName=$1
region=$2

cfn-create-or-update \
  --stack-name ecr-spring-back-repo \
  --template-body file://buildAndDeploy/build/ecr.yml \
  --region ${region} \
  --profile ${profileName} \
  --wait

mvn clean package org.owasp:dependency-check-maven:check

commitHash=$(git rev-parse --short HEAD)
docker build -t egnikai-spring-back:${commitHash} .

