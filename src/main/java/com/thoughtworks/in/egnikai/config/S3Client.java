package com.thoughtworks.in.egnikai.config;


import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class S3Client {

    private static final Logger logger = LoggerFactory.getLogger(S3Client.class);

    @Value("${spring-back-user-access-key:''}")
    private String springBackUserAccessKey;

    @Value("${spring-back-user-access-secret-key:''}")
    private String springBackUserAccessSecretKey;

    @Value("${default-aws-region}")
    private String awsRegion;

    @Autowired
    private AWSCredentialsProvider credentialsProvider;

    @Bean
    public AmazonS3 amazonS3Client() {
        logger.debug("Type of credentials Provider {}", credentialsProvider.toString());
        return AmazonS3ClientBuilder
                .standard()
                .withCredentials(credentialsProvider)
                .withRegion(Regions.US_EAST_1)
                .build();
    }

    @Bean
    @Profile("!local")
    public AWSCredentialsProvider getAwsInstanceCredentialsProvider() {
        return InstanceProfileCredentialsProvider.createAsyncRefreshingProvider(true);
    }

    @Bean
    @Profile("local")
    public AWSCredentialsProvider getStaticAwsCredentials() {
        return new AWSStaticCredentialsProvider(
                new BasicAWSCredentials(springBackUserAccessKey, springBackUserAccessSecretKey));
    }

}
