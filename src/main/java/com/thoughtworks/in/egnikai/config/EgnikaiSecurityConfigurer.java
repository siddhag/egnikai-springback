package com.thoughtworks.in.egnikai.config;


import com.thoughtworks.in.egnikai.filter.CheckUserBlockedFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class EgnikaiSecurityConfigurer extends WebSecurityConfigurerAdapter {
    @Autowired
    DataSource dataSource;

    @Autowired
    AuthenticationEntryPoint authEntryPoint;

    @Autowired
    CheckUserBlockedFilter checkUserBlockedFilter;

    private final String USERS_QUERY = "select user_id, password, enabled from user where user_id=?";
    private final String ROLES_QUERY = "select user_id, role from user where user_id=?";

    // Authorization
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.addFilterAfter(checkUserBlockedFilter, BasicAuthenticationFilter.class)
                .httpBasic().authenticationEntryPoint(authEntryPoint).and().authorizeRequests()
                .anyRequest().fullyAuthenticated().and()
                .csrf().disable().headers().frameOptions().disable();
    }

    // Authentication
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery(USERS_QUERY)
                .authoritiesByUsernameQuery(ROLES_QUERY);
    }
}
