package com.thoughtworks.in.egnikai.error;

public class ErrorConstants {
    static final String HTTP_CODE_404 = "404";
    static final String ERROR_CODE_404 = "RESOURCE_NOT_FOUND";
    static final String HTTP_CODE_403 = "403";
    static final String ERROR_CODE_403 = "FORBIDDEN";
    static final String HTTP_CODE_524 = "524";
    static final String ERROR_CODE_524 = "TIMEOUT";
}
