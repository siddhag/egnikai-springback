package com.thoughtworks.in.egnikai.error;

import java.util.Arrays;
import java.util.List;

import static com.thoughtworks.in.egnikai.error.ErrorConstants.*;

public class Errors {

    List<ApplicationError> errors;

    private Errors(List<ApplicationError> errors) {
        this.errors = errors;
    }

    public List<ApplicationError> getErrors() {
        return errors;
    }

    public void setErrors(List<ApplicationError> errors) {
        this.errors = errors;
    }

    public static Errors unAuthorizedAccess() {
        return new Errors(Arrays.asList(new ApplicationError(HTTP_CODE_403, ERROR_CODE_403)));
    }

    public static Errors resourceNotFoundException() {
        return new Errors(Arrays.asList(new ApplicationError(HTTP_CODE_404, ERROR_CODE_404)));
    }

    public static Errors timeOutException() {
        return new Errors(Arrays.asList(new ApplicationError(HTTP_CODE_524, ERROR_CODE_524)));
    }

}
