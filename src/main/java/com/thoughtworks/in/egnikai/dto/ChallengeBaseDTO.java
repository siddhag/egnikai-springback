package com.thoughtworks.in.egnikai.dto;

import com.thoughtworks.in.egnikai.persistence.model.Challenge;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

@ToString
@EqualsAndHashCode
public class ChallengeBaseDTO {
    @Getter
    @Setter
    private long challengeId;

    @Getter
    @Setter
    private String challengeTitle;

    public static List<ChallengeBaseDTO> fromChallenge(Iterable<Challenge> challenges) {
        List<ChallengeBaseDTO> challengeDetailDTOs = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        for (Challenge challenge : challenges) {
            challengeDetailDTOs.add(modelMapper.map(challenge, ChallengeBaseDTO.class));
        }
        return challengeDetailDTOs;
    }
}
