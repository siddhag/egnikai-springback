package com.thoughtworks.in.egnikai.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@EqualsAndHashCode
public class CloningMessageDTO {
    @Getter
    @Setter
    private String gitURL;

    @Getter
    @Setter
    private long candidateId;

    @Getter
    @Setter
    private long operationId;
}
