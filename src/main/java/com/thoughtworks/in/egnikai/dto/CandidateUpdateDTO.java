package com.thoughtworks.in.egnikai.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Required;

import javax.validation.constraints.NotNull;
import java.util.List;

public class CandidateUpdateDTO {

    @Getter
    @Setter
    private List<Long> candidateIds;

    @Getter
    @Setter
    @NotNull
    private Boolean isLocked;

    @Getter
    @Setter
    @NotNull
    private Boolean isActive;
}
