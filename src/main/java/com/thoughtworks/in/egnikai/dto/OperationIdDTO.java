package com.thoughtworks.in.egnikai.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode
public class OperationIdDTO {

    @Getter
    @Setter
    private long operationId;

    public static OperationIdDTO createDTO(long operationId) {
        OperationIdDTO operationIdDTO = new OperationIdDTO();
        operationIdDTO.setOperationId(operationId);
        return operationIdDTO;
    }
}
