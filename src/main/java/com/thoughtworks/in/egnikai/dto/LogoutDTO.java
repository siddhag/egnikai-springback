package com.thoughtworks.in.egnikai.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode
@ToString
public class LogoutDTO {

    @Getter
    @Setter
    private boolean shouldCandidateBeLocked;
}
