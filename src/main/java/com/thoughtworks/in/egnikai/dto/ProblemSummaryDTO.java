package com.thoughtworks.in.egnikai.dto;

import com.thoughtworks.in.egnikai.persistence.model.Problem;
import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProblemSummaryDTO {

    @Getter
    @Setter
    private long problemId;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private boolean optional;

    @Getter
    @Setter
    private Integer credit;

    public static List<ProblemSummaryDTO> fromProblem(List<Problem> problems, boolean isCreditVisible) {
        ModelMapper modelMapper = new ModelMapper();

        List<ProblemSummaryDTO> problemSummaryDTOS = problems.stream().map(problem -> {
            ProblemSummaryDTO problemSummaryDTO = new ProblemSummaryDTO();
            mapProblemToDTO(isCreditVisible, problem, problemSummaryDTO);

            return problemSummaryDTO;
        }).collect(Collectors.toList());
        return problemSummaryDTOS;
    }

    private static void mapProblemToDTO(boolean isCreditVisible, Problem problem, ProblemSummaryDTO problemSummaryDTO) {
        problemSummaryDTO.setProblemId(problem.getProblemId());
        problemSummaryDTO.setTitle(problem.getTitle());
        problemSummaryDTO.setDescription(problem.getDescription());
        problemSummaryDTO.setOptional(problem.isOptional());
        if(isCreditVisible)
        {
            problemSummaryDTO.setCredit(problem.getCredit());
        }
    }

}
