package com.thoughtworks.in.egnikai.dto;

import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.util.GitUtil;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.jgit.util.StringUtils;
import org.modelmapper.ModelMapper;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

@EqualsAndHashCode
public class CandidateDTO implements Serializable {

    @Getter
    @Setter
    private long candidateId;

    @Getter
    @Setter
    private String eventId;


    @Getter
    @Setter
    private String candidateName;

    @Setter
    private boolean isLocked;

    public boolean getIsLocked() {
        return isLocked;
    }

    @Setter
    private boolean isActive;

    public boolean getIsActive() {
        return isActive;
    }

    @Getter
    @Setter
    private int score;

    @Getter
    @Setter
    private String createdAt;

    @Getter
    @Setter
    private String candidateGitBranch;

    @Getter
    @Setter
    protected String userId;

    @Getter
    @Setter
    protected String password;


    @Getter
    @Setter
    private String challengeTitle;

    public static List<CandidateDTO> fromCandidates(List<Candidate> candidates) {
        ModelMapper modelMapper = new ModelMapper();

        List<CandidateDTO> candidateDTOList = candidates.stream().map(candidate -> {
            CandidateDTO candidateDTO = modelMapper.map(candidate, CandidateDTO.class);
            candidateDTO.setCandidateName(candidate.getUser().getUserName());
            candidateDTO.setPassword(candidate.getUser().getPassword());

            if (null != candidate.getChallenge() && !StringUtils.isEmptyOrNull(candidate.getCandidateGitBranch())) {
                candidateDTO.setCandidateGitBranch(GitUtil.getHTTPSBranchURL(candidate.getCandidateGitBranch(),
                        candidate.getChallenge().getChallengeGitURL()));
            }
            return candidateDTO;
        }).collect(Collectors.toList());
        return candidateDTOList;
    }
}
