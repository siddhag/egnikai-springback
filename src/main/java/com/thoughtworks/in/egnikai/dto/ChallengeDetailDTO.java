package com.thoughtworks.in.egnikai.dto;

import com.thoughtworks.in.egnikai.persistence.model.Challenge;
import com.thoughtworks.in.egnikai.persistence.model.Problem;
import com.thoughtworks.in.egnikai.util.GitUtil;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.jgit.util.StringUtils;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ChallengeDetailDTO {

    @Getter
    @Setter
    private long challengeId;

    @Getter
    @Setter
    private String challengeTitle;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private String platformName;

    @Getter
    @Setter
    private String challengeGitURL;

    @Getter
    @Setter
    private List<Problem> problems;

    public static List<ChallengeDetailDTO> fromChallenge(List<Challenge> challenges) {
        ModelMapper modelMapper = new ModelMapper();

        List<ChallengeDetailDTO> challengeDetailDTOs = challenges.stream().map(challenge -> {
            ChallengeDetailDTO challengeDetailDTO = modelMapper.map(challenge, ChallengeDetailDTO.class);

            return challengeDetailDTO;
        }).collect(Collectors.toList());
        return challengeDetailDTOs;
    }
}
