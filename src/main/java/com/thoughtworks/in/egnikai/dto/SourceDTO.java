package com.thoughtworks.in.egnikai.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;

@EqualsAndHashCode
public class SourceDTO {
    @Getter
    @Setter
    @Id
    protected long sourceId;

    @Getter
    @Setter
    private String sourceName;

    @Getter
    @Setter
    private String sourceCode;

    @Getter
    @Setter
    private boolean isEditable;
}
