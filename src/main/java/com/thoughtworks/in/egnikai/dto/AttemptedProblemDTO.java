package com.thoughtworks.in.egnikai.dto;

import com.thoughtworks.in.egnikai.persistence.model.AttemptedProblem;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;

@EqualsAndHashCode
public class AttemptedProblemDTO {
    @Getter
    @Setter
    private long attemptedProblemId;

    @Getter
    @Setter
    private long associatedProblemId;

    @Getter
    @Setter
    private boolean isPassed;

    public static AttemptedProblemDTO createAttemptedProblemDTO(AttemptedProblem attemptedProblem) {
        ModelMapper mapper = new ModelMapper();
        AttemptedProblemDTO attemptedProblemDTO = mapper.map(attemptedProblem, AttemptedProblemDTO.class);
        attemptedProblemDTO.associatedProblemId = attemptedProblem.getProblem().getProblemId();
        return attemptedProblemDTO;
    }
}