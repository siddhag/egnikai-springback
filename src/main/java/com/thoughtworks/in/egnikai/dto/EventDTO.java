package com.thoughtworks.in.egnikai.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
public class EventDTO {
    @Getter
    @Setter
    String eventId;

    @Getter
    @Setter
    List<CandidateChallengeDTO> candidateChallengeDTOS;

}
