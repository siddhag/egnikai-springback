package com.thoughtworks.in.egnikai.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Id;
import java.util.List;

@EqualsAndHashCode
@ToString
public class ProblemDetailDTO {
    @Getter
    @Setter
    @Id
    private long problemId;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private Integer credit;

    @Getter
    @Setter
    private List<TestDTO> testDTOs;

    @Getter
    @Setter
    private List<SourceDTO> sourceDTOs;
}
