package com.thoughtworks.in.egnikai.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Id;

@EqualsAndHashCode
@ToString
public class TestDTO {
    @Getter
    @Setter
    @Id
    private long testId;

    @Getter
    @Setter
    private String testFileName;

    @Getter
    @Setter
    private String testCode;
}
