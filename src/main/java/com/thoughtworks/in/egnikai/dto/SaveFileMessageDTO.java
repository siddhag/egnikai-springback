package com.thoughtworks.in.egnikai.dto;

import lombok.Getter;
import lombok.Setter;

public class SaveFileMessageDTO {

    @Getter
    @Setter
    private long candidateId;

    @Getter
    @Setter
    private String filePath;

    @Getter
    @Setter
    private String tempFilePath;

    public static SaveFileMessageDTO createDTO(long candidateId) {
        SaveFileMessageDTO saveFileMessageDTO = new SaveFileMessageDTO();
        saveFileMessageDTO.setCandidateId(candidateId);
        return saveFileMessageDTO;
    }

    @Override
    public String toString() {
        return "SaveFileMessageDTO{" +
                "candidateId=" + candidateId +
                '}';
    }
}
