package com.thoughtworks.in.egnikai.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class CandidateChallengeDTO {
    @Getter
    @Setter
    private String candidateName;

    @Getter
    @Setter
    private Long challengeId;
}
