package com.thoughtworks.in.egnikai.dto;

import lombok.Getter;
import lombok.Setter;

public class RunProblemDTO {

    @Getter
    @Setter
    private long candidateId;

    @Getter
    @Setter
    private long problemId;

    @Getter
    @Setter
    private String platform;

    @Getter
    @Setter
    private long operationId;


    public static RunProblemDTO createDTO(long candidateId, long problemId,long operationId, String platform) {

        RunProblemDTO runProblemDTO = new RunProblemDTO();
        runProblemDTO.setCandidateId(candidateId);
        runProblemDTO.setProblemId(problemId);
        runProblemDTO.setPlatform(platform);
        runProblemDTO.setOperationId(operationId);
        return runProblemDTO;
    }
}


