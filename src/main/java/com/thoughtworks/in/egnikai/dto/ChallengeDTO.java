package com.thoughtworks.in.egnikai.dto;

import com.thoughtworks.in.egnikai.persistence.model.Challenge;
import com.thoughtworks.in.egnikai.persistence.model.Problem;
import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;

import java.util.List;

public class ChallengeDTO {

    @Getter
    @Setter
    private String challengeTitle;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private String platformName;

    @Getter
    @Setter
    private List<ProblemSummaryDTO> problemSummaryDTOs;


    public static ChallengeDTO fromChallenge(Challenge challenge, boolean isCreditVisible) {
        ModelMapper modelMapper = new ModelMapper();
        ChallengeDTO challengeDTO = modelMapper.map(challenge, ChallengeDTO.class);
        challengeDTO.setProblemSummaryDTOs(ProblemSummaryDTO.fromProblem(challenge.getProblems(), isCreditVisible));
        return challengeDTO;
    }

}
