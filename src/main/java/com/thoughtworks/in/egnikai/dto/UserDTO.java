package com.thoughtworks.in.egnikai.dto;

import com.thoughtworks.in.egnikai.persistence.model.User;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class UserDTO implements Serializable {

    @Getter
    @Setter
    private String userid;

    @Getter
    @Setter
    private String username;

    public static UserDTO fromUser(User user) {
        UserDTO dto = new UserDTO();
        dto.userid = user.getUserId();
        dto.username = user.getUserName();
        return dto;
    }
}
