package com.thoughtworks.in.egnikai.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@EqualsAndHashCode
public class GenericResponseDTO {

    @Getter
    @Setter
    private HttpStatus httpStatus;

    @Getter
    @Setter
    private String message;

}
