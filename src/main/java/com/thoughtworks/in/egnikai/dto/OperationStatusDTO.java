package com.thoughtworks.in.egnikai.dto;

import com.thoughtworks.in.egnikai.persistence.model.Operation;
import com.thoughtworks.in.egnikai.persistence.model.OperationName;
import com.thoughtworks.in.egnikai.persistence.model.OperationStatus;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;

@EqualsAndHashCode
public class OperationStatusDTO {
    @Getter
    @Setter
    private long operationId;

    @Getter
    @Setter
    private String operationName;

    @Getter
    @Setter
    private String operationStatus;

    @Setter
    @Getter
    private String errorInformation;

    public static OperationStatusDTO createOperationStatusDTO(Operation operation) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(operation, OperationStatusDTO.class);
    }
}