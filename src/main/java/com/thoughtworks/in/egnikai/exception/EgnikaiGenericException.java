package com.thoughtworks.in.egnikai.exception;

public class EgnikaiGenericException extends RuntimeException {
    public EgnikaiGenericException(Exception cause) {
        super(cause);
    }
}
