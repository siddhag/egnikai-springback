package com.thoughtworks.in.egnikai.exception;

public class TimeOutException extends Exception {
    public TimeOutException(String msg) {
        super(msg);
    }
}
