package com.thoughtworks.in.egnikai.exception;

import com.thoughtworks.in.egnikai.error.Errors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(ForbiddenResourceException.class)
    public ResponseEntity<Errors> handleForbiddenResourceException(Exception exception) {
        logger.error("Forbidden resource" + exception.getMessage(), exception);
        return new ResponseEntity<>(Errors.unAuthorizedAccess(), HttpStatus.UNAUTHORIZED);
    }


    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Errors> handleResourceNotFoundException(Exception exception) {
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(Errors.resourceNotFoundException(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(EgnikaiGenericException.class)
    public ResponseEntity<Errors> handleEgnikaiGenericException(Exception exception) {
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(Errors.resourceNotFoundException(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(TimeOutException.class)
    public ResponseEntity<Errors> handleTimeOutException(Exception exception) {
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(Errors.resourceNotFoundException(), HttpStatus.NOT_FOUND);
    }
}
