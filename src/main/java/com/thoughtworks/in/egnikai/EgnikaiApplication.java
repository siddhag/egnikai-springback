package com.thoughtworks.in.egnikai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EgnikaiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EgnikaiApplication.class, args);
	}
}
