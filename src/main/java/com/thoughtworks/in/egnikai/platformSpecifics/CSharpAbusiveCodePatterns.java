package com.thoughtworks.in.egnikai.platformSpecifics;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class CSharpAbusiveCodePatterns implements AbusiveCodePatterns {

    @Override
    public List<Pattern> getAbusiveCodePatterns() {
        List<Pattern> patterns = new ArrayList<>();
        patterns.add(Pattern.compile(".*new Process\\(.*\\).*"));
        patterns.add(Pattern.compile(".*System\\.Threading.*"));
        patterns.add(Pattern.compile(".*Process\\.Start\\..*"));
        patterns.add(Pattern.compile(".*File\\..*"));
        patterns.add(Pattern.compile(".*FileStream.*"));
        patterns.add(Pattern.compile(".*FileInfo\\..*"));
        patterns.add(Pattern.compile(".*System\\.IO.*"));
        patterns.add(Pattern.compile(".*using System\\.Web.*"));
        patterns.add(Pattern.compile(".*using System\\.Net.*"));
        patterns.add(Pattern.compile(".*using System\\.Linq.*"));
        patterns.add(Pattern.compile(".*System\\.Environment\\.Exit.*"));

        return patterns;
    }

    @Override
    public String getSingleLineCommentPattern() {
        return "[\\s]*//.*";
    }

    @Override
    public String getMultiLineCommentPattern() {
        return "(?s)\\s*/\\*.*\\*/";
    }

}
