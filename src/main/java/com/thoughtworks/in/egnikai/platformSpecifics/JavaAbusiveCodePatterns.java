package com.thoughtworks.in.egnikai.platformSpecifics;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class JavaAbusiveCodePatterns implements AbusiveCodePatterns {

    @Override
    public List<Pattern> getAbusiveCodePatterns() {
        List<Pattern> patterns = new ArrayList<>();
        patterns.add(Pattern.compile(".*System\\..*"));
        patterns.add(Pattern.compile(".*implements Runnable.*"));
        patterns.add(Pattern.compile(".*extends Thread.*"));
        patterns.add(Pattern.compile(".*Process.*"));
        patterns.add(Pattern.compile(".*Thread.*"));
        patterns.add(Pattern.compile(".*sleep.*"));
        patterns.add(Pattern.compile(".*exec\\(.*"));
        patterns.add(Pattern.compile(".*new File\\(.*\\).*"));
        patterns.add(Pattern.compile(".*import java\\.net.*"));
        patterns.add(Pattern.compile(".*import java\\.io.*"));
        return patterns;
    }

    @Override
    public String getSingleLineCommentPattern() {
        return "[\\s]*//.*";
    }

    @Override
    public String getMultiLineCommentPattern() {
        return "(?s)\\s*/\\*.*\\*/";
    }


}
