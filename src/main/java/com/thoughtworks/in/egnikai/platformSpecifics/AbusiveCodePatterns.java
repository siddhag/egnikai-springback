package com.thoughtworks.in.egnikai.platformSpecifics;

import java.util.List;
import java.util.regex.Pattern;

public interface AbusiveCodePatterns {
    List<Pattern> getAbusiveCodePatterns();

    String getSingleLineCommentPattern();

    String getMultiLineCommentPattern();


}
