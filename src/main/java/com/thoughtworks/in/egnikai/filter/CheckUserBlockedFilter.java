package com.thoughtworks.in.egnikai.filter;

import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.service.CandidateService;
import com.thoughtworks.in.egnikai.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CheckUserBlockedFilter extends GenericFilterBean {

    private static final Logger logger = LoggerFactory.getLogger(CheckUserBlockedFilter.class);

    private CandidateService candidateService;
    private UserService userService;

    @Autowired
    public CheckUserBlockedFilter(CandidateService candidateService, UserService userService) {
        this.candidateService = candidateService;
        this.userService = userService;
    }


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        try {
            logger.debug("CheckUserBlockedFilter called..");


            if (!isAuthenticatedWebRequest(request, response)) {
                return; //will go to finally block
            }

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();

            if (userIsBlocked()) {
                logger.debug("User {} is blocked! logging user out.", auth.getName());
                userService.logOut((HttpServletRequest) request, (HttpServletResponse) response, auth);
            }
        } catch (Exception e) {
            logger.error("Error in CheckUserBlockedFilter", e);
        } finally {
            filterChain.doFilter(request, response);
        }
    }

    private boolean isAuthenticatedWebRequest(ServletRequest request, ServletResponse response) {
        boolean isWebRequest = request instanceof HttpServletRequest && response instanceof HttpServletResponse;
        boolean isAuthenticated = SecurityContextHolder.getContext().getAuthentication() != null;
        return isWebRequest && isAuthenticated;
    }

    private boolean userIsBlocked() {
        try {
            Candidate candidate = candidateService.getCurrentCandidate();
            return candidate.isLocked();
        } catch (ResourceNotFoundException candidateNotFound) {
            return true;
        } catch (ForbiddenResourceException e) {
            return false; //User is Not a candidate
        }
    }
}

