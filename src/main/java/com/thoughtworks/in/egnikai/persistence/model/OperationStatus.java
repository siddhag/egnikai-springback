package com.thoughtworks.in.egnikai.persistence.model;

public enum OperationStatus {
    NOT_STARTED,
    IN_PROGRESS,
    COMPLETED,
    FAILED
}
