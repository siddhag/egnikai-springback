package com.thoughtworks.in.egnikai.persistence.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@EqualsAndHashCode
public class ComplexityScore {
    @Getter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int complexityScoreId;

    @Getter
    private String complexity;

    @Getter
    private int score;
}
