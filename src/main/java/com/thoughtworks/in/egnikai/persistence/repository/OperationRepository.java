package com.thoughtworks.in.egnikai.persistence.repository;

import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.persistence.model.Operation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OperationRepository extends CrudRepository<Operation, String> {

    Operation findByOperationId(long operationId);
    List<Operation> findByCandidate_CandidateId(long candidateId);

}
