package com.thoughtworks.in.egnikai.persistence.repository;

import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidateRepository extends CrudRepository<Candidate, Long> {
    Candidate findOneByUser_UserIdAndIsActive(String userId, boolean isActive);

    List<Candidate> findAllByisActiveOrderByCandidateIdDesc(boolean isActive);
}
