package com.thoughtworks.in.egnikai.persistence.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static javax.persistence.TemporalType.TIMESTAMP;


@Entity
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Candidate implements Serializable {
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long candidateId;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "User_id")
    private User user;

    @Getter
    @Setter
    private String candidateGitBranch;

    @Getter
    @Setter
    private Date challengeDate;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Challenge_id")
    private Challenge challenge;

    @Getter
    @Setter
    private boolean isPassed;

    @Getter
    @Setter
    private int score;

    @Getter
    @Setter
    private boolean isActive;

    @Getter
    @Setter
    private boolean isLocked;

    @Getter
    @Setter
    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<AttemptedProblem> attemptedProblems;

    @Getter
    @Setter
    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<Operation> operations;


    @Getter
    @Setter
    private String eventId;

    @Getter
    @CreationTimestamp
    private Timestamp createdAt;

    @Getter
    @UpdateTimestamp
    private Timestamp updatedDateTime;


    public Candidate(User user, Challenge challenge, String eventId){
        this.user = user;
        this.challenge = challenge;
        this.eventId = eventId;
        this.isActive = true;

    }


    @Override
    public String toString() {
        return "Candidate{" +
                "candidateId=" + candidateId +
                ", candidateGitBranch='" + candidateGitBranch + '\'' +
                ", isActive=" + isActive +
                ", isLocked=" + isLocked +
                '}';
    }

    }
