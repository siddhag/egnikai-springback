package com.thoughtworks.in.egnikai.persistence.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="Source")
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SourceCode {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected long sourceId;

    /**
     * SourceCode file name
     */
    @Getter
    @Setter
    private String sourceName;

    /**
     * Relative file path
     */
    @Getter
    @Setter
    private String filePath;

    /**
     * SourceCode class name
     */
    @Getter
    @Setter
    private String className;

    /**
     * Whether candidate is allowed to edit this source code or not
     */
    @Getter
    @Setter
    private boolean isEditable;
}
