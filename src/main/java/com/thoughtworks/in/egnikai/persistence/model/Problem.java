package com.thoughtworks.in.egnikai.persistence.model;

import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;

@Entity
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Problem {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long problemId;

    /** Every problem must have a unique title */
    @Getter
    @Setter
    private String title;

    /** Problem statement */
    @Getter
    @Setter
    @Lob
    private String description;

    /** If this problem is optional to attend */
    @Getter
    @Setter
    private boolean optional;

    /** points to be credited if this problem is solved, i.e., all testCases passed. */
    @Getter
    @Setter
    private int credit;

    /** List of all test cases in the problem */
    @Getter
    @Setter
    @OneToMany(fetch=FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<TestCase> testCases;

    /** Platform Name(Language) corresponding to the problem */
    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "complexityScoreId" )
    private ComplexityScore complexityScoreId;

    /**
     * List of all source entities associated with this problem
     */
    @Getter
    @Setter
    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<SourceCode> sourceCodes;


    public Optional<SourceCode> findSourceCode(long sourceCodeId) {
        return getSourceCodes().stream()
                .filter(sourceCode -> sourceCode.getSourceId() == sourceCodeId)
                .findFirst();
    }
}
