package com.thoughtworks.in.egnikai.persistence.model;

import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@EqualsAndHashCode
@ToString
public class Challenge implements Serializable  {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long challengeId;

    /** Challenge header, tips etc */
    @Getter
    @Setter
    private String challengeTitle;

    /** Challenge header, tips etc */
    @Getter
    @Setter
    @Lob
    private String description;

    /**
     * Challenge platform name
     */
    @Getter
    @Setter
    private String platformName;

    /**
     * Challenge Git URL
     */
    @Getter
    @Setter
    private String challengeGitURL;

    /**
     * Challenge S3 Base Key
     */
    @Getter
    @Setter
    @Column(name = "challenge_base_s3_key")
    private String challengeBaseS3Key;


    /** List of Problems in this challenge */
    @Getter
    @Setter
    @OneToMany(fetch=FetchType.EAGER)
    private List<Problem> problems;


    public Challenge(){

    }

    public Challenge(String description) {
        this.description = description;
        problems = new ArrayList<Problem>();
    }

    public Problem findProblem(Long problemId) throws ResourceNotFoundException {
        return problems.stream()
                .filter(p -> p.getProblemId() == problemId)
                .findFirst()
                .orElseThrow(() ->
                        new ResourceNotFoundException("Problem with "
                                + problemId + " not assigned to candidate " + getChallengeId()));
    }


    public void addProblem(Problem problem) {
        problems.add(problem); // TODO -- add validation check later?
    }

}
