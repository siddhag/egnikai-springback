package com.thoughtworks.in.egnikai.persistence.repository;

import com.thoughtworks.in.egnikai.persistence.model.AttemptedProblem;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttemptedProblemRepository extends CrudRepository<AttemptedProblem, Long> {
}
