package com.thoughtworks.in.egnikai.persistence.repository;

import com.thoughtworks.in.egnikai.persistence.model.Problem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProblemRepository extends CrudRepository<Problem, Long> {
}
