package com.thoughtworks.in.egnikai.persistence.model;

public enum OperationName {
    CLONE,
    RUN
}
