package com.thoughtworks.in.egnikai.persistence.repository;

import com.thoughtworks.in.egnikai.persistence.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
}
