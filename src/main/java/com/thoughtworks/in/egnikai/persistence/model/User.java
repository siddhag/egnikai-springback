package com.thoughtworks.in.egnikai.persistence.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Random;

@NoArgsConstructor
@Entity
@EqualsAndHashCode
@ToString
public class User implements Serializable {

    @Getter
    @Setter
    @Id
    protected String userId;

    @Getter
    @Setter
    protected String userName;

    @Getter
    @Setter
    protected String password;

    @Getter
    @Setter
    protected String role;

    @Getter
    @Setter
    protected boolean enabled;

    public User(String userName, String role){
        this.userName = userName;
        this.role = role;
        this.userId = generateUserId();
        this.password = generatePassword();
        this.enabled = true;
    }

    private String generatePassword() {
       return Long.toHexString(Double.doubleToLongBits(Math.random())).substring(0,6);
    }


    private String generateUserId() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.userName.replaceAll(" ",""));
        builder.append(Double.doubleToLongBits(Math.random())%10000);
        return builder.toString();
    }

}
