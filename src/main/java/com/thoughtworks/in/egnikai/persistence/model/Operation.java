package com.thoughtworks.in.egnikai.persistence.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
@EqualsAndHashCode
@ToString
public class Operation implements Serializable {
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long operationId;

    @Getter
    @Setter
    private String operationName;

    @Getter
    @Setter
    private String operationStatus;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "Candidate_id")
    private Candidate candidate;

    @Getter
    @Setter
    @Column(name="error_information", length = 16777215, columnDefinition = "mediumtext")
    private String errorInformation;

    @Getter
    @Setter
    @UpdateTimestamp
    private Timestamp updatedDateTime;


    @Getter
    @Setter
    @CreationTimestamp
    private Timestamp createdDateTime;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "problem_id")
    private Problem problem;

}
