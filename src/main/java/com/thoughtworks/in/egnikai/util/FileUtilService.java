package com.thoughtworks.in.egnikai.util;

import com.thoughtworks.in.egnikai.exception.EgnikaiGenericException;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@Component
public class FileUtilService {

    public void writeToFile(File file, String content) throws EgnikaiGenericException {
        try {
            FileUtils.writeStringToFile(file, content, Charset.defaultCharset());
        } catch (IOException e) {
            throw new EgnikaiGenericException(e);
        }
    }

    private void deleteDirectory(String directoryPath) throws EgnikaiGenericException {
        try {
            FileUtils.deleteDirectory(new File(directoryPath));
        } catch (IOException e) {
            throw new EgnikaiGenericException(e);
        }
    }

    public Path createTempDirectory() throws EgnikaiGenericException {
        try {
            return Files.createTempDirectory("egnikai");
        } catch (IOException e) {
            throw new EgnikaiGenericException(e);
        }
    }


    public File createTarGzip(Path inputDirectoryPath) throws EgnikaiGenericException {
        File outputFile;
        try {
            outputFile = File.createTempFile("outputFile", "candidateCode.tgz");
        } catch (IOException e) {
            throw new EgnikaiGenericException(e);
        }

        try (FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
             BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
             GzipCompressorOutputStream gzipOutputStream = new GzipCompressorOutputStream(bufferedOutputStream);
             TarArchiveOutputStream tarArchiveOutputStream = new TarArchiveOutputStream(gzipOutputStream)) {

            tarArchiveOutputStream.setBigNumberMode(TarArchiveOutputStream.BIGNUMBER_POSIX);
            tarArchiveOutputStream.setLongFileMode(TarArchiveOutputStream.LONGFILE_GNU);

            List<File> files = new ArrayList<>(FileUtils.listFiles(
                    inputDirectoryPath.toFile(),
                    new RegexFileFilter("^(.*?)"),
                    DirectoryFileFilter.DIRECTORY
            ));

            for (File currentFile : files) {
                String relativeFilePath = new File(inputDirectoryPath.toUri()).toURI().relativize(
                        new File(currentFile.getAbsolutePath()).toURI()).getPath();

                TarArchiveEntry tarEntry = new TarArchiveEntry(currentFile, relativeFilePath);
                tarEntry.setSize(currentFile.length());

                tarArchiveOutputStream.putArchiveEntry(tarEntry);
                tarArchiveOutputStream.write(IOUtils.toByteArray(new FileInputStream(currentFile)));
                tarArchiveOutputStream.closeArchiveEntry();
            }
            tarArchiveOutputStream.close();
            return outputFile;
        }catch (Exception e) {
            throw new EgnikaiGenericException(e);
        }
    }
}
