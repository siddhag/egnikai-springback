package com.thoughtworks.in.egnikai.util;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import java.io.File;
import java.io.IOException;

/**
 * Git Utility Methods
 */
public class GitUtil {

    public static final String DEFAULT_GIT_DIRECTORY_PATH = "./jgit";

    /**
     * Clone specified sshUrl onto specified localDirectoryPath
     *
     * @param sshUrl
     * @param localDirectoryPath
     * @throws GitAPIException
     */
    public static void clone(String sshUrl, String localDirectoryPath) throws GitAPIException {
        Git.cloneRepository()
                .setURI(sshUrl)
                .setDirectory(new File(localDirectoryPath))
                .call();
    }

    public static void checkout(String localDirectoryPath, String branchName) throws GitAPIException, IOException {
        new Git(FileRepositoryBuilder.create(new File(localDirectoryPath, ".git"))).checkout().setName(branchName).call();
    }


    /**
     * Clone specified sshUrl onto APP DEFAULT GIT DirectoryPath
     *
     * @param sshUrl
     * @throws GitAPIException
     */
    public static void clone(String sshUrl) throws GitAPIException {
        clone(sshUrl, DEFAULT_GIT_DIRECTORY_PATH);
    }

    public static void commit(String localDirectoryPath, String sourcePath) throws IOException, GitAPIException {
        new Git(FileRepositoryBuilder.create(new File(localDirectoryPath, ".git")))
                .commit().setOnly(sourcePath).setMessage("from program").call();
    }

    public static String getHTTPSBranchURL(String branchName, String sshURL) {
        return new StringBuilder(sshURL.replaceFirst(":", "/").replace("git@", "https://")).
                append("/").append("src").append("/").append(branchName).toString();
    }
}
