package com.thoughtworks.in.egnikai.util;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PatternMatcher {
    private String NEWLINE_PATTERN = "\\r?\\n";

    public Optional<String> checkForAbusivePatterns(List<Pattern> patterns, String singleLineComment, String multiLineComment, String fileContent) {

        List<Pattern> matchedPatterns = getMatchedPatterns(patterns, singleLineComment, multiLineComment, fileContent);

        if(matchedPatterns.size() > 0) {
            Stream<String> lines = Arrays.stream(fileContent.split(NEWLINE_PATTERN));
            return lines.filter(line -> matchedPatterns.get(0).matcher(line).matches()).findFirst();
        }
        return Optional.empty();
    }


    private List<Pattern> getMatchedPatterns(List<Pattern> patterns, String singleLineComment, String multiLineComment, String fileContent){
        fileContent = fileContent.replaceAll(multiLineComment, "");
        Stream<String> lines = Arrays.stream(fileContent.split(NEWLINE_PATTERN));
        String content = lines.filter(line -> !Pattern.matches(singleLineComment, line)).collect(Collectors.joining());
        return patterns.stream().filter(pattern -> pattern.matcher(content).matches())
                .collect(Collectors.toList());
    }
}
