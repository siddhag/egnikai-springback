package com.thoughtworks.in.egnikai.util;

public class ResponseConstants {
    public static final String CLONING_REQUEST_ACCEPTED = "Cloning request accepted";
    public static final String RUN_SUCCESS_MESSAGE = "Successfully placed the run message on queue";
    public static final String SAVE_SUCCESS_MESSAGE = "Successfully placed the save message on queue";
    public static final String CANDIDATE_UPDATE_MESSAGE = "Successfully updated candidates";
    public static final String CANDIDATE_SAVE_MESSAGE = "Successfully saved candidates";
    public static final String ABUSIVE_CODE_MESSAGE = "Save not successful as it contains malicious code";

}
