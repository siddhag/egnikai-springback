package com.thoughtworks.in.egnikai.util;

import com.thoughtworks.in.egnikai.service.Tester;

public interface Platform {
    String getName();
    Tester getTester();
}
