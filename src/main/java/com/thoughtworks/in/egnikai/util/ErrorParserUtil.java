package com.thoughtworks.in.egnikai.util;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ErrorParserUtil {

    public static StringBuilder javaFailedTestCasesErrorParser(String errorInput, String packageName) {
        StringBuilder stringBuilder = new StringBuilder();
        Scanner in = null;
        if (errorInput.isEmpty() || errorInput == null) throw new NullPointerException("Error input is null for package " + packageName);
        in = new Scanner(errorInput);
        boolean isRunTimeError = true;
        while (in.hasNext()) {
            String line = in.nextLine();
            if (line.contains("java.lang.AssertionError:"))
                isRunTimeError = false;
        }
        if (isRunTimeError == false) {
            String regex = "[1-9]\\) [a-zA-Z]+";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(errorInput);
            stringBuilder.append("Failed test cases are: \n");
            while (matcher.find()) {
                stringBuilder.append(matcher.group()+"\n");
            }
        } else {
            Scanner input = null;
            input = new Scanner(errorInput);
            stringBuilder.append(input.nextLine()+"\n");
            while (input.hasNext()) {
                String line = input.nextLine();
                if (line.contains(packageName))
                    stringBuilder.append(line+"\n");
            }
        }
        return stringBuilder;
    }
}