package com.thoughtworks.in.egnikai.util;

import java.util.HashMap;

public final class PlatformRegistry {
    private static PlatformRegistry instance;
    private HashMap<String, Platform> registry;

    private PlatformRegistry() {
        registry = new HashMap<String, Platform>();
    }

    public void registerPlatform(Platform platform) {
        if ( registry.containsKey(platform.getName()) ) {
            registry.remove(platform.getName());
        }
        registry.put(platform.getName(), platform);

    }

    public static PlatformRegistry getRegistry() {
        if ( instance == null ) {
            instance = new PlatformRegistry();
        }
        return instance;
    }



    public Platform getPlatform(String platformName) {
        if ( platformName == null || platformName.trim().length() <= 0 ) {
            throw new IllegalArgumentException("Invalid (empty) platform name");
        }

        if ( registry.containsKey(platformName) ) {
            return registry.get(platformName);
        } else {
            throw new IllegalArgumentException("platform not registered: " + platformName);
        }
    }

}
