package com.thoughtworks.in.egnikai.controller;

import com.thoughtworks.in.egnikai.dto.AttemptedProblemDTO;
import com.thoughtworks.in.egnikai.dto.OperationIdDTO;
import com.thoughtworks.in.egnikai.dto.OperationStatusDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.service.CandidateService;
import com.thoughtworks.in.egnikai.service.OperationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(
        path = "/api/candidate"
)
public class CandidateController {

    private CandidateService candidateService;
    private OperationService operationService;

    private static final Logger logger = LoggerFactory.getLogger(CandidateController.class);

    @Autowired
    public CandidateController(CandidateService candidateService, OperationService operationService) {
        this.candidateService = candidateService;
        this.operationService = operationService;
    }

    @PutMapping("/problems/{problemId}")
    public OperationIdDTO runProblem(@PathVariable String problemId) throws ResourceNotFoundException {
        logger.debug("redirecting to candidate service to run problem:" + problemId);
        return candidateService.runProblem(Long.parseLong(problemId));
    }

//    TODO: Need to be removed
    @GetMapping("/operation")
    public List<OperationStatusDTO> getOperationDetails(@RequestParam("operationId") List<Long> operationIds)  {
        logger.debug("Getting operation details");
        List<OperationStatusDTO> operationDetails = operationService.getOperationDetails(operationIds);
        logger.debug("Returning operation details:" + operationDetails);
        return operationDetails;
    }

//    TODO: Need to be removed
    @PutMapping(value = "/challenge")
    public OperationIdDTO cloneGitRepository() throws ForbiddenResourceException, ResourceNotFoundException {
        logger.debug("Calling PutMessageInQueue from Candidate Service");
        return candidateService.putCloningMessageInQueue();
    }

//    TODO: Need to be removed
    @GetMapping(value = "/challenge")
    public OperationStatusDTO getCloneStatus() throws ForbiddenResourceException, ResourceNotFoundException {
        logger.debug("Calling getCloningOperationDetail from Candidate Service");
        return candidateService.getCloningOperationDetails();
    }

    @GetMapping(value = "/attemptedProblems")
    public List<AttemptedProblemDTO> getAttemptedProblems() throws ForbiddenResourceException, ResourceNotFoundException {
        return  candidateService.getAttemptedProblems();
    }
}
