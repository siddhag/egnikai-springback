package com.thoughtworks.in.egnikai.controller;

import com.thoughtworks.in.egnikai.dto.ChallengeBaseDTO;
import com.thoughtworks.in.egnikai.dto.ChallengeDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.service.ChallengeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(
        path = "/api"
)
public class ChallengeController {
    private static final Logger logger = LoggerFactory.getLogger(ChallengeController.class);

    private ChallengeService challengeService;

    @Autowired
    public ChallengeController(ChallengeService challengeService) {
        this.challengeService = challengeService;
    }


    @GetMapping(value = "/challenges")
    public ChallengeDTO getChallenge() throws ForbiddenResourceException, ResourceNotFoundException {
        logger.debug("Getting challenge\n");
        ChallengeDTO challengeDTO = challengeService.getChallenge();
        logger.debug("Returning " + challengeDTO);
        return challengeDTO;
    }

}
