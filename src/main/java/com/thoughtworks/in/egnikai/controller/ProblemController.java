package com.thoughtworks.in.egnikai.controller;

import com.thoughtworks.in.egnikai.dto.GenericResponseDTO;
import com.thoughtworks.in.egnikai.dto.ProblemDetailDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.exception.TimeOutException;
import com.thoughtworks.in.egnikai.service.ProblemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping(
        path = "/api/problems"
)
public class ProblemController {

    private ProblemService problemService;
    private static final Logger logger = LoggerFactory.getLogger(ProblemController.class);


    @Autowired
    public ProblemController(ProblemService problemService) {
        this.problemService = problemService;
    }

    @PutMapping("/{problemId}/sources/{sourceId}")
    public GenericResponseDTO updateSourceFile(@PathVariable String problemId,
                                               @PathVariable String sourceId,
                                               @RequestBody String fileContent) throws IOException, ResourceNotFoundException, ForbiddenResourceException {
        logger.debug("calling the problem service to update file with  problem id:" + problemId + " and source id:" + sourceId);
        return problemService.saveProblem(
                Integer.parseInt(problemId),
                Integer.parseInt(sourceId),
                fileContent);
    }

    @GetMapping(value = "/{problemId}")
    public ProblemDetailDTO getProblem(@PathVariable("problemId") long problemId)
            throws ResourceNotFoundException, ForbiddenResourceException {
        return problemService.getProblem(problemId);
    }

}