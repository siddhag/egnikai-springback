package com.thoughtworks.in.egnikai.controller;

import com.thoughtworks.in.egnikai.dto.SourceDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.service.CandidateService;
import com.thoughtworks.in.egnikai.service.SourceCodeService;
import com.thoughtworks.in.egnikai.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(path = "/api")
public class SourceCodeController {
    private SourceCodeService sourceCodeService;
    private UserService userService;
    private CandidateService candidateService;


    @Autowired
    public SourceCodeController(SourceCodeService sourceCodeService,
                                UserService userService,
                                CandidateService candidateService) {
        this.sourceCodeService = sourceCodeService;
        this.userService = userService;
        this.candidateService = candidateService;
    }

    @GetMapping(value = "/problems/{problemId}/sources/{sourceId}")
    public String getSource(@PathVariable("problemId") long problemId,
                            @PathVariable("sourceId") long sourceId)
            throws IOException, ResourceNotFoundException, ForbiddenResourceException {
        if (!userService.isCandidate(userService.getCurrentUser())) {
            throw new ForbiddenResourceException("Not a valid user for candidate "+ userService.getCurrentUser());
        }
        Candidate currentCandidate = candidateService.getCurrentCandidate();
        SourceDTO sourceDTO = sourceCodeService.getSourceCode(currentCandidate, problemId, sourceId);
        return sourceDTO.getSourceCode();
    }
}
