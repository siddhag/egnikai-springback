package com.thoughtworks.in.egnikai.controller;

import com.thoughtworks.in.egnikai.dto.TestDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.service.CandidateService;
import com.thoughtworks.in.egnikai.service.TestCaseService;
import com.thoughtworks.in.egnikai.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(path = "/api")
public class TestCaseController {
    private TestCaseService testCaseService;
    private UserService userService;
    private CandidateService candidateService;

    @Autowired
    public TestCaseController(TestCaseService testCaseService,
                              UserService userService,
                              CandidateService candidateService) {
        this.testCaseService = testCaseService;
        this.userService = userService;
        this.candidateService = candidateService;
    }

    @GetMapping(value = "/problems/{problemId}/tests/{testId}")
    public String getTest(@PathVariable("problemId") long problemId,
                          @PathVariable("testId") long testId)
            throws IOException, ResourceNotFoundException, ForbiddenResourceException {
        if (!userService.isCandidate(userService.getCurrentUser())) {
            throw new ForbiddenResourceException("Not a valid user for candidate "+ userService.getCurrentUser());
        }

        Candidate currentCandidate = candidateService.getCurrentCandidate();

        TestDTO testDTO = testCaseService.getTest(currentCandidate, problemId, testId);
        return testDTO.getTestCode();
    }
}
