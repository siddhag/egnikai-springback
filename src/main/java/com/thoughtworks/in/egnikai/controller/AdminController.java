package com.thoughtworks.in.egnikai.controller;

import com.thoughtworks.in.egnikai.dto.*;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.User;
import com.thoughtworks.in.egnikai.service.AdminService;
import com.thoughtworks.in.egnikai.service.ChallengeService;
import com.thoughtworks.in.egnikai.service.UserService;
import com.thoughtworks.in.egnikai.util.ResponseConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.NotAuthorizedException;
import java.util.List;

@RestController
@RequestMapping(
        path = "/admin/api"
)
public class AdminController {

    private static final Logger logger = LoggerFactory.getLogger(ChallengeController.class);
    private AdminService adminService;
    private ChallengeService challengeService;
    private UserService userService;

    @Autowired
    public AdminController(AdminService adminService, UserService userService, ChallengeService challengeService) {
        this.userService = userService;
        this.adminService = adminService;
        this.challengeService = challengeService;
    }

    @GetMapping(value = "/candidates")
    public List<CandidateDTO> getCandidates(@RequestParam(required = true, value = "isActive") Boolean isActive) throws ForbiddenResourceException, ResourceNotFoundException {
        throwUnauthorizedWhenNotAdmin();
        return adminService.getCandidates(isActive);
    }

    @GetMapping(value = "/challenges")
    public List<ChallengeBaseDTO> getChallenges() throws ResourceNotFoundException, NotAuthorizedException {
        throwUnauthorizedWhenNotAdmin();
        logger.debug("Getting all challenges\n");
        List<ChallengeBaseDTO> challenges = challengeService.getChallenges();
        logger.debug("Returning challenges");
        return challenges;
    }

    @PutMapping(value = "/candidates")
    public GenericResponseDTO updateCandidates(@RequestBody CandidateUpdateDTO candidateUpdateDTO) throws ResourceNotFoundException, ForbiddenResourceException, AuthenticationException {
        throwUnauthorizedWhenNotAdmin();
        return adminService.updateCandidates(candidateUpdateDTO.getIsLocked(), candidateUpdateDTO.getIsActive(), candidateUpdateDTO.getCandidateIds());
    }

    @PostMapping(value = "/candidates/create")
    public GenericResponseDTO createCandidatesForEvent(@RequestBody EventDTO eventDTO) throws ResourceNotFoundException {
        throwUnauthorizedWhenNotAdmin();
        try {
            adminService.createUsersForEvent(eventDTO);
            return new GenericResponseDTO(HttpStatus.OK, ResponseConstants.CANDIDATE_SAVE_MESSAGE);
        } catch (Exception ex) {
            String errorMessage = "Something went wrong while creating user or candidate";
            logger.error(errorMessage + ex);
            return new GenericResponseDTO(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage);
        }
    }

    @PostMapping("/login")
    public UserDTO loginRequestHandler() throws ResourceNotFoundException {
        throwUnauthorizedWhenNotAdmin();
        User currentUser = userService.getCurrentUser();
        UserDTO userDTO = UserDTO.fromUser(currentUser);
        return userDTO;
    }

    @PostMapping("/logout")
    public String logoutRequestHandler(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login";
    }

    private void throwUnauthorizedWhenNotAdmin() throws NotAuthorizedException, ResourceNotFoundException {
        if (!userService.isAdmin(userService.getCurrentUser())) {
            throw new NotAuthorizedException("User not admin");
        }
    }
}
