package com.thoughtworks.in.egnikai.controller;


import com.thoughtworks.in.egnikai.dto.LogoutDTO;
import com.thoughtworks.in.egnikai.dto.UserDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.User;
import com.thoughtworks.in.egnikai.service.CandidateService;
import com.thoughtworks.in.egnikai.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api")
public class AuthController {
    private UserService userService;
    private final CandidateService candidateService;


    @Autowired
    public AuthController(UserService userService, CandidateService candidateService) {
        this.userService = userService;
        this.candidateService = candidateService;
    }

    @PostMapping("/logout")
    public String logout(HttpServletRequest request,
                         HttpServletResponse response,
                         @RequestBody LogoutDTO logoutDTO)
            throws ResourceNotFoundException, ForbiddenResourceException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            if(logoutDTO.isShouldCandidateBeLocked()) {
                candidateService.lockCandidate();
            }
            userService.logOut(request,response,auth);
        }
        return "redirect:/login";
    }

    @PostMapping("/login")
    public UserDTO login() throws ResourceNotFoundException {
        User currentUser = userService.getCurrentUser();
        UserDTO userDTO = UserDTO.fromUser(currentUser);
        return userDTO;
    }
}
