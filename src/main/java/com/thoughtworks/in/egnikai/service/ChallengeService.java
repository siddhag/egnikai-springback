package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.dto.ChallengeBaseDTO;
import com.thoughtworks.in.egnikai.dto.ChallengeDTO;
import com.thoughtworks.in.egnikai.dto.ProblemSummaryDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.persistence.model.Challenge;
import com.thoughtworks.in.egnikai.persistence.model.Problem;
import com.thoughtworks.in.egnikai.persistence.repository.ChallengeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChallengeService {
    private static final Logger logger = LoggerFactory.getLogger(ChallengeService.class);

    private ChallengeRepository challengeRepository;
    private CandidateService candidateService;

    @Value("${egnikai.problem.credit.visible}")
    private boolean isCreditVisible;

    @Autowired
    public ChallengeService(ChallengeRepository challengeRepository, CandidateService candidateService) {
        this.challengeRepository = challengeRepository;
        this.candidateService = candidateService;
    }

    //for admin
    public List<ProblemSummaryDTO> getProblems(long challengeId) {
        Challenge challenge = challengeRepository.findOne(challengeId);
        List<Problem> problems = challenge.getProblems();
        return ProblemSummaryDTO.fromProblem(problems, isCreditVisible);
    }

    //for admin
    public List<ChallengeBaseDTO> getChallenges() {
        Iterable<Challenge> challenges = challengeRepository.findAll();


        return ChallengeBaseDTO.fromChallenge(challenges);
    }

    public ChallengeDTO getChallenge() throws ForbiddenResourceException, ResourceNotFoundException {
        logger.debug("Getting challenge - Challenge Service");
        Candidate candidate = candidateService.getCurrentCandidate();
        Challenge challenge = candidate.getChallenge();
        if(challenge == null){
            throw new ResourceNotFoundException("Challenge Not found");
        }
        ChallengeDTO challengeDTO = ChallengeDTO.fromChallenge(challenge, isCreditVisible);
        logger.debug("Returning " + challengeDTO);
        return challengeDTO;
    }

    public Challenge getChallenge(Long challengeId) {
        Challenge challenge = challengeRepository.findOne(challengeId);
        return challenge;
    }
}
