package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.persistence.model.Challenge;
import com.thoughtworks.in.egnikai.persistence.model.Problem;

//TODO: for now this file is just placed in this folder.

public interface Tester {

    TestResults runTests(Challenge challenge);
    TestResults runTests(Problem problem);
}
