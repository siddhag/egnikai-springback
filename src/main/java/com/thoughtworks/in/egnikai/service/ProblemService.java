package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.dto.*;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.*;
import com.thoughtworks.in.egnikai.persistence.repository.ProblemRepository;
import com.thoughtworks.in.egnikai.platformSpecifics.CSharpAbusiveCodePatterns;
import com.thoughtworks.in.egnikai.platformSpecifics.JavaAbusiveCodePatterns;
import com.thoughtworks.in.egnikai.util.PatternMatcher;
import com.thoughtworks.in.egnikai.util.ResponseConstants;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;
import static org.springframework.data.jpa.domain.Specifications.where;

@Service
public class ProblemService {

    public static final String C_SHARP = "CSharp";
    public static final String JAVA = "Java";
    private UserService userService;
    private CandidateService candidateService;
    private S3Service s3Service;
    private boolean isCreditVisible;
    private String candidateCodeSubmissionsBucket;
    private ModelMapper mapper = new ModelMapper();
    private PatternMatcher patternMatcher = new PatternMatcher();
    private JavaAbusiveCodePatterns javaAbusiveCodePatterns = new JavaAbusiveCodePatterns();
    private CSharpAbusiveCodePatterns cSharpAbusiveCodePatterns = new CSharpAbusiveCodePatterns();

    @Autowired
    public ProblemService(UserService userService,
                          CandidateService candidateService,
                          S3Service s3Service,
                          @Value("${egnikai.problem.credit.visible}") boolean isCreditVisible,
                          @Value("${egnikai.s3.bucket.candidate-code-submissions}") String candidateCodeSubmissionsBucket) {
        this.userService = userService;
        this.candidateService = candidateService;
        this.s3Service = s3Service;
        this.isCreditVisible = isCreditVisible;
        this.candidateCodeSubmissionsBucket = candidateCodeSubmissionsBucket;
    }

    public ProblemDetailDTO getProblem(long problemId) throws ResourceNotFoundException, ForbiddenResourceException {
        if (!userService.isCandidate(userService.getCurrentUser())) {
            throw new ForbiddenResourceException("Not a valid user for candidate " + userService.getCurrentUser());
        }
        Candidate candidate = candidateService.getCurrentCandidate();
        Problem problem = getProblem(candidate, problemId);
        ProblemDetailDTO problemDetailDTO = new ProblemDetailDTO();
        mapProblemToProblemDetailDTO(problem, problemDetailDTO);
        return problemDetailDTO;
    }

    private Problem getProblem(Candidate currentCandidate, long problemId) throws ResourceNotFoundException {
        return currentCandidate.getChallenge().getProblems().stream()
                .filter(problem -> problem.getProblemId() == problemId)
                .findFirst()
                .orElseThrow(() -> new ResourceNotFoundException("Problem with " + problemId + "not found"));
    }

    private void mapProblemToProblemDetailDTO(Problem problem, ProblemDetailDTO problemDetailDTO) throws ResourceNotFoundException {
        List<SourceDTO> sourceDTOs = getSourceDTOS(problem);
        List<TestDTO> testDTOs = getTestDTOS(problem);

        problemDetailDTO.setProblemId(problem.getProblemId());
        problemDetailDTO.setTitle(problem.getTitle());
        problemDetailDTO.setDescription(problem.getDescription());
        if (isCreditVisible) {
            problemDetailDTO.setCredit(problem.getCredit());
        }
        problemDetailDTO.setSourceDTOs(sourceDTOs);
        problemDetailDTO.setTestDTOs(testDTOs);
    }

    private List<SourceDTO> getSourceDTOS(Problem problem) throws ResourceNotFoundException {
        List<SourceCode> sourceCodes = problem.getSourceCodes();
        if (sourceCodes == null) {
            throw new ResourceNotFoundException("Sources not found for problem " + problem);
        }

        return sourceCodes.stream().map(sourceCode -> {
            SourceDTO sourceDTO = new SourceDTO();
            mapper.map(sourceCode, sourceDTO);
            return sourceDTO;

        }).collect(Collectors.toList());
    }

    private List<TestDTO> getTestDTOS(Problem problem) throws ResourceNotFoundException {
        List<TestCase> testCases = problem.getTestCases();
        if (testCases == null) {
            throw new ResourceNotFoundException("Tests not found for problem " + problem);
        }

        Stream<TestCase> testCaseStream = testCases.stream().filter(testCase -> testCase.getType() == TestType.PUBLIC);

        return testCaseStream.map(testCase -> {
            TestDTO testDTO = new TestDTO();
            mapper.map(testCase, testDTO);
            return testDTO;
        }).collect(Collectors.toList());
    }

    private String getSourceFilePath(Candidate currentCandidate, long problemId, long sourceId) throws ResourceNotFoundException {
        Problem problem = getProblem(currentCandidate, problemId);
        return problem.findSourceCode(sourceId)
                .orElseThrow(() -> new ResourceNotFoundException("Source id " + sourceId + " not found"))
                .getFilePath();
    }

    public GenericResponseDTO saveProblem(int problemId, int sourceId, String fileContent) throws ResourceNotFoundException, ForbiddenResourceException {
        if (!userService.isCandidate(userService.getCurrentUser())) {
            throw new ForbiddenResourceException("Not a valid user for candidate " + userService.getCurrentUser());
        }
        String language = candidateService.getCurrentCandidate().getChallenge().getPlatformName();
        Candidate candidate = candidateService.getCurrentCandidate();
        String filepath = getSourceFilePath(candidate, problemId, sourceId);
        Optional<String> matchedPattern = Optional.empty();

        if (language.equals(JAVA)) {
            matchedPattern = patternMatcher.checkForAbusivePatterns(javaAbusiveCodePatterns.getAbusiveCodePatterns(),
                    javaAbusiveCodePatterns.getSingleLineCommentPattern(), javaAbusiveCodePatterns.getMultiLineCommentPattern(), fileContent);

        } else if (language.equals(C_SHARP)) {
            matchedPattern = patternMatcher.checkForAbusivePatterns(cSharpAbusiveCodePatterns.getAbusiveCodePatterns(),
                    cSharpAbusiveCodePatterns.getSingleLineCommentPattern(), cSharpAbusiveCodePatterns.getMultiLineCommentPattern(), fileContent);
        }

        if (matchedPattern.isPresent()) {
            return new GenericResponseDTO(HttpStatus.BAD_REQUEST, ResponseConstants.ABUSIVE_CODE_MESSAGE + " : " + matchedPattern.get());
        }
        saveFileToS3(problemId, fileContent, candidate, filepath);
        return new GenericResponseDTO(HttpStatus.OK, ResponseConstants.SAVE_SUCCESS_MESSAGE);
    }

    private void saveFileToS3(int problemId, String fileContent, Candidate candidate, String filepath) {
        s3Service.uploadFileContent(candidateCodeSubmissionsBucket
                , format("saved/%d/%d/%s"
                        , candidate.getCandidateId()
                        , problemId
                        , filepath)
                , fileContent);
    }


}
