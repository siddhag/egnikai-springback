package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.dto.SourceDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.persistence.model.Challenge;
import com.thoughtworks.in.egnikai.persistence.model.Problem;
import com.thoughtworks.in.egnikai.persistence.model.SourceCode;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static java.lang.String.format;

@Service
public class SourceCodeService {
    private ModelMapper mapper = new ModelMapper();
    private S3Service s3Service;
    private final String candidateCodeSubmissionsBucket;
    private final String codeProblemsBucket;


    @Autowired
    public SourceCodeService(S3Service s3Service,
                             @Value("${egnikai.s3.bucket.candidate-code-submissions}") String candidateCodeSubmissionsBucket,
                             @Value("${egnikai.s3.bucket.code-problems}") String codeProblemsBucket) {
        this.s3Service = s3Service;
        this.candidateCodeSubmissionsBucket = candidateCodeSubmissionsBucket;
        this.codeProblemsBucket = codeProblemsBucket;
    }

    public SourceDTO getSourceCode(Candidate currentCandidate, long problemId, long sourceId)
            throws ResourceNotFoundException, ForbiddenResourceException {
        SourceCode sourceCode = findSourceCode(currentCandidate, problemId, sourceId);
        SourceDTO sourceDTO = new SourceDTO();
        mapper.map(sourceCode, sourceDTO);
        sourceDTO.setSourceCode(getCode(currentCandidate, problemId, sourceCode.getFilePath()));
        return sourceDTO;
    }

    private SourceCode findSourceCode(Candidate currentCandidate, long problemId, long sourceId) throws  ResourceNotFoundException {
        Problem problem = currentCandidate.getChallenge().findProblem(problemId);
        return problem.getSourceCodes().stream()
                .filter(sourceCode -> sourceCode.getSourceId() == sourceId)
                .findFirst()
                .orElseThrow(() -> new ResourceNotFoundException("Source id "+sourceId+" not found"));
    }

    private String getCode(Candidate candidate, long problemId, String sourceFilePath) throws ResourceNotFoundException {
        return s3Service.getFileContent(candidateCodeSubmissionsBucket,
                format("saved/%d/%d/%s",
                        candidate.getCandidateId(),
                        problemId,
                        sourceFilePath))
                .orElseGet(() -> s3Service.getFileContent(codeProblemsBucket,
                        format("%s/%s",
                                candidate.getChallenge().getChallengeBaseS3Key(),
                                sourceFilePath))
                        .orElseThrow(() -> new ResourceNotFoundException("Source code not found")));
    }
}
