package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.dto.CandidateDTO;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.Challenge;
import com.thoughtworks.in.egnikai.persistence.model.User;
import com.thoughtworks.in.egnikai.persistence.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private UserRepository userRepository;

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getCurrentUser() throws ResourceNotFoundException {
        logger.debug("Getting Current User");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) auth.getPrincipal();
        User userFromRepository = userRepository.findOne(user.getUsername());
        if(userFromRepository == null){
            logger.debug("User not found "+user);
            throw new ResourceNotFoundException("User not found");
        }
        logger.debug("Returning "+userFromRepository);
        return  userFromRepository;
    }

    public Boolean isCandidate(User user) throws ResourceNotFoundException {
        logger.debug("Checking for user is candidate\n"+ "Returning "+ user.getRole().equalsIgnoreCase("Candidate"));
        return user.getRole().equalsIgnoreCase("Candidate");
    }

    public Boolean isAdmin(User user) throws ResourceNotFoundException {
        boolean isAdmin = user.getRole().equalsIgnoreCase("Admin");
        logger.debug("Checking if user is an admin\n" + "Returning "+ isAdmin);
        return isAdmin;
    }

    public void logOut(HttpServletRequest request, HttpServletResponse response, Authentication auth) {
        new SecurityContextLogoutHandler().logout(request, response, auth);
    }

    @Transactional(propagation=Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void saveUsers(List<User> users) {
        userRepository.save(users);
    }

}
