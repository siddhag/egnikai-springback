package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.dto.CandidateChallengeDTO;
import com.thoughtworks.in.egnikai.dto.CandidateDTO;
import com.thoughtworks.in.egnikai.dto.EventDTO;
import com.thoughtworks.in.egnikai.dto.GenericResponseDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.persistence.model.Challenge;
import com.thoughtworks.in.egnikai.persistence.model.User;
import com.thoughtworks.in.egnikai.persistence.repository.CandidateRepository;
import com.thoughtworks.in.egnikai.util.ResponseConstants;
import jersey.repackaged.com.google.common.collect.Lists;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdminService {

    @Autowired
    private CandidateRepository candidateRepository;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CandidateService.class);

    @Autowired
    private UserService userService;

    @Autowired
    private ChallengeService challengeService;

    @Autowired
    private CandidateService candidateService;

    public List<CandidateDTO> getCandidates(Boolean isActive) {
        return CandidateDTO.fromCandidates(Lists.newArrayList(candidateRepository.findAllByisActiveOrderByCandidateIdDesc(isActive)));
    }


    public GenericResponseDTO updateCandidates(Boolean isLocked, Boolean isActive, List<Long> candidateIds) throws ForbiddenResourceException {

        candidateIds.stream().forEach(candidateId -> {

            Candidate candidate = candidateRepository.findOne(candidateId);
            if (candidate != null) {

                if (isLocked != null) {
                    candidate.setLocked(isLocked);

                }
                if (isActive != null) {
                    candidate.setActive(isActive);

                }
                candidateRepository.save(candidate);
            }

        });

        return new GenericResponseDTO(HttpStatus.OK,ResponseConstants.CANDIDATE_UPDATE_MESSAGE);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class})
    public void createUsersForEvent(EventDTO eventDTO) throws ResourceNotFoundException {
        ArrayList<Candidate> candidatesToBeSaved = new ArrayList<>();
        ArrayList<User> usersToBeCreated = new ArrayList<>();
        for (CandidateChallengeDTO candidateChallengeDTO : eventDTO.getCandidateChallengeDTOS()) {
            createUsersAndCandidates(candidatesToBeSaved, usersToBeCreated, candidateChallengeDTO, eventDTO.getEventId());
        }
        userService.saveUsers(usersToBeCreated);
        candidateService.saveCandidates(candidatesToBeSaved);
    }

    private void createUsersAndCandidates(ArrayList<Candidate> candidatesToBeSaved, ArrayList<User> usersToBeCreated, CandidateChallengeDTO candidateChallengeDTO, String eventId) throws ResourceNotFoundException {
        Challenge challenge = challengeService.getChallenge(candidateChallengeDTO.getChallengeId());
        if (challenge == null) {
            throw new ResourceNotFoundException("Challenge " + candidateChallengeDTO.getChallengeId() + " not found!");
        }
        User user = new User(candidateChallengeDTO.getCandidateName(), "CANDIDATE");
        usersToBeCreated.add(user);
        candidatesToBeSaved.add(new Candidate(user, challenge, eventId));
    }
}
