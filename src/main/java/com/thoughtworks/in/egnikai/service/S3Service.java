package com.thoughtworks.in.egnikai.service;

import com.amazonaws.services.s3.AmazonS3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Optional;

@Service
public class S3Service {

    private final AmazonS3 amazonS3Client;

    @Autowired
    public S3Service(AmazonS3 amazonS3Client) {
        this.amazonS3Client = amazonS3Client;
    }


    private boolean isFilePresent(String bucket, String key) {
        return amazonS3Client.doesObjectExist(bucket, key);
    }

    public Optional<String> getFileContent(String bucket, String keyName) {
        if (isFilePresent(bucket, keyName)) {
            return Optional.of(amazonS3Client.getObjectAsString(bucket, keyName));
        }
        return Optional.empty();
    }

    public void uploadFileContent(String bucketName, String keyName, String filecontent) {
        amazonS3Client.putObject(bucketName, keyName, filecontent);
    }

    public void uploadFile(String bucket, String key, File file) {
        amazonS3Client.putObject(bucket, key, file);
    }
}
