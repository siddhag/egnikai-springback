package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.controller.CandidateController;
import com.thoughtworks.in.egnikai.dto.SourceDTO;
import com.thoughtworks.in.egnikai.dto.TestDTO;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.*;
import com.thoughtworks.in.egnikai.util.FileUtilService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Path;

import static java.lang.String.format;

@Service
public class RunsService {

    private OperationService operationService;
    private S3Service s3Service;
    private final SourceCodeService sourceCodeService;
    private final TestCaseService testCaseService;
    private FileUtilService fileUtilService;
    private final String candidateCodeSubmissionsBucket;
    private static final Logger logger = LoggerFactory.getLogger(RunsService.class);

    @Autowired
    public RunsService(OperationService operationService,
                       S3Service s3Service,
                       SourceCodeService sourceCodeService,
                       TestCaseService testCaseService,
                       FileUtilService fileUtilService,
                       @Value("${egnikai.s3.bucket.candidate-code-submissions}") String candidateCodeSubmissionsBucket) {
        this.operationService = operationService;
        this.s3Service = s3Service;
        this.sourceCodeService = sourceCodeService;
        this.testCaseService = testCaseService;
        this.fileUtilService = fileUtilService;
        this.candidateCodeSubmissionsBucket = candidateCodeSubmissionsBucket;
    }

    public Operation scheduleRunProblemForCandidate(Candidate candidate, Problem problem) throws ResourceNotFoundException {
        Operation operation = createOperation(candidate, problem);
        operationService.saveOperation(operation);

        Path tempDirectory = saveSourceAndTestInTempDirectory(problem, candidate);

        File tarGzip = fileUtilService.createTarGzip(tempDirectory);

        String s3FileKey = getS3FileKey(candidate, operation);
        logger.info("Going to upload tgz file {}", s3FileKey);
        s3Service.uploadFile(candidateCodeSubmissionsBucket, s3FileKey, tarGzip);

        operation.setOperationStatus(OperationStatus.IN_PROGRESS.name());
        operationService.saveOperation(operation);
        return operation;
    }

    private String getS3FileKey(Candidate candidate, Operation operation) {
        return format("runs/%s/%d/%d.tgz",
                candidate.getChallenge().getPlatformName(),
                candidate.getCandidateId(),
                operation.getOperationId());
    }

    private Path saveSourceAndTestInTempDirectory(Problem problem, Candidate currentCandidate) {
        Path tempDirectory = fileUtilService.createTempDirectory();
        getSourceCodeInTempDirectory(problem, tempDirectory, currentCandidate);
        getTestCodeInTempDirectory(problem, tempDirectory, currentCandidate);
        return tempDirectory;
    }

    private void getTestCodeInTempDirectory(Problem problem, Path tempDirectory, Candidate currentCandidate) {
        problem.getTestCases().stream().forEach(testCase -> {
            TestDTO testDTO = testCaseService.getTest(currentCandidate, problem.getProblemId(), testCase.getTestId(), true);
            fileUtilService.writeToFile(tempDirectory.resolve(testCase.getFilePath()).toFile(), testDTO.getTestCode());
        });
    }

    private void getSourceCodeInTempDirectory(Problem problem, Path tempDirectory, Candidate currentCandidate) {
        problem.getSourceCodes().stream().forEach(sourceCode -> {
            SourceDTO sourceDTO = sourceCodeService.getSourceCode(currentCandidate, problem.getProblemId(), sourceCode.getSourceId());
            fileUtilService.writeToFile(tempDirectory.resolve(sourceCode.getFilePath()).toFile(), sourceDTO.getSourceCode());
        });
    }

    private Operation createOperation(Candidate candidate, Problem problem) {
        Operation operation = new Operation();
        operation.setOperationName(OperationName.RUN.toString());
        operation.setCandidate(candidate);
        operation.setOperationStatus(OperationStatus.NOT_STARTED.toString());
        operation.setProblem(problem);
        return operation;
    }
}
