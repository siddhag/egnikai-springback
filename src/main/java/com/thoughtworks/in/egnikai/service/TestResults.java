package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.persistence.model.Challenge;
import com.thoughtworks.in.egnikai.persistence.model.Problem;
import com.thoughtworks.in.egnikai.persistence.model.TestCase;

//TODO: for now this file is just placed in this folder.
public class TestResults {
    private Challenge challenge;
    private Problem problem;
    private int score;

    public TestResults(Challenge challenge) {
        if ( challenge == null ) {
            throw new IllegalArgumentException("Cannot create test results for empty challenge");
        }
        this.challenge = challenge;
    }

    public TestResults(Problem problem) {
        if ( problem == null ) {
            throw new IllegalArgumentException("Cannot create test results for empty problem");
        }
        this.problem = problem;
    }
    public int getScore() {
        return this.score;
    }
}
