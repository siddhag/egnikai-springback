package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.persistence.model.Operation;
import com.thoughtworks.in.egnikai.persistence.model.OperationStatus;
import com.thoughtworks.in.egnikai.persistence.repository.OperationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

import static java.lang.String.format;

@Service
public class RunsCompletionService {
    private static final Logger logger = LoggerFactory.getLogger(RunsCompletionService.class);

    private final S3Service s3Service;
    private final String candidateCodeSubmissionsBucket;
    private final OperationRepository operationRepository;
    private final AttemptedProblemService attemptedProblemService;
    private final ScoreService scoreService;

    public RunsCompletionService(
      S3Service s3Service,
      @Value("${egnikai.s3.bucket.candidate-code-submissions}")
        String candidateCodeSubmissionsBucket,
      OperationRepository operationRepository,
      AttemptedProblemService attemptedProblemService, ScoreService scoreService) {
        this.s3Service = s3Service;
        this.candidateCodeSubmissionsBucket = candidateCodeSubmissionsBucket;
        this.operationRepository = operationRepository;
        this.attemptedProblemService = attemptedProblemService;
        this.scoreService = scoreService;
    }

    public void checkAndProcessRunCompletion(Operation operation) {
        checkIfResultComputedInS3(operation);
    }

    private void checkIfResultComputedInS3(Operation operation) {
        Candidate candidate = operation.getCandidate();
        Optional<String> resultJson = s3Service.getFileContent(candidateCodeSubmissionsBucket,
          format("runs/%s/%d/%d.json",
            candidate.getChallenge().getPlatformName(),
            candidate.getCandidateId(),
            operation.getOperationId()));

        if (resultJson.isPresent()) {
            JacksonJsonParser jacksonJsonParser = new JacksonJsonParser();
            Map<String, Object> parseMap = jacksonJsonParser.parseMap(resultJson.get());

            operation.setErrorInformation(resultJson.get());
            operation.setOperationStatus(
              (Boolean) parseMap.get("success") ?
                OperationStatus.COMPLETED.name() :
                OperationStatus.FAILED.name());
            operationRepository.save(operation);

            callbackIfResultAvailable(operation);
        }
    }

    private void callbackIfResultAvailable(Operation operation) {
        attemptedProblemService.updateAttemptedProblemInDB(operation);
        scoreService.calculateScore(operation.getCandidate());
    }

}
