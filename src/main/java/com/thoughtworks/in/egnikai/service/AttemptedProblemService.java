package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.AttemptedProblem;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.persistence.model.Operation;
import com.thoughtworks.in.egnikai.persistence.model.OperationStatus;
import com.thoughtworks.in.egnikai.persistence.repository.CandidateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AttemptedProblemService {
    private static final Logger logger = LoggerFactory.getLogger(AttemptedProblemService.class);

    private final CandidateRepository candidateRepository;

    @Autowired
    public AttemptedProblemService(CandidateRepository candidateRepository) {
        this.candidateRepository = candidateRepository;
    }

    public void updateAttemptedProblemInDB(Operation operation) {
        long problemId = operation.getProblem().getProblemId();
        logger.debug("Updating attempted problem with " + problemId + " operationId " + operation);
        try {
            Candidate candidate = operation.getCandidate();
            List<AttemptedProblem> attemptedProblemList = candidate.getAttemptedProblems().stream()
              .filter(predicate -> predicate.getProblem().getProblemId() == problemId).collect(Collectors.toList());
            if (attemptedProblemList.size() > 0) {
                attemptedProblemList.forEach(attemptedProblem -> attemptedProblem.setPassed(getPassedStatus(operation)));
            } else {
                addNewAttemptedProblemToCandidate(problemId, operation, candidate);
            }
            logger.debug("[candidate is]:" + candidate.toString());
            candidateRepository.save(candidate);
        } catch (Exception e) {
            logger.error("updating attempted problems operation for operation Id " + operation +
              " failed, problem Id " + problemId, e);
        }
    }

    private boolean getPassedStatus(Operation operation) {
        return OperationStatus.COMPLETED.name().equals(operation.getOperationStatus());
    }

    private void addNewAttemptedProblemToCandidate(long problemId, Operation operation, Candidate candidate) throws ResourceNotFoundException {
        logger.debug("new problem attempted {}, {} by candidate {}", problemId, operation, candidate);
        AttemptedProblem attemptedProblem = new AttemptedProblem();
        attemptedProblem.setPassed(getPassedStatus(operation));
        attemptedProblem.setProblem(operation.getProblem());
        attemptedProblem.setScore(getPassedStatus(operation) ? operation.getProblem().getCredit() : 0);
        candidate.getAttemptedProblems().add(attemptedProblem);
    }
}
