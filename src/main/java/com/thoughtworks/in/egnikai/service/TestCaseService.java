package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.dto.TestDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service
public class TestCaseService {
    private ModelMapper mapper = new ModelMapper();
    private UserService userService;
    private S3Service s3Service;
    private String codeProblemsBucket;

    @Autowired
    public TestCaseService(UserService userService,
                           S3Service s3Service,
                           @Value("${egnikai.s3.bucket.code-problems}") String codeProblemsBucket) {
        this.userService = userService;
        this.s3Service = s3Service;
        this.codeProblemsBucket = codeProblemsBucket;
    }

    public TestDTO getTest(Candidate candidate, long problemId, long testId, boolean includeHiddenTests) {
        TestCase testCase = getTestCase(candidate, problemId, testId, includeHiddenTests);
        TestDTO testDTO = new TestDTO();
        mapper.map(testCase, testDTO);
        testDTO.setTestCode(getCode(candidate.getChallenge(), testCase.getFilePath()));
        return testDTO;
    }

    public TestDTO getTest(Candidate candidate, long problemId, long testId) {
        return getTest(candidate, problemId, testId, false);
    }

    private TestCase getTestCase(Candidate candidate, long problemId, long testCaseId, boolean includeHiddenTests) {
        Problem problem = getProblem(candidate, problemId);

        return problem.getTestCases().stream()
                .filter(t -> t.getTestId() == testCaseId && (includeHiddenTests || t.getType() == TestType.PUBLIC))
                .findFirst()
                .orElseThrow(() -> new ResourceNotFoundException("Test case "+testCaseId+" not found for problem id "+ problemId));

    }

    private Problem getProblem(Candidate candidate, long problemId)  {
        return candidate.getChallenge().getProblems().stream()
                    .filter((p) -> p.getProblemId() == problemId)
                    .findFirst()
                    .orElseThrow(() -> new ResourceNotFoundException("Problem with " + problemId + "not found"));
    }

    private String getCode(Challenge challenge, String filePath) throws ResourceNotFoundException {
        return s3Service.getFileContent(codeProblemsBucket,
                format("%s/%s", challenge.getChallengeBaseS3Key(), filePath))
                .orElseThrow(() -> new ResourceNotFoundException("Test case file not present for path "+filePath));
    }
}
