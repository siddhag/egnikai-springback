INSERT INTO challenge
   (challenge_title, challenge_giturl, description, platform_name, challenge_base_s3_key)
   VALUES
   ('CSharp Challenge Set 1', '', 'CSharp challenge set comprising of chess, exceptions, expression evaluator, immutable
   collection, olympics, product catalog, right angled triangle, salary increment and zip', 'CSharp', 'CSharp/CSharp-Challenge-Set1');


-- problem Olympics --

INSERT INTO problem ( title, description, credit, optional, platform_name)
  VALUES
    ('Olympics', 'At Olympic we need to assign Rank to the countries based on the medals each country has won. Each
medal has its own score which has to be accumulated to find the total score.', 10, 0, 'CSharp');

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) FROM challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('ScoreBoard.cs', 'src.Olympics', 'src/olympics/ScoreBoard.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));



INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('ScoreBoardTest.cs', 'test.Olympics', 'test/olympics/ScoreBoardTest.cs', 1);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));






-- problem Exceptions --

INSERT INTO problem ( title, description, credit, optional, platform_name)
  VALUES  ('Exceptions', 'Should return error code in case of exception and and should clear inherited stack trace', 5, 1, 'CSharp');

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) FROM challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('StackTraceImpl.cs', 'src.Exceptions', 'src/exceptions/StackTraceImpl.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('ErraticServiceImpl.cs', 'src.Exceptions', 'src/exceptions/ErraticServiceImpl.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('MethodNotImplementedException.cs', 'src.Services', 'src/Services/MethodNotImplementedException.cs', FALSE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('IStackTrace.cs', 'src.Services', 'src/Services/IStackTrace.cs', FALSE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('IErraticService.cs', 'src.Services', 'src/Services/IErraticService.cs', FALSE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('ExceptionService.cs', 'src.Services', 'src/Services/ExceptionService.cs', FALSE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('RandomException.cs', 'src.Services', 'src/Services/RandomException.cs', FALSE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('RandomError.cs', 'src.Services', 'src/Services/RandomError.cs', FALSE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));




INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('ErraticServiceTest.cs', 'test.Exceptions', 'test/Exceptions/ErraticServiceTest.cs', 1);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('ErraticTraceTest.cs', 'test.ExceptionsTest', 'test/Exceptions/ErraticTraceTest.cs', 1);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));






-- problem RightAngleTriangle --

INSERT INTO problem ( title, description, credit, optional, platform_name)
  VALUES ('Right Angled Triangle', 'Given three coordinates of some triangle in a 2 dimensional graph, check if it forms a right-angled
triangle. Hint: Distance Between two points = sqrt of ((x1 − x2)square + (y1 − y2)square)', 10, 0, 'CSharp');

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) FROM challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('Point.cs', '﻿ src.RightAngleTriangle', 'src/rightAngleTriangle/Point.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('Triangle.cs', 'src.RightAngleTriangle', 'src/rightAngleTriangle/Triangle.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));



INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('TriangleTest.cs', 'test.RightAngleTriangle', 'test/rightAngleTriangle/TriangleTest.cs', 1);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));






-- problem Zip --

INSERT INTO problem ( title, description, credit, optional, platform_name)
  VALUES
    ('Zip', 'Build a custom Zipper for collections which can be used for both zipping and unzipping for efficient
transmission of collections.', 10, 0, 'CSharp');

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) FROM challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('Zipper.cs', 'src.Zip', 'src/Zip/Zipper.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));



INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('ZipperTest.cs', 'test.Zip', 'test/Zip/ZipperTest.cs', 1);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));






-- problem ProductCatalog --

INSERT INTO problem ( title, description, credit, optional, platform_name)
  VALUES
    ('Product Catalog', 'As a Product Catalog manager I would like to differentiate product based on their category and also
to sort them based on their price.', 10, 0, 'CSharp');


INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) FROM challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('ProductCatalog.cs', 'src.ProductCatalog', 'src/productCatalog/ProductCatalog.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('Product.cs', '﻿ src.ProductCatalog', 'src/productCatalog/Product.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));



INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('ProductCatalogTest.cs', 'test.ProductCatalog', 'test/productCatalog/ProductCatalogTest.cs', 1);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));






-- problem SalaryIncrement --

INSERT INTO problem ( title, description, credit, optional, platform_name)
  VALUES
    ('Salary Increment', 'We should increment employee salary based on their experience with a fixed decided percentage and get
the total salary of all the employees.', 5, 1, 'CSharp');

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) FROM challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('SalaryIncrementManager.cs', 'src.SalaryIncrement', 'src/salaryIncrement/SalaryIncrementManager.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('Employee.cs', 'src.SalaryIncrement', 'src/salaryIncrement/Employee.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));



INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('SalaryIncrementManagerTest.cs', 'test.SalaryIncrement', 'test/salaryIncrement/SalaryIncrementManagerTest.cs', 1);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));






-- problem Chess --

INSERT INTO problem ( title, description, credit, optional, platform_name)
  VALUES
   ('Chess', 'The N Queen is the problem of placing N chess queens on an N*N chessboard so that no two queens attack
each other. In this problem, Columns are named a-h left to right, Rows are numbered 1-8 from whites home to blacks, a1 is
a black square. For example, following is a solution for 4 Queen problem. {"d1", "g2", "c3", "h4", "b5", "e6", "a7", "f8"}.
Solve for 8*8 problem.', 5, 1, 'CSharp');

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) FROM challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('NQueensBoard.cs', 'src.Chess', 'src/chess/NQueensBoard.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));



INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('NQueenBoardTest.cs', 'test.Chess', 'test/Chess/NQueenBoardTest.cs', 1);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));






-- problem ImmutableCollection --

INSERT INTO problem ( title, description, credit, optional, platform_name)
  VALUES
    ('Immutable Collection', 'A collection once created, never changes again. No one can mess around with this collection not even
multiple threads', 5, 1, 'CSharp');

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) FROM challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('IImmutableCollection.cs', '﻿ src.ImmutableCollection', 'src/ImmutableCollection/IImmutableCollection.cs', FALSE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('ImmutableCollection.cs', 'src.ImmutableCollection', 'src/ImmutableCollection/ImmutableCollection.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));



INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('ImmutableCollectionTest.cs', 'test.ImmutableCollection', 'test/immutableCollection/ImmutableCollectionTest.cs', 1);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));






-- problem ExpressionEvaluator --

INSERT INTO problem ( title, description, credit, optional, platform_name)
  VALUES
    ('Infix And Postfix', 'A postfix expression is a collection of operators and operands in which the operator is placed after
the operands. For example [4.0 2.0 + ] = [6]. Infix is already solved as an example for you.', 5, 1, 'CSharp');

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) FROM challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('Operator.cs', 'src.ExpressionEvaluator', 'src/expressionEvaluator/Operator.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('InvalidExpressionException.cs', 'src.ExpressionEvaluator', 'src/expressionEvaluator/InvalidExpressionException.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('NumberCalculator.cs', 'src.ExpressionEvaluator', 'src/expressionEvaluator/NumberCalculator.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('PostfixCalculator.cs', 'src.ExpressionEvaluator', 'src/expressionEvaluator/PostfixCalculator.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('InfixCalculator.cs', 'src.ExpressionEvaluator', 'src/expressionEvaluator/InfixCalculator.cs', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));



INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('PostfixCalculatorTest.cs', 'test.ExpressionEvaluator', 'test/expressionEvaluator/PostfixCalculatorTest.cs', 1);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('InfixCalculatorTest.cs', 'test.ExpressionEvaluator', 'test/expressionEvaluator/InfixCalculatorTest.cs', 1);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));




COMMIT;


