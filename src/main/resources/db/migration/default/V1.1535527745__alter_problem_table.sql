ALTER TABLE `problem`
ADD COLUMN (`platform_name` varchar(255) DEFAULT 'Java' NOT NULL,
            `complexity_score_id` int(2) DEFAULT 1 NOT NULL),
ADD CONSTRAINT `FK_problem_complexity_score` FOREIGN KEY (`complexity_score_id`) REFERENCES `complexity_score` (`complexity_score_id`);

UPDATE challenge c, challenge_problems cp, problem p
SET p.platform_name = c.platform_name
where c.challenge_id = cp.challenge_challenge_id
and cp.problems_problem_id = p.problem_id;
