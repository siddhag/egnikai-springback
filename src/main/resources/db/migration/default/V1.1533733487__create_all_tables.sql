CREATE TABLE `user` (
  `user_id` varchar(255) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `challenge` (
  `challenge_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `challenge_giturl` varchar(255) DEFAULT NULL,
  `challenge_title` varchar(255) DEFAULT NULL,
  `description` longtext,
  `platform_name` varchar(255) DEFAULT NULL,
  `challenge_base_s3_key` varchar(200) NOT NULL,
  PRIMARY KEY (`challenge_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `source` (
  `source_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `is_editable` bit(1) NOT NULL,
  `source_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`source_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `test` (
  `test_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `test_file_name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`test_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE `problem` (
  `problem_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `credit` int(11) NOT NULL,
  `description` longtext,
  `optional` bit(1) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`problem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `problem_source_codes` (
  `problem_problem_id` bigint(20) NOT NULL,
  `source_codes_source_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_6kyne6v3r4wrolsr6d4p89f5f` (`source_codes_source_id`),
  KEY `FKfbp2nd2nvjblnycqqs02r2cac` (`problem_problem_id`),
  CONSTRAINT `FKc6kt6694brng24sy6y3uuhlfn` FOREIGN KEY (`source_codes_source_id`) REFERENCES `source` (`source_id`),
  CONSTRAINT `FKfbp2nd2nvjblnycqqs02r2cac` FOREIGN KEY (`problem_problem_id`) REFERENCES `problem` (`problem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `problem_test_cases` (
  `problem_problem_id` bigint(20) NOT NULL,
  `test_cases_test_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_9j8t2ax43cnjpxgnjcx5xtfuw` (`test_cases_test_id`),
  KEY `FKhquxvum0vdpewd8fqnt42fh0j` (`problem_problem_id`),
  CONSTRAINT `FKhquxvum0vdpewd8fqnt42fh0j` FOREIGN KEY (`problem_problem_id`) REFERENCES `problem` (`problem_id`),
  CONSTRAINT `FKioywh25cbbyex8w0bqxei2i22` FOREIGN KEY (`test_cases_test_id`) REFERENCES `test` (`test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `challenge_problems` (
  `challenge_challenge_id` bigint(20) NOT NULL,
  `problems_problem_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_o0xjy01c1mofhqbo4uc0vbrqi` (`problems_problem_id`),
  KEY `FKe5nhxbfldgrvoqw60w32iopfy` (`challenge_challenge_id`),
  CONSTRAINT `FKe5nhxbfldgrvoqw60w32iopfy` FOREIGN KEY (`challenge_challenge_id`) REFERENCES `challenge` (`challenge_id`),
  CONSTRAINT `FKickej712ob7lqolldh1bua58o` FOREIGN KEY (`problems_problem_id`) REFERENCES `problem` (`problem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `candidate` (
  `candidate_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `candidate_git_branch` varchar(255) DEFAULT NULL,
  `challenge_date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `event_id` varchar(255) DEFAULT NULL,
  `is_active` bit(1) NOT NULL,
  `is_locked` bit(1) NOT NULL,
  `is_passed` bit(1) NOT NULL,
  `score` int(11) NOT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `challenge_id` bigint(20) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`candidate_id`),
  KEY `FK2ywyebvy95r8obytk0dku8tvh` (`challenge_id`),
  KEY `FKj9h7beyp5gsdtdb20km82b4fl` (`user_id`),
  CONSTRAINT `FK2ywyebvy95r8obytk0dku8tvh` FOREIGN KEY (`challenge_id`) REFERENCES `challenge` (`challenge_id`),
  CONSTRAINT `FKj9h7beyp5gsdtdb20km82b4fl` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

CREATE TABLE `attempted_problem` (
  `attempted_problem_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `is_passed` bit(1) NOT NULL,
  `score` int(11) NOT NULL,
  `problem_problem_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`attempted_problem_id`),
  KEY `FK2tgdkc4d11nkbtad5ej9h6ryb` (`problem_problem_id`),
  CONSTRAINT `FK2tgdkc4d11nkbtad5ej9h6ryb` FOREIGN KEY (`problem_problem_id`) REFERENCES `problem` (`problem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `candidate_attempted_problems` (
  `candidate_candidate_id` bigint(20) NOT NULL,
  `attempted_problems_attempted_problem_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_24hxv6r4hgtgouqy47jysl6fy` (`attempted_problems_attempted_problem_id`),
  KEY `FKffilduyujiqgofvct5d69q1p4` (`candidate_candidate_id`),
  CONSTRAINT `FKffilduyujiqgofvct5d69q1p4` FOREIGN KEY (`candidate_candidate_id`) REFERENCES `candidate` (`candidate_id`),
  CONSTRAINT `FKrkotnh3lhcwcse7oj3okmkyi3` FOREIGN KEY (`attempted_problems_attempted_problem_id`) REFERENCES `attempted_problem` (`attempted_problem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `operation` (
  `operation_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date_time` datetime DEFAULT NULL,
  `error_information` MEDIUMTEXT,
  `operation_name` varchar(255) DEFAULT NULL,
  `operation_status` varchar(255) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `candidate_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`operation_id`),
  KEY `FKt88tj934jhmpqlybog1x8r5th` (`candidate_id`),
  CONSTRAINT `FKt88tj934jhmpqlybog1x8r5th` FOREIGN KEY (`candidate_id`) REFERENCES `candidate` (`candidate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `candidate_operations` (
  `candidate_candidate_id` bigint(20) NOT NULL,
  `operations_operation_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_8q891dpmjalarl451o86c5rhu` (`operations_operation_id`),
  KEY `FK1td7pby03m993c4x252nq07qw` (`candidate_candidate_id`),
  CONSTRAINT `FK1td7pby03m993c4x252nq07qw` FOREIGN KEY (`candidate_candidate_id`) REFERENCES `candidate` (`candidate_id`),
  CONSTRAINT `FKhw3km8rgbw3l95iee042i5d7e` FOREIGN KEY (`operations_operation_id`) REFERENCES `operation` (`operation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
