CREATE TABLE `complexity_score` (
  `complexity_score_id` int(2) NOT NULL AUTO_INCREMENT,
  `complexity` varchar(10) NOT NULL,
  `score` int(10) NOT NULL,
  PRIMARY KEY (`complexity_score_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `complexity_score` values (1, "Easy", 10);
INSERT INTO `complexity_score` values (2, "Medium", 20);
INSERT INTO `complexity_score` values (3, "Hard", 30);
