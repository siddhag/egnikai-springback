INSERT INTO challenge
   (challenge_title, challenge_giturl, description, platform_name, challenge_base_s3_key)
   VALUES
   ('Java 8 Challenge Set 1', '', 'Java challenge set comprising of chess, exceptions, expression evaluator, immutable
   collection, olympics, product catalog, right angled triangle, salary increment and zip', 'Java', 'java/Java-Recruitment-Challenge-Set1');

-- Problem 1
INSERT INTO problem ( title, description, credit, optional)
   VALUES
   ('Chess', 'The N Queen is the problem of placing N chess queens on an N*N chessboard so that no two queens attack
each other. In this problem, Columns are named a-h left to right, Rows are numbered 1-8 from whites home to blacks, a1 is
a black square. For example, following is a solution for 4 Queen problem. {"d1", "g2", "c3", "h4", "b5", "e6", "a7", "f8"}.
Solve for 8*8 problem.', 5, 1);

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) from challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('NQueensBoard.java', 'P1.NQueensBoard', 'src/main/java/P1/NQueensBoard.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('NQueensBoardTest.java', 'P1.NQueensBoardTest', 'src/test/java/P1/NQueensBoardTest.java', 1);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('NQueensBoardHiddenTest.java', 'P1.NQueensBoardHiddenTest', 'src/test/java/P1/NQueensBoardHiddenTest.java', 0);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

-- Problem 2

INSERT INTO problem (title, description, credit, optional)
   VALUES
   ('Exceptions', 'Should return error code in case of exception and and should clear inherited stack trace', 5, 1);
INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) from challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source (source_name, class_name, file_path, is_editable)
   VALUES ('ErraticService.java', 'P2.ErraticService', 'src/main/java/P2/ErraticService.java', TRUE);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('ErraticServiceImpl.java', 'P2.ErraticServiceImpl', 'src/main/java/P2/ErraticServiceImpl.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('ExceptionService.java', 'P2.ExceptionService', 'src/main/java/P2/ExceptionService.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('RandomError.java', 'P2.RandomError', 'src/main/java/P2/RandomError.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('RandomException.java', 'P2.RandomException', 'src/main/java/P2/RandomException.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('StackTrace.java', 'P2.StackTrace', 'src/main/java/P2/StackTrace.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('StackTraceImpl.java', 'P2.StackTraceImpl', 'src/main/java/P2/StackTraceImpl.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('ErraticServiceTest.java', 'P2.ErraticServiceTest', 'src/test/java/P2/ErraticServiceTest.java', 1);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('StackTraceTest.java', 'P2.StackTraceTest', 'src/test/java/P2/StackTraceTest.java', 1);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

-- Problem 3
INSERT INTO problem (title, description, credit, optional)
    VALUES
    ('Infix And Postfix', 'A postfix expression is a collection of operators and operands in which the operator is placed after
the operands. For example [4.0 2.0 + ] = [6]. Infix is already solved as an example for you.', 5, 1);

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) from challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('InfixCalculator.java', 'P3.InfixCalculator', 'src/main/java/P3/InfixCalculator.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));


INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('InvalidExpressionException.java', 'P3.InvalidExpressionException', 'src/main/java/P3/InvalidExpressionException.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('NumberCalculator.java', 'P3.NumberCalculator', 'src/main/java/P3/NumberCalculator.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('Operator.java', 'P3.Operator', 'src/main/java/P3/Operator.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('PostfixCalculator.java', 'P3.PostfixCalculator', 'src/main/java/P3/PostfixCalculator.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('InfixCalculatorTest.java', 'P3.InfixCalculatorTest', 'src/test/java/P3/InfixCalculatorTest.java', 1);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('PostfixCalculatorTest.java', 'P3.PostfixCalculatorTest', 'src/test/java/P3/PostfixCalculatorTest.java', 1);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('PostfixCalculatorHiddenTest.java', 'P3.PostfixCalculatorHiddenTest', 'src/test/java/P3/PostfixCalculatorHiddenTest.java', 0);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

-- Problem 4

INSERT INTO problem (title, description, credit, optional)
    VALUES
    ('Immutable Collection', 'A collection once created, never changes again. No one can mess around with this collection not even
multiple threads', 5, 1);

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) from challenge), (SELECT MAX(problem_id) from problem));


INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('ImmutableCollection.java', 'P4.ImmutableCollection', 'src/main/java/P4/ImmutableCollection.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('ImmutableCollectionImpl.java', 'P4.ImmutableCollectionImpl', 'src/main/java/P4/ImmutableCollectionImpl.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('MethodNotImplementedException.java', 'P4.MethodNotImplementedException', 'src/main/java/P4/MethodNotImplementedException.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('ImmutableCollectionTest.java', 'P4.ImmutableCollectionTest', 'src/test/java/P4/ImmutableCollectionTest.java', 1);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('ImmutableCollectionHiddenTest.java', 'P4.ImmutableCollectionHiddenTest', 'src/test/java/P4/ImmutableCollectionHiddenTest.java', 0);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

-- Problem 5

INSERT INTO problem (title, description, credit, optional)
    VALUES
    ('Olympics', 'At Olympic we need to assign Rank to the countries based on the medals each country has won. Each
medal has its own score which has to be accumulated to find the total score.', 10, 0);

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) from challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('ScoreBoard.java', 'P5.ScoreBoard', 'src/main/java/P5/ScoreBoard.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('ScoreBoardTest.java', 'P5.ScoreBoardTest', 'src/test/java/P5/ScoreBoardTest.java', 1);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('ScoreBoardHiddenTest.java', 'P5.ScoreBoardHiddenTest', 'src/test/java/P5/ScoreBoardHiddenTest.java', 0);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));


-- Problem 6

INSERT INTO problem (title, description, credit, optional)
    VALUES
    ('Product Catalog', 'As a Product Catalog manager I would like to differentiate product based on their category and also
to sort them based on their price.', 10, 0);

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) from challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('Product.java', 'P6.Product', 'src/main/java/P6/Product.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('ProductCatalog.java', 'P6.ProductCatalog', 'src/main/java/P6/ProductCatalog.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('ProductCatalogTest.java', 'P6.ProductCatalogTest', 'src/test/java/P6/ProductCatalogTest.java', 1);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('ProductCatalogHiddenTest.java', 'P6.ProductCatalogHiddenTest', 'src/test/java/P6/ProductCatalogHiddenTest.java', 0);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

-- Problem 7

INSERT INTO problem (title, description, credit, optional)
    VALUES
    ('Right Angled Triangle', 'Given three coordinates of some triangle in a 2 dimensional graph, check if it forms a right-angled
triangle. Hint: Distance Between two points = sqrt of ((x1 − x2)square + (y1 − y2)square)', 10, 0);

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) from challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('MethodNotImplementedException.java', 'P7.MethodNotImplementedException', 'src/main/java/P7/MethodNotImplementedException.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));


INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('Point.java', 'P7.Point', 'src/main/java/P7/Point.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('Triangle.java', 'P7.Triangle', 'src/main/java/P7/Triangle.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('TriangleTest.java', 'P7.TriangleTest', 'src/test/java/P7/TriangleTest.java', 1);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('TriangleHiddenTest.java', 'P7.TriangleHiddenTest', 'src/test/java/P7/TriangleHiddenTest.java', 0);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

-- Problem 8

INSERT INTO problem (title, description, credit, optional)
    VALUES
    ('Salary Increment', 'We should increment employee salary based on their experience with a fixed decided percentage and get
the total salary of all the employees.', 5, 1);

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) from challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('Employee.java', 'P8.Employee', 'src/main/java/P8/Employee.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('SalaryIncrementManager.java', 'P8.SalaryIncrementManager', 'src/main/java/P8/SalaryIncrementManager.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('SalaryIncrementManagerTest.java', 'P8.SalaryIncrementManagerTest', 'src/test/java/P8/SalaryIncrementManagerTest.java', 1);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('SalaryIncrementManagerHiddenTest.java', 'P8.SalaryIncrementManagerHiddenTest', 'src/test/java/P8/SalaryIncrementManagerHiddenTest.java',0);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

-- Problem 9

INSERT INTO problem (title, description, credit, optional)
    VALUES
    ('Zip', 'Build a custom Zipper for collections which can be used for both zipping and unzipping for efficient
transmission of collections.', 10, 0);

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id) VALUES ((SELECT MAX(challenge_id) from challenge), (SELECT MAX(problem_id) from problem));

INSERT INTO source ( source_name, class_name, file_path, is_editable)
   VALUES ('Zipper.java', 'P9.Zipper', 'src/main/java/P9/Zipper.java', TRUE);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(source_id) from source));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('ZipperTest.java', 'P9.ZipperTest', 'src/test/java/P9/ZipperTest.java',1);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

INSERT INTO test (test_file_name, class_name, file_path, type)
   VALUES ('ZipperHiddenTest.java', 'P9.ZipperHiddenTest', 'src/test/java/P9/ZipperHiddenTest.java',0);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES ((SELECT MAX(problem_id) from problem), (SELECT MAX(test_id) from test));

COMMIT;