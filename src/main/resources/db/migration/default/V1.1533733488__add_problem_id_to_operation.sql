ALTER TABLE operation ADD problem_id bigint NULL;
ALTER TABLE operation
ADD CONSTRAINT operation_problem_fk
FOREIGN KEY (problem_id) REFERENCES problem (problem_id);