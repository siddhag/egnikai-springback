INSERT INTO challenge
   (challenge_id, challenge_title, challenge_giturl, description, platform_name, challenge_base_s3_key)
   VALUES
   (1, 'Java 8 Sample', 'git@bitbucket.org:siddhag/egnikai-java-challenge-sample.git', 'This is a sample egnikai challenge for the Java 8 Platform', 'Java', 'java/egnikai-java-challenge-sample');

INSERT INTO problem (problem_id, title, description, credit, optional)
   VALUES
   (1, 'Prime Number Checker', 'Given a number checks whether it is prime or not.', 10, 0);

INSERT INTO problem (problem_id, title, description, credit, optional)
   VALUES
   (2, 'Number Sum', 'Given two numbers, adds them up.', 5, 1);

INSERT INTO problem (problem_id, title, description, credit, optional)
    VALUES
    (3, 'Sum Generator', 'Given two numbers, adds them up.', 10, 0);
INSERT INTO problem (problem_id, title, description, credit, optional)
    VALUES
    (4, 'Nth Prime Calculator', 'Give a integer N this problem identifies the Nth prime number ', 10, 0);

INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id)
   VALUES (1, 1);
INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id)
   VALUES (1, 2);
INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id)
   VALUES (1, 3);
INSERT INTO challenge_problems (challenge_challenge_id, problems_problem_id)
   VALUES (1, 4);

INSERT INTO source (source_id, source_name, class_name, file_path, is_editable)
   VALUES (1, 'PrimeChecker.java', 'P1.PrimeChecker', 'src/main/java/P1/PrimeChecker.java', TRUE);

INSERT INTO source (source_id, source_name, class_name, file_path, is_editable)
   VALUES (2, 'NumericAdder.java', 'P2.NumericAdder', 'src/main/java/P2/NumericAdder.java', TRUE);

INSERT INTO source (source_id, source_name, class_name, file_path, is_editable)
   VALUES (3, 'Adder.java', 'P3.Adder', 'src/main/java/P3/Adder.java', TRUE);

INSERT INTO source (source_id, source_name, class_name, file_path, is_editable)
   VALUES (4, 'NthPrime.java', 'P4.NthPrime', 'src/main/java/P4/NthPrime.java', TRUE);

INSERT INTO test (test_id, test_file_name, class_name, file_path, type)
   VALUES (1, 'PrimeCheckerTest.java', 'P1.PrimeCheckerTest', 'src/test/java/P1/PrimeCheckerTest.java', 1);
INSERT INTO test (test_id, test_file_name, class_name, file_path, type)
   VALUES (2, 'PrimeCheckerHiddenTest.java', 'P1.PrimeCheckerHiddenTest', 'src/test/java/P1/PrimeCheckerHiddenTest.java', 0);
INSERT INTO test (test_id, test_file_name, class_name, file_path, type)
   VALUES (3, 'NumericAdderTest.java', 'P2.NumericAdderTest', 'src/test/java/P2/NumericAdderTest.java', 1);
INSERT INTO test (test_id, test_file_name, class_name, file_path, type)
      VALUES (4, 'AdderTest.java', 'P3.AdderTest', 'src/test/java/P3/AdderTest.java', 1);
INSERT INTO test (test_id, test_file_name, class_name, file_path, type)
   VALUES (5, 'NthPrimeTest.java', 'P4.NthPrimeTest', 'src/test/java/P4/NthPrimeTest.java', 1);
INSERT INTO test (test_id, test_file_name, class_name, file_path, type)
   VALUES (6, 'NthPrimeHiddenTest.java', 'P4.NthPrimeHiddenTest', 'src/test/java/P4/NthPrimeHiddenTest.java', 0);

INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES (1, 1);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES (2, 2);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES (3, 3);
INSERT INTO problem_source_codes (problem_problem_id, source_codes_source_id) VALUES (4, 4);

INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES (1, 1);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES (1, 2);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES (2, 3);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES (3, 4);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES (4, 5);
INSERT INTO problem_test_cases (problem_problem_id, test_cases_test_id) VALUES (4, 6);


INSERT INTO `user` VALUES ('admin',1,'admin','ADMIN','admin');
INSERT INTO `user` VALUES ('user',1,'pass','ADMIN','admin');

INSERT INTO `user` VALUES ('testuser', 1, 'testuser', 'CANDIDATE', 'Test User');
INSERT INTO `user` VALUES ('pai', 1, 'pai', 'CANDIDATE', 'pai');
INSERT INTO `user` VALUES ('harshitha', 1, 'harshitha', 'CANDIDATE', 'harshitha');
INSERT INTO `user` VALUES ('jijo', 1, 'jijo', 'CANDIDATE', 'jijo');
INSERT INTO `user` VALUES ('megha', 1, 'megha', 'CANDIDATE', 'megha');
INSERT INTO `user` VALUES ('sethu', 1, 'sethu', 'CANDIDATE', 'sethu');
INSERT INTO `user` VALUES ('sam', 1, 'sam', 'CANDIDATE', 'sam');
INSERT INTO `user` VALUES ('deb', 1, 'deb', 'CANDIDATE', 'deb');

INSERT INTO candidate VALUES(4, '', '2012-12-12','2012-12-12', 'dev-event', TRUE,FALSE, 0,0,'2012-12-12', 1, 'testuser');
INSERT INTO candidate VALUES(5, '', '2012-12-12','2012-12-12', 'dev-event', TRUE,FALSE, 0,0,'2012-12-12', 1, 'pai');
INSERT INTO candidate VALUES(2, '', '2012-12-12','2012-12-12', 'dev-event', TRUE,FALSE, 0,0,'2012-12-12', 1, 'harshitha');
INSERT INTO candidate VALUES(7, '', '2012-12-12','2012-12-12', 'dev-event', TRUE,FALSE, 0,0,'2012-12-12', 1, 'jijo');
INSERT INTO candidate VALUES(8, '', '2012-12-12','2012-12-12', 'dev-event', TRUE,FALSE, 0,0,'2012-12-12', 1, 'megha');
INSERT INTO candidate VALUES(9, '', '2012-12-12','2012-12-12', 'dev-event', TRUE,FALSE, 0,0,'2012-12-12', 1, 'sethu');
INSERT INTO candidate VALUES(6, '', '2012-12-12','2012-12-12', 'dev-event', TRUE,FALSE, 0,0,'2012-12-12', 1, 'sam');
INSERT INTO candidate VALUES(1, '', '2012-12-12','2012-12-12', 'dev-event', TRUE,FALSE, 0,0,'2012-12-12', 1, 'deb');


Commit;