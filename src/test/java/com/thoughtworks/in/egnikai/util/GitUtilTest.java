package com.thoughtworks.in.egnikai.util;

import org.junit.Assert;
import org.junit.Test;

public class GitUtilTest {

    @Test
    public void shouldReturnHttpsBranchURL() {
        Assert.assertEquals("https://bitbucket.org/siddhag/egnikai-springback.git/src/cicd_spike_1",
                GitUtil.getHTTPSBranchURL("cicd_spike_1", "git@bitbucket.org:siddhag/egnikai-springback.git"));
    }
}
