package com.thoughtworks.in.egnikai.util;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class PatternMatcherTest {
    private PatternMatcher patternMatcher;

    @Before
    public void setUp() {
        patternMatcher = new PatternMatcher();
    }

    @Test
    public void shouldReturnTrueWhenThePatternIsMatched() {
        Optional<String> actualPattern = patternMatcher.checkForAbusivePatterns(Arrays.asList(Pattern.compile(".*System\\..*")), getMultiLineComment(),
                getCommentPattern(), "System.exit(0);");
        assertEquals("System.exit(0);", actualPattern.get());
    }

    @Test
    public void shouldReturnFalseWhenThePatternIsNotMatched() {
        Optional<String> actualPattern = patternMatcher.checkForAbusivePatterns(Arrays.asList(Pattern.compile("exit")), getMultiLineComment(),
                getCommentPattern(), "public class NumericAdder{\n" +
                        "    public int add(int a, int b) {\n" +
                        "        return c;\n" +
                        "    }\n" +
                        "}");
        assertFalse(actualPattern.isPresent());
    }

    @Test
    public void shouldReturnTrueWhenOneOfThePatternsIsMatched() {
        Optional<String> actual = patternMatcher.checkForAbusivePatterns(getAbusiveCodePatterns(), getCommentPattern(), getMultiLineComment(),
                "//Thread.sleep(100); \nnew File();");
        assertEquals("new File();", actual.get());
    }

    @Test
    public void shouldReturnTrueWhenOneOfThePatternsIsMatched1() {
        Optional<String> actual = patternMatcher.checkForAbusivePatterns(getAbusiveCodePatterns(), getCommentPattern(), getMultiLineComment(),
                "package P1;\n" +
                        "\n" +
                        "public class Adder {\n" +
                        "    public void add(int a, int b) {\n" +
                        "new File();\n" +
                        "        return 0;\n" +
                        "    }\n" +
                        "}");
        assertEquals("new File();", actual.get());
    }

    @Test
    public void shouldIgnoreSingleLineComment() {
        Optional<String> actual = patternMatcher.checkForAbusivePatterns(getAbusiveCodePatterns(), getCommentPattern(), getMultiLineComment(),
                "//Thread.sleep(10);");
        assertFalse(actual.isPresent());

    }

    @Test
    public void shouldIgnoreMultiLineComment() {
        Optional<String> actual = patternMatcher.checkForAbusivePatterns(getAbusiveCodePatterns(), getCommentPattern(), getMultiLineComment(),
                "/*Thread.sleep(10);*/");
        assertFalse(actual.isPresent());

    }

    private List<Pattern> getAbusiveCodePatterns() {
        List<Pattern> patterns = new ArrayList<>();
        patterns.add(Pattern.compile(".*System\\..*"));
        patterns.add(Pattern.compile(".*implements Runnable.*"));
        patterns.add(Pattern.compile(".*extends Thread.*"));
        patterns.add(Pattern.compile(".*Process.*"));
        patterns.add(Pattern.compile(".*Thread.*"));
        patterns.add(Pattern.compile(".*exec\\(.*"));
        patterns.add(Pattern.compile(".*new File\\(.*\\).*"));
        patterns.add(Pattern.compile(".*import java\\.net.*"));
        patterns.add(Pattern.compile(".*import java\\.io.*"));
        return patterns;
    }

    private String getCommentPattern() {
        return "[\\s]*//.*";
    }

    private String getMultiLineComment() {
        return "(?s)\\s*/\\*.*\\*/";
    }
}