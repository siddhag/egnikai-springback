package com.thoughtworks.in.egnikai.util;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

public class FileUtilServiceTest {

    public static final String CONTENT = "this is content";

    @Test
    public void shouldWriteToFile() throws IOException {
        File tempFile = File.createTempFile("test", ".test");
        tempFile.deleteOnExit();

        new FileUtilService().writeToFile(tempFile, CONTENT);
        String readFileContents = FileUtils.readFileToString(tempFile, Charset.defaultCharset());
        assertThat(readFileContents).isEqualTo(CONTENT);
    }

    @Test
    public void shouldCreateTempDirectory() throws IOException {
        Path tempDirectory = new FileUtilService().createTempDirectory();
        assertThat(tempDirectory.toFile().isDirectory()).isTrue();
    }

}
