package com.thoughtworks.in.egnikai.util;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;
public class ErrorParserUtilTest {
    @Test
    public void shouldReturnTestCaseName(){
        ErrorParserUtil.javaFailedTestCasesErrorParser("JUnit version 4.12\n" +
                ".hello\n" +
                "..E.E.E.E.E\n" +
                "Time: 0.008\n" +
                "There were 5 failures:\n" +
                "1) testRnaTranscriptionOfThymineIsAdenine(RnaTranscriptionTest)\n" +
                "java.lang.AssertionError: expected:<A> but was:<null>\n" +
                "    at org.junit.Assert.fail(Assert.java:88)\n" +
                "    at org.junit.Assert.failNotEquals(Assert.java:834)\n" +
                "    at org.junit.Assert.assertEquals(Assert.java:118)\n" +
                "    at org.junit.Assert.assertEquals(Assert.java:144)\n" +
                "    at RnaTranscriptionTest.testRnaTranscriptionOfThymineIsAdenine(RnaTranscriptionTest.java:28)\n" +
                "    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n" +
                "    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n" +
                "    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n" +
                "    at java.lang.reflect.Method.invoke(Method.java:498)\n" +
                "    at org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:50)\n" +
                "    at org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12)\n" +
                "    at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:47)\n" +
                "    at org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)\n" +
                "    at org.junit.internal.runners.statements.RunBefores.evaluate(RunBefores.java:26)\n" +
                "    at org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:325)\n" +
                "    at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:78)\n" +
                "    at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:57)\n" +
                "    at org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\n" +
                "    at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\n" +
                "    at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\n" +
                "    at org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\n" +
                "    at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\n" +
                "    at org.junit.runners.ParentRunner.run(ParentRunner.java:363)\n" +
                "    at org.junit.runners.Suite.runChild(Suite.java:128)\n" +
                "    at org.junit.runners.Suite.runChild(Suite.java:27)\n" +
                "    at org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\n" +
                "    at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\n" +
                "    at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\n" +
                "    at org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\n" +
                "    at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\n" +
                "    at org.junit.runners.ParentRunner.run(ParentRunner.java:363)\n" +
                "    at org.junit.runner.JUnitCore.run(JUnitCore.java:137)\n" +
                "    at org.junit.runner.JUnitCore.run(JUnitCore.java:115)\n" +
                "    at org.junit.runner.JUnitCore.runMain(JUnitCore.java:77)\n" +
                "    at org.junit.runner.JUnitCore.main(JUnitCore.java:36)\n" +
                "2) testRnaTranscription(RnaTranscriptionTest)\n" +
                "java.lang.AssertionError: expected:<UGCACCAGAAUU> but was:<null>\n" +
                "    at org.junit.Assert.fail(Assert.java:88)\n" +
                "    at org.junit.Assert.failNotEquals(Assert.java:834)\n" +
                "    at org.junit.Assert.assertEquals(Assert.java:118)\n" +
                "    at org.junit.Assert.assertEquals(Assert.java:144)\n" +
                "    at RnaTranscriptionTest.testRnaTranscription(RnaTranscriptionTest.java:38)\n" +
                "    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n" +
                "    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n" +
                "    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n" +
                "    at java.lang.reflect.Method.invoke(Method.java:498)\n" +
                "    at org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:50)\n" +
                "    at org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12)\n" +
                "    at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:47)\n" +
                "    at org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)\n" +
                "    at org.junit.internal.runners.statements.RunBefores.evaluate(RunBefores.java:26)\n" +
                "    at org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:325)\n" +
                "    at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:78)\n" +
                "    at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:57)\n" +
                "    at org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\n" +
                "    at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\n" +
                "    at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\n" +
                "    at org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\n" +
                "    at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\n" +
                "    at org.junit.runners.ParentRunner.run(ParentRunner.java:363)\n" +
                "    at org.junit.runners.Suite.runChild(Suite.java:128)\n" +
                "    at org.junit.runners.Suite.runChild(Suite.java:27)\n" +
                "    at org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\n" +
                "    at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\n" +
                "    at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\n" +
                "    at org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\n" +
                "    at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\n" +
                "    at org.junit.runners.ParentRunner.run(ParentRunner.java:363)\n" +
                "    at org.junit.runner.JUnitCore.run(JUnitCore.java:137)\n" +
                "    at org.junit.runner.JUnitCore.run(JUnitCore.java:115)\n" +
                "    at org.junit.runner.JUnitCore.runMain(JUnitCore.java:77)\n" +
                "    at org.junit.runner.JUnitCore.main(JUnitCore.java:36)", "RnaTranscriptionTest.java");
    }
    @Test
    public void shouldReturnRunTimeError(){
        ErrorParserUtil.javaFailedTestCasesErrorParser("java.lang.NullPointerException\n" +
                "    at Maze.getNumRandOccupants(Maze.java:118)\n" +
                "    at P4TestDriver.testMaze(P4TestDriver.java:995)\n" +
                "    at P4TestDriver.main(P4TestDriver.java:116)\n" +
                "    at __SHELL8.run(__SHELL8.java:7)\n" +
                "    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n" +
                "    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)\n" +
                "    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)\n" +
                "    at java.lang.reflect.Method.invoke(Method.java:597)\n" +
                "    at bluej.runtime.ExecServer$3.run(ExecServer.java:814)\n" +
                "java.lang.NullPointerException\n" +
                "    at Maze.addRandomOccupant(Maze.java:130)\n" +
                "    at P4TestDriver.testMazeReadWrite(P4TestDriver.java:1071)\n" +
                "    at P4TestDriver.main(P4TestDriver.java:127)\n" +
                "    at __SHELL8.run(__SHELL8.java:7)\n" +
                "    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n" +
                "    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)\n" +
                "    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)\n" +
                "    at java.lang.reflect.Method.invoke(Method.java:597)\n" +
                "    at bluej.runtime.ExecServer$3.run(ExecServer.java:814)", "Maze.java");
    }
}

