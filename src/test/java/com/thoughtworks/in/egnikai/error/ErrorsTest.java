package com.thoughtworks.in.egnikai.error;

import org.junit.Test;

import static com.thoughtworks.in.egnikai.error.ErrorConstants.*;
import static com.thoughtworks.in.egnikai.error.Errors.resourceNotFoundException;
import static com.thoughtworks.in.egnikai.error.Errors.unAuthorizedAccess;
import static org.assertj.core.api.Assertions.assertThat;

public class ErrorsTest {

    @Test
    public void shouldReturnUnauthorizedAccessErrorObject() {
        assertThat(unAuthorizedAccess().errors.get(0).getStatus()).isEqualTo(HTTP_CODE_403);
        assertThat(unAuthorizedAccess().errors.get(0).getCode()).isEqualTo(ERROR_CODE_403);
    }

    @Test
    public void shouldReturnResourceNotFoundErrorObject() {
        assertThat(resourceNotFoundException().errors.get(0).getStatus()).isEqualTo(HTTP_CODE_404);
        assertThat(resourceNotFoundException().errors.get(0).getCode()).isEqualTo(ERROR_CODE_404);
    }
}