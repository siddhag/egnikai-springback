package com.thoughtworks.in.egnikai.controller;

import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.exception.TimeOutException;
import com.thoughtworks.in.egnikai.service.ProblemService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ProblemControllerTest {
    @Mock
    ProblemService problemService;

    @Test
    public void shouldUpdateSourceFile() throws ForbiddenResourceException, ResourceNotFoundException, TimeOutException, IOException {
        ProblemController problemController = new ProblemController(problemService);
        problemController.updateSourceFile("1", "1", "this is content");
        verify(problemService).saveProblem(1, 1, "this is content");
    }

    @Test
    public void shouldInvokeGetProblem() throws ForbiddenResourceException, ResourceNotFoundException{
        ProblemController problemController = new ProblemController(problemService);
        problemController.getProblem(1);
        verify(problemService).getProblem(1);
    }
}