package com.thoughtworks.in.egnikai.controller;

import com.thoughtworks.in.egnikai.dto.SourceDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.service.CandidateService;
import com.thoughtworks.in.egnikai.service.SourceCodeService;
import com.thoughtworks.in.egnikai.service.UserService;
import com.thoughtworks.in.egnikai.testHelpers.CandidateTestHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SourceCodeControllerTest {
    @Mock
    private SourceCodeService sourceCodeService;
    @Mock
    private UserService userService;
    @Mock
    private CandidateService candidateService;

    @Test
    public void shouldGetSourceForProblem() throws IOException, ResourceNotFoundException, ForbiddenResourceException {
        Candidate candidate = CandidateTestHelper.setUpCandidate("java");
        SourceCodeController controller = new SourceCodeController(sourceCodeService, userService, candidateService);
        when(userService.isCandidate(any())).thenReturn(true);
        when(candidateService.getCurrentCandidate()).thenReturn(candidate);
        when(sourceCodeService.getSourceCode(candidate, (long) 2, (long) 1)).thenReturn(new SourceDTO());
        controller.getSource((long) 2, (long) 1);
        verify(sourceCodeService).getSourceCode(candidate, (long) 2, (long) 1);
    }
}
