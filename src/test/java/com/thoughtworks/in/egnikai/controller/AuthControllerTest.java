package com.thoughtworks.in.egnikai.controller;

import com.thoughtworks.in.egnikai.dto.LogoutDTO;
import com.thoughtworks.in.egnikai.dto.UserDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.User;
import com.thoughtworks.in.egnikai.service.CandidateService;
import com.thoughtworks.in.egnikai.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.test.util.ReflectionTestUtils;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuthControllerTest {

    private AuthController authController;
    private Authentication authentication;
    private SecurityContext securityContext;
    private SecurityContextLogoutHandler securityContextLogoutHandler;
    private SecurityContextHolder mockedSecurityContextHolder;
    private CandidateService candidateService;


    private UserService userService;

    private org.springframework.security.core.userdetails.User springSecurityUser;

    @Before
    public void setUp() throws ResourceNotFoundException {
        userService = mock(UserService.class);
        candidateService=mock(CandidateService.class);

        authController = new AuthController(userService, candidateService);

        User user = new User();
        user.setUserId("userId");
        user.setEnabled(true);
        user.setPassword("password");
        user.setRole("user");
        user.setUserName("userName");

        when(userService.getCurrentUser()).thenReturn(user);

        ReflectionTestUtils.setField(authController, "userService", userService);

        authentication = mock(Authentication.class);
        mockedSecurityContextHolder = Mockito.mock(SecurityContextHolder.class);


        // Mockito.whens() for your authorization object
        securityContext = Mockito.mock(SecurityContext.class);
        securityContextLogoutHandler = Mockito.mock(SecurityContextLogoutHandler.class);

        SecurityContextHolder.setContext(securityContext);
        springSecurityUser=mock(org.springframework.security.core.userdetails.User.class);
    }

    @Test
    public void loginRequestHandler() throws ResourceNotFoundException {

        when(authentication.getPrincipal()).thenReturn(springSecurityUser);
        when(springSecurityUser.getUsername()).thenReturn("userId");
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);

        UserDTO result = authController.login();
        Assert.assertEquals("userName", result.getUsername());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowResourceNotFoundExceptionOnLoginWhenUserIsUnauthorised() throws ResourceNotFoundException {
        when(userService.getCurrentUser()).thenThrow(new ResourceNotFoundException("not a valid user"));
        authController.login();

    }

    @Test
    public void shouldReturnToLoginPageWhenUnauthorizedUserTriedToLogout() throws ResourceNotFoundException, ForbiddenResourceException {
        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse mockedResponse = Mockito.mock(HttpServletResponse.class);

        Mockito.when(securityContext.getAuthentication()).thenReturn(null);


        String response = authController.logout(mockedRequest,mockedResponse, new LogoutDTO());

        verify(securityContextLogoutHandler,never()).logout(mockedRequest,mockedResponse,null);
        Assert.assertEquals("redirect:/login",response);
    }
}