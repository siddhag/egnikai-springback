package com.thoughtworks.in.egnikai.controller;

import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.service.ChallengeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ChallengeControllerTest {

    @Mock
    private ChallengeService challengeService;

    private ChallengeController challengeController;

    @Before
    public void setUp() throws Exception {
        challengeController = new ChallengeController(challengeService);
    }

    @Test
    public void shouldInvokeGetChallengeFromService() throws ForbiddenResourceException, ResourceNotFoundException {
        challengeController.getChallenge();

        verify(challengeService).getChallenge();
    }
}