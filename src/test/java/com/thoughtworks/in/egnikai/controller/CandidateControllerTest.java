package com.thoughtworks.in.egnikai.controller;

import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.service.CandidateService;
import com.thoughtworks.in.egnikai.service.OperationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CandidateControllerTest {
    @Mock
    private CandidateService candidateService;
    @Mock
    private OperationService operationService;
    private CandidateController candidateController;

    @Before
    public void setUp() {
        candidateController = new CandidateController(candidateService, operationService);
    }

    @Test
    public void shouldRunAProblem() throws ResourceNotFoundException {
        candidateController.runProblem("1");
        verify(candidateService).runProblem(1);
    }


    @Test
    public void shouldInvokePutMessageFromService() throws ForbiddenResourceException, ResourceNotFoundException {
        candidateController.cloneGitRepository();
        verify(candidateService).putCloningMessageInQueue();
    }

    @Test
    public void shouldInvokeGetCloningStatusDetails() throws ForbiddenResourceException, ResourceNotFoundException {
        candidateController.getCloneStatus();

        verify(candidateService).getCloningOperationDetails();
    }

    @Test
    public void shouldGetOperationStatus() {
        candidateController.getOperationDetails(anyObject());
        verify(operationService).getOperationDetails(anyObject());
    }

    @Test
    public void shouldGetAttemptedProblemDetails() throws ForbiddenResourceException, ResourceNotFoundException {
        candidateController.getAttemptedProblems();
        verify(candidateService).getAttemptedProblems();
    }
}