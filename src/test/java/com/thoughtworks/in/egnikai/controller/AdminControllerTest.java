package com.thoughtworks.in.egnikai.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.thoughtworks.in.egnikai.dto.CandidateChallengeDTO;
import com.thoughtworks.in.egnikai.dto.EventDTO;
import com.thoughtworks.in.egnikai.dto.GenericResponseDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.User;
import com.thoughtworks.in.egnikai.service.AdminService;
import com.thoughtworks.in.egnikai.service.ChallengeService;
import com.thoughtworks.in.egnikai.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.exceptions.base.MockitoException;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import javax.ws.rs.NotAuthorizedException;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AdminControllerTest {
    @Mock
    private AdminService adminService;
    @Mock
    private UserService userService;
    @Mock
    private ChallengeService challengeService;
    private AdminController adminController;

    @Before
    public void setUp() {
        adminController = new AdminController(adminService, userService, challengeService);
    }


    @Test
    public void shouldInvokeGetCandidatesFromService() throws ForbiddenResourceException, ResourceNotFoundException {
        when(userService.isAdmin(any(User.class))).thenReturn(true);
        adminController.getCandidates(false);
        verify(adminService).getCandidates(false);
    }

    @Test
    public void shouldInvokeAllChallengeFromService() throws ResourceNotFoundException {
        when(userService.isAdmin(any(User.class))).thenReturn(true);

        adminController.getChallenges();

        verify(challengeService).getChallenges();
    }

    @Test(expected = NotAuthorizedException.class)
    public void shouldNotInvokeAllChallengeFromServiceWhenUserIsNotAdmin() throws ResourceNotFoundException {
        when(userService.isAdmin(any(User.class))).thenReturn(false);

        adminController.getChallenges();
    }

    @Test
    public void shouldCreateCandidates() throws ResourceNotFoundException, JsonProcessingException {
        when(userService.isAdmin(any(User.class))).thenReturn(true);
        List<CandidateChallengeDTO> usersToBeCreated = Arrays.asList(new CandidateChallengeDTO("some name", new Long(1)));
        EventDTO eventDTO = new EventDTO("some Event", usersToBeCreated);
        GenericResponseDTO responseDTO = adminController.createCandidatesForEvent(eventDTO);
        verify(adminService).createUsersForEvent(eventDTO);
        Assert.assertEquals(HttpStatus.OK,responseDTO.getHttpStatus());
    }

    @Test(expected = NotAuthorizedException.class)
    public void shouldThrowExceptionOnCreateCandidatesWhenUserIsNotAdmin() throws ResourceNotFoundException {
        List<CandidateChallengeDTO> usersToBeCreated = Arrays.asList(new CandidateChallengeDTO("some name", new Long(1)));
        EventDTO eventDTO = new EventDTO("some Event", usersToBeCreated);
        when(userService.isAdmin(any(User.class))).thenReturn(false);

        adminController.createCandidatesForEvent(eventDTO);
    }

    @Test
    public void shouldReturnInternalServerErrorIfCreateCandidatesFails() throws ResourceNotFoundException {
        List<CandidateChallengeDTO> usersToBeCreated = Arrays.asList(new CandidateChallengeDTO("some name", new Long(1)));
        EventDTO eventDTO = new EventDTO("some Event", usersToBeCreated);
        when(userService.isAdmin(any(User.class))).thenReturn(true);
        doThrow(new MockitoException("")).when(adminService).createUsersForEvent(eventDTO);
        GenericResponseDTO responseDTO = adminController.createCandidatesForEvent(eventDTO);
        verify(adminService).createUsersForEvent(eventDTO);
        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,responseDTO.getHttpStatus());
    }
}