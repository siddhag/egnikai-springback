package com.thoughtworks.in.egnikai.testHelpers;

import com.thoughtworks.in.egnikai.persistence.model.*;

import static java.util.Arrays.asList;

public class CandidateTestHelper {

    public static final int PROBLEM_ID = 1;
    public static final int SOURCE_ID = 2;
    public static final String CODE_SUBMISSIONS_BUCKET = "code_submissions";
    public static final String CODE_PROBLEMS_BUCKET = "code_problems";
    public static final int CANDIDATE_ID = 1;
    public static final String SOURCE_PATH = "src/main/java/P1/PrimeChecker_1.java";
    public static final String TEST_PATH = "src/TEST/java/P1/PrimeChecker_1Test.java";
    public static final String CHALLENGE_BASE_S_3_KEY = "java/egnikai-java-challenge-sample";
    public static final int PUBLIC_TEST_ID = 2;
    public static final int PRIVATE_TEST_ID = 1;
    public static final int CREDIT = 3;


    public static Candidate setUpCandidate(String language) {
        User user = new User();
        ComplexityScore complexityScore = new ComplexityScore();
        Candidate candidate = getCandidate(user);
        SourceCode sourceCode1_1 = new SourceCode(1, "Source1_1",
                "src/main/java/P1/PrimeChecker.java", "SourceClass", true);
        SourceCode sourceCode1_2 = new SourceCode(SOURCE_ID, "Source1_2",
                SOURCE_PATH, "SourceClass", true);

        TestCase testCase1_1 = new TestCase(PRIVATE_TEST_ID, "Test1_1",
                "TestClass", TestType.PRIVATE, "/testpath1_1");
        TestCase testCase1_2 = new TestCase(PUBLIC_TEST_ID, "Test1_2",
                "TestClass", TestType.PUBLIC, TEST_PATH);
        Problem problem1 = new Problem((long) PROBLEM_ID, "Problem1",
                "Problem1 desc", true, CREDIT, asList(testCase1_1, testCase1_2), complexityScore,
                asList(sourceCode1_1, sourceCode1_2));


        SourceCode sourceCode2_1 = new SourceCode(3, "Source2_1",
                "src/main/java/P2/SumChecker.java", "SourceClass", true);
        SourceCode sourceCode2_2 = new SourceCode(4, "Source2_2",
                "src/main/java/P2/SumChecker_1.java", "SourceClass", true);
        TestCase testCase2_1 = new TestCase(3, "Test2_1",
                "TestClass", TestType.PUBLIC, "/testpath2_1");
        TestCase testCase2_2 = new TestCase(4, "Test1_2",
                "TestClass", TestType.PUBLIC, "/testpath2_2");
        Problem problem2 = new Problem((long) 2, "Problem1",
                "Problem1 desc", true, 3, asList(testCase2_1, testCase2_2), complexityScore,
                asList(sourceCode2_1, sourceCode2_2));

        Challenge challenge = new Challenge("description");
        challenge.setChallengeBaseS3Key(CHALLENGE_BASE_S_3_KEY);
        challenge.addProblem(problem1);
        challenge.addProblem(problem2);
        challenge.setPlatformName(language);
        candidate.setChallenge(challenge);
        return candidate;
    }

    private static Candidate getCandidate(User user) {
        Candidate candidate = new Candidate();
        candidate.setCandidateId(CANDIDATE_ID);
        candidate.setUser(user);
        candidate.setActive(true);
        return candidate;
    }

}
