package com.thoughtworks.in.egnikai.filter;

import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.service.CandidateService;
import com.thoughtworks.in.egnikai.service.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CheckUserBlockedFilterTest {


    private CheckUserBlockedFilter checkUserBlockedFilter;
    private CandidateService candidateService;
    private UserService userService;

    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);

    @Mock
    private HttpServletResponse response = mock(HttpServletResponse.class);

    @Mock
    private FilterChain filterChain = mock(FilterChain.class);

    @Before
    public void setUp() {
        candidateService = mock(CandidateService.class);
        userService = mock(UserService.class);

        checkUserBlockedFilter = new CheckUserBlockedFilter(candidateService, userService);

        Authentication authentication = setMockAuthenticationToSecurityContext();

        org.springframework.security.core.userdetails.User springSecurityUser = mock(org.springframework.security.core.userdetails.User.class);
        when(authentication.getPrincipal()).thenReturn(springSecurityUser);
    }

    @After
    public void validate() {
        validateMockitoUsage();
    }

    private Authentication setMockAuthenticationToSecurityContext() {
        Authentication authentication = mock(Authentication.class);

        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        return authentication;
    }

    @Test
    public void doFilterShouldNotCallLogOutWhenUserIsValid() throws IOException, ServletException, ForbiddenResourceException, ResourceNotFoundException {
        Candidate candidate=new Candidate();
        candidate.setLocked(false);

        when(candidateService.getCurrentCandidate()).thenReturn(candidate);

        checkUserBlockedFilter.doFilter(request, response, filterChain);

        verify(filterChain).doFilter(request, response);
        verify(userService, never()).logOut(any(), any(), any());
    }

    @Test
    public void doFilterShouldCallLogOutWhenUserIsNotPresentInDb() throws IOException, ServletException, ForbiddenResourceException, ResourceNotFoundException {
        when(candidateService.getCurrentCandidate()).thenThrow(new ResourceNotFoundException("User / candidate not found"));

        checkUserBlockedFilter.doFilter(request, response, filterChain);

        verify(userService).logOut(eq(request), eq(response), any());
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void doFilterShouldCallLogOutWhenUserIsNotEnabled() throws IOException, ServletException, ForbiddenResourceException, ResourceNotFoundException {
        Candidate candidate=new Candidate();
        candidate.setLocked(true);

        when(candidateService.getCurrentCandidate()).thenReturn(candidate);

        checkUserBlockedFilter.doFilter(request, response, filterChain);

        verify(userService).logOut(eq(request), eq(response), any());
        verify(filterChain).doFilter(request, response);
    }


    @Test
    public void doFilterShouldAlwaysCallDoFilterOnFilterChain() throws IOException, ServletException, ForbiddenResourceException, ResourceNotFoundException {
        when(candidateService.getCurrentCandidate()).thenThrow(new NullPointerException());

        checkUserBlockedFilter.doFilter(request, response, filterChain);

        verify(filterChain).doFilter(request, response);
        verify(userService, never()).logOut(any(), any(), any());
    }
}