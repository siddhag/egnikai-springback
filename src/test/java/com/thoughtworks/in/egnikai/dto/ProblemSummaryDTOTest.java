package com.thoughtworks.in.egnikai.dto;

import com.thoughtworks.in.egnikai.persistence.model.Problem;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ProblemSummaryDTOTest {

    @Test
    public void shouldReturnDtoWithProblemDetails() {
        List<Problem> problems = Arrays.asList(this.createProblem());

        ProblemSummaryDTO problemSummaryDTO = ProblemSummaryDTO.fromProblem(problems, true).get(0);

        assertEquals(problemSummaryDTO.getCredit(), new Integer(problems.get(0).getCredit()));
        assertEquals(problemSummaryDTO.getDescription(), problems.get(0).getDescription());
        assertEquals(problemSummaryDTO.getProblemId(), problems.get(0).getProblemId());
        assertEquals(problemSummaryDTO.getTitle(), problems.get(0).getTitle());
        assertEquals(problemSummaryDTO.isOptional(), problems.get(0).isOptional());
    }

    @Test
    public void shouldReturnTwoDtosIfGivenTwoProblems() {
        List<Problem> problems = Arrays.asList(new Problem(), new Problem());

        List<ProblemSummaryDTO> expected = ProblemSummaryDTO.fromProblem(problems, true);

        assertEquals(expected.size(), problems.size());
    }

    @Test
    public void shouldNotProvideTestAndSourcesToDTO() {
        assertEquals(ProblemSummaryDTO.class.getDeclaredFields().length, 5);
    }

    private Problem createProblem() {
        Problem problem = new Problem();
        problem.setCredit(5);
        problem.setDescription("TestCase Description");
        problem.setOptional(true);
        problem.setProblemId(1);
        problem.setTitle("TestCase Title");
        return problem;
    }
}