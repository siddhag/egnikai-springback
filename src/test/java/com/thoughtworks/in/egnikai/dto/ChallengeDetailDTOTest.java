package com.thoughtworks.in.egnikai.dto;

import com.thoughtworks.in.egnikai.persistence.model.Challenge;
import com.thoughtworks.in.egnikai.persistence.model.Problem;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ChallengeDetailDTOTest {

    @Test
    public void shouldReturnDTOWithChallengeDetails() {
        List<Challenge> challenges = Arrays.asList(this.createChallenge());

        ChallengeDetailDTO challengeDetailDTO = ChallengeDetailDTO.fromChallenge(challenges).get(0);

        assertEquals(challengeDetailDTO.getChallengeId(), challenges.get(0).getChallengeId());
        assertEquals(challengeDetailDTO.getChallengeGitURL(), challenges.get(0).getChallengeGitURL());
        assertEquals(challengeDetailDTO.getChallengeTitle(), challenges.get(0).getChallengeTitle());
        assertEquals(challengeDetailDTO.getPlatformName(), challenges.get(0).getPlatformName());
        assertEquals(challengeDetailDTO.getDescription(), challenges.get(0).getDescription());
        assertEquals(challengeDetailDTO.getProblems(), challenges.get(0).getProblems());
    }

    @Test
    public void shouldReturnTwoDtosIfGivenTwoChallenges() {
        List<Challenge> challenges = Arrays.asList(new Challenge(), new Challenge());

        List<ChallengeDetailDTO> expected = ChallengeDetailDTO.fromChallenge(challenges);

        assertEquals(expected.size(), challenges.size());
    }

    private Challenge createChallenge() {

        Challenge challenge = new Challenge();
        challenge.setChallengeId(1);
        challenge.setChallengeGitURL("giturl.com");
        challenge.setChallengeTitle("title");
        challenge.setDescription("description");
        challenge.setPlatformName("JAVA");
        challenge.setProblems(Arrays.asList(new Problem()));

        return challenge;
    }

}