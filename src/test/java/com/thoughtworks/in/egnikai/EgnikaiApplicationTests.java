package com.thoughtworks.in.egnikai;

import com.jcraft.jsch.JSch;
import com.thoughtworks.in.egnikai.dto.*;
import com.thoughtworks.in.egnikai.persistence.model.Challenge;
import com.thoughtworks.in.egnikai.persistence.repository.ChallengeRepository;
import com.thoughtworks.in.egnikai.util.GitUtil;
import org.apache.commons.codec.binary.Base64;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {
                "spring.datasource.url=jdbc:mysql://localhost:3306/egnikaidb",
                "spring.datasource.initialization-mode=always",
                "spring.jpa.hibernate.ddl-auto",
                "spring.datasource.driver-class-name=com.mysql.jdbc.Driver",
                "spring.datasource.username=root",
                "security.basic.enabled=false",
                "management.security.enabled=false",
                "management.health.db.enabled=false",
                "egnikai.log.path=src/test/resources/log",
                "spring.flyway.locations=classpath:db/migration/default",
                "default-aws-region=us-east-1",
                "egnikai.s3.bucket.candidate-code-submissions=tw-egnikai-dev-candidate-code-submissions",
                "egnikai.s3.bucket.code-problems=tw-egnikai-problems",
                "egnikai.sharedVolume.path=/code",
                "egnikai.allowed.origins=http://localhost:3333"
        })
public class EgnikaiApplicationTests {
    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${egnikai.testRepoLink}")
    private String testRepoLink;

    @Autowired
    private ChallengeRepository challengeRepository;

    @Before
    public void initTests() {
        JSch.setConfig("StrictHostKeyChecking", "no");
    }

    @Test
    public void contextLoads() {
    }

    @Test
    public void shouldReturnUserDetailsForCorrectUserCredentials() {
        String apiEndPoint = "/api/login";

        ResponseEntity<UserDTO> responseEntity = restTemplate.exchange(apiEndPoint, HttpMethod.POST, new HttpEntity<Object>(createBasicAuthorizationHeaders("testuser", "testuser")), UserDTO.class);
        UserDTO userDTO = responseEntity.getBody();

        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assert.assertEquals("testuser", userDTO.getUserid());
    }

    @Test
    public void shouldReturnLoginFailureOnWrongUserCredentials() {
        String apiEndPoint = "/api/login";

        ResponseEntity<UserDTO> responseEntity = restTemplate.exchange(apiEndPoint, HttpMethod.POST, new HttpEntity<Object>(createBasicAuthorizationHeaders("non-existing-user-id", "wrong-password")), UserDTO.class);

        Assert.assertEquals(HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
    }

    @Test
    public void fileDeletes() throws IOException {
        String directoryName = "./tempDirectory";
        File toBeDeletedTextFile = new File(directoryName + "/jaguar-xf.txt");
        toBeDeletedTextFile.getParentFile().mkdirs();
        try {
            deleteFile(directoryName);
            Assert.assertFalse(toBeDeletedTextFile.exists());
        } finally {
            new File(directoryName).delete();
        }
    }

    @Test
    public void shouldSuccessfullyCreateCandidatesAndAssignChallenges() {
        String createCandidateApiEndPoint = "/admin/api/candidates/create";

        ResponseEntity<UserDTO> loginResponseEntity = loginAsAdmin();
        ResponseEntity<GenericResponseDTO> responseEntity = restTemplate.exchange(createCandidateApiEndPoint, HttpMethod.POST,
                new HttpEntity<Object>(new EventDTO("some event", Arrays.asList(new CandidateChallengeDTO("user", new Long(1)))),
                        createBasicAuthorizationHeaders("admin", "admin")), GenericResponseDTO.class);

        Assert.assertEquals(HttpStatus.OK, loginResponseEntity.getStatusCode());
        Assert.assertEquals(HttpStatus.OK, responseEntity.getBody().getHttpStatus());
    }

    @Test
    public void shouldNotCreateCandidateWhenWrongChallengeIsAssigned() {
        String createCandidateApiEndPoint = "/admin/api/candidates/create";

        ResponseEntity<UserDTO> loginResponseEntity = loginAsAdmin();

        ResponseEntity<GenericResponseDTO> responseEntity = restTemplate.exchange(createCandidateApiEndPoint, HttpMethod.POST,
                new HttpEntity<Object>(new EventDTO("some event", Arrays.asList(new CandidateChallengeDTO("user", new Long(100)))),
                        createBasicAuthorizationHeaders("admin", "admin")), GenericResponseDTO.class);

        Assert.assertEquals(HttpStatus.OK, loginResponseEntity.getStatusCode());
        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getBody().getHttpStatus());
    }

    @Test
    public void shouldRetrieveAllChallenges() {
        String fetchAllChallenges = "/admin/api/challenges";

        Challenge expectedChallenge = new Challenge();
        expectedChallenge.setChallengeBaseS3Key("S3BucketUrl");
        challengeRepository.save(expectedChallenge);

        ResponseEntity<UserDTO> loginResponseEntity = loginAsAdmin();
        ResponseEntity<ChallengeBaseDTO[]> responseEntity = restTemplate.exchange(fetchAllChallenges, HttpMethod.GET,
                new HttpEntity<>(createBasicAuthorizationHeaders("admin", "admin")), ChallengeBaseDTO[].class);

        List<ChallengeBaseDTO> challengeBaseDTOS = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
        List<ChallengeBaseDTO> actualChallenge = challengeBaseDTOS.stream().filter(challengeBaseDTO ->
                challengeBaseDTO.getChallengeId() == expectedChallenge.getChallengeId())
                .collect(Collectors.toList());

        Assert.assertEquals(HttpStatus.OK, loginResponseEntity.getStatusCode());
        Assert.assertEquals(expectedChallenge.getChallengeId(), actualChallenge.get(0).getChallengeId());

        challengeRepository.delete(expectedChallenge.getChallengeId());
    }

    private ResponseEntity<UserDTO> loginAsAdmin() {
        String loginApiEndPoint = "/admin/api/login";
        return restTemplate.exchange(loginApiEndPoint, HttpMethod.POST,
                new HttpEntity<>(createBasicAuthorizationHeaders("admin", "admin")), UserDTO.class);
    }

    private void deleteFile(String directoryPath) throws IOException {
        if (new File(directoryPath).exists()) {
            Files.walk(Paths.get(directoryPath), FileVisitOption.FOLLOW_LINKS)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
    }

    private HttpHeaders createBasicAuthorizationHeaders(String username, String password) {
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName("US-ASCII")));
            String authHeader = "Basic " + new String(encodedAuth);
            set("Authorization", authHeader);
            set("origin","http://localhost:3333");
        }};
    }

}

