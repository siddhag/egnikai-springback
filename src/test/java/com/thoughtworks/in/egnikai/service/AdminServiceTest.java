package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.dto.CandidateChallengeDTO;
import com.thoughtworks.in.egnikai.dto.CandidateDTO;
import com.thoughtworks.in.egnikai.dto.EventDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.persistence.model.Challenge;
import com.thoughtworks.in.egnikai.persistence.model.User;
import com.thoughtworks.in.egnikai.persistence.repository.CandidateRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AdminServiceTest {
    @InjectMocks
    private AdminService adminService;

    @Mock
    private CandidateRepository candidateRepository;

    @Mock
    private UserService userService;

    @Mock
    private CandidateService candidateService;

    @Mock
    private ChallengeService challengeService;

    @Test
    public void shouldGetActiveCandidates() throws ForbiddenResourceException, ResourceNotFoundException {
        List<Candidate> candidates = new ArrayList<>();
        User user = new User();
        user.setUserId("1");
        user.setUserName("Name");

        Candidate candidate1 = new Candidate();
        candidate1.setCandidateId(1);
        candidate1.setActive(true);
        candidate1.setUser(user);
        candidates.add(candidate1);

        Candidate candidate2 = new Candidate();
        candidate2.setCandidateId(2);
        candidate2.setActive(true);
        candidate2.setUser(user);
        candidates.add(candidate2);

        when(userService.getCurrentUser()).thenReturn(user);
        when(userService.isAdmin(user)).thenReturn(true);
        List<CandidateDTO> candidateDTOs = CandidateDTO.fromCandidates(candidates);
        when(candidateRepository.findAllByisActiveOrderByCandidateIdDesc(true)).thenReturn(candidates);

        assertEquals(candidateDTOs, adminService.getCandidates(true));
    }

    @Test
    public void shouldGetPastCandidates() throws ForbiddenResourceException, ResourceNotFoundException {
        List<Candidate> candidates = new ArrayList<>();
        User user = new User();
        user.setUserId("1");
        user.setUserName("Name");

        Candidate candidate1 = new Candidate();
        candidate1.setCandidateId(1);
        candidate1.setActive(false);
        candidate1.setUser(user);
        candidates.add(candidate1);

        Candidate candidate2 = new Candidate();
        candidate2.setCandidateId(2);
        candidate2.setActive(false);
        candidate2.setUser(user);
        candidates.add(candidate2);

        when(userService.getCurrentUser()).thenReturn(user);
        when(userService.isAdmin(user)).thenReturn(true);
        List<CandidateDTO> candidateDTOs = CandidateDTO.fromCandidates(candidates);
        when(candidateRepository.findAllByisActiveOrderByCandidateIdDesc(false)).thenReturn(candidates);

        assertEquals(candidateDTOs, adminService.getCandidates(false));
    }

    @Test
    public void shouldReturnEmptyListWhenNoPastCandidatesFound() throws ResourceNotFoundException {
        List<Candidate> candidates = new ArrayList<>();
        User user = new User();
        when(userService.getCurrentUser()).thenReturn(user);
        when(userService.isAdmin(user)).thenReturn(true);
        List<CandidateDTO> candidateDTOs = CandidateDTO.fromCandidates(candidates);

        when(candidateRepository.findAllByisActiveOrderByCandidateIdDesc(false)).thenReturn(candidates);

        assertEquals(candidateDTOs, adminService.getCandidates(false));
    }

    @Test
    public void shouldUpdateCandidates() throws ForbiddenResourceException {

        Long invalidCandidateId = new Long(5);
        Candidate candidate1 = new Candidate();
        candidate1.setCandidateId(1);
        candidate1.setActive(false);

        Candidate candidate2 = new Candidate();
        candidate2.setCandidateId(2);
        candidate2.setActive(false);
        List<Long> candidates = new ArrayList<>();
        candidates.add(candidate1.getCandidateId());
        candidates.add(candidate2.getCandidateId());
        candidates.add(invalidCandidateId);
        when(candidateRepository.findOne(candidate1.getCandidateId())).thenReturn(candidate1);
        when(candidateRepository.findOne(candidate2.getCandidateId())).thenReturn(candidate2);
        when(candidateRepository.findOne(invalidCandidateId)).thenReturn(null);

        adminService.updateCandidates(true,true,candidates);
        verify(candidateRepository,times(2)).save((Candidate) any());

}

    @Test
    public void ShouldCreateUsers() throws Exception {
        CandidateChallengeDTO candidateChallengeDTO = new CandidateChallengeDTO("some name", new Long(1));
        List<CandidateChallengeDTO> candidateChallengeDTOList = Arrays.asList(candidateChallengeDTO);
        EventDTO eventDTO = new EventDTO("some Event", candidateChallengeDTOList);

        when(challengeService.getChallenge(candidateChallengeDTO.getChallengeId())).thenReturn(new Challenge());
        adminService.createUsersForEvent(eventDTO);

        verify(userService).saveUsers(any());
        verify(candidateService).saveCandidates(any());
        verify(challengeService).getChallenge(any());
    }

}