package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.dto.SourceDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.*;
import com.thoughtworks.in.egnikai.persistence.repository.ProblemRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import java.io.IOException;
import java.util.Optional;

import static com.thoughtworks.in.egnikai.testHelpers.CandidateTestHelper.*;
import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SourceCodeServiceTest {
    @Mock
    private S3Service s3Service;

    private SourceCodeService sourceCodeService;

    private ModelMapper mapper = new ModelMapper();
    private Candidate candidate;

    @Before
    public void setup() throws ForbiddenResourceException, ResourceNotFoundException {
        sourceCodeService = new SourceCodeService(
                s3Service,
                CODE_SUBMISSIONS_BUCKET,
                CODE_PROBLEMS_BUCKET);
        candidate = setUpCandidate("java");
        
        when(s3Service.getFileContent(CODE_SUBMISSIONS_BUCKET,
                format("saved/%d/%d/%s", CANDIDATE_ID, PROBLEM_ID, SOURCE_PATH)))
                .thenReturn(Optional.of("this is the file content"));
    }

    @Test
    public void shouldGetSourceForProblemFromSubmissionBucket() throws IOException, ResourceNotFoundException, ForbiddenResourceException {
        SourceCode sourceForId2 = candidate.getChallenge().getProblems().get(0).getSourceCodes().get(1);

        SourceDTO expectedSourceDTO = new SourceDTO();
        mapper.map(sourceForId2, expectedSourceDTO);
        expectedSourceDTO.setSourceCode("this is the file content");

        SourceDTO sourceDTO = sourceCodeService.getSourceCode(candidate, PROBLEM_ID, SOURCE_ID);

        assertEquals(expectedSourceDTO, sourceDTO);
    }

    @Test
    public void shouldGetSourceForProblemFromProblemsBucketIfSubmissionBucketDoesntHaveIt() throws IOException, ResourceNotFoundException, ForbiddenResourceException {
        when(s3Service.getFileContent(anyString(), anyString()))
                .thenReturn(Optional.empty())
                .thenReturn(Optional.of("this is the file content"));

        SourceCode sourceForId2 = candidate.getChallenge().getProblems().get(0).getSourceCodes().get(1);

        SourceDTO expectedSourceDTO = new SourceDTO();
        mapper.map(sourceForId2, expectedSourceDTO);
        expectedSourceDTO.setSourceCode("this is the file content");

        SourceDTO sourceDTO = sourceCodeService.getSourceCode(candidate, PROBLEM_ID, SOURCE_ID);

        assertEquals(expectedSourceDTO, sourceDTO);
        InOrder inOrder = inOrder(s3Service);
        inOrder.verify(s3Service).getFileContent(CODE_SUBMISSIONS_BUCKET,
                format("saved/%d/%d/%s", CANDIDATE_ID, PROBLEM_ID, SOURCE_PATH));
        inOrder.verify(s3Service).getFileContent(CODE_PROBLEMS_BUCKET,
                format("%s/%s", CHALLENGE_BASE_S_3_KEY, SOURCE_PATH));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowResourceNoFoundIfSourcePathNotFoundInCodeSubmissionsOrProblems() throws IOException, ResourceNotFoundException, ForbiddenResourceException {
        when(s3Service.getFileContent(anyString(), anyString()))
                .thenReturn(Optional.empty())
                .thenReturn(Optional.empty());

        candidate.getChallenge().getProblems().get(0).getSourceCodes().get(1);


        sourceCodeService.getSourceCode(candidate, PROBLEM_ID, SOURCE_ID);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowResourceNotFoundExceptionIfProblemIsNotFound() throws ResourceNotFoundException, ForbiddenResourceException {
        long notFoundProblemId = 999;
        SourceDTO sourceDTO = sourceCodeService.getSourceCode(candidate, notFoundProblemId, 2);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowResourceNotFoundExceptionIfSourceIdIsNotFound() throws ResourceNotFoundException, ForbiddenResourceException {
        long notFoundSourceId = 999;
        sourceCodeService.getSourceCode(candidate, PROBLEM_ID, notFoundSourceId);
    }
}
