package com.thoughtworks.in.egnikai.service;

import com.amazonaws.services.s3.AmazonS3;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class S3ServiceTest {

    private static final String KEY_NAME = "saved/c4/src/main/java/P1/Primechecker.java";
    private static final String BUCKET = "bucket";
    private static final String FILE_CONTENT = "This is the file content";
    @Mock
    private AmazonS3 amazonS3;

    @Test
    public void shouldReturnFileAsStringIfKeyIsPresent() {
        when(amazonS3.doesObjectExist(BUCKET, KEY_NAME))
                .thenReturn(true);
        when(amazonS3.getObjectAsString(BUCKET, KEY_NAME)).thenReturn(FILE_CONTENT);
        Optional<String> fileContent = new S3Service(amazonS3).getFileContent(BUCKET, KEY_NAME);
        assertThat(fileContent.isPresent()).isTrue();
        assertThat(fileContent.get()).isEqualTo(FILE_CONTENT);
    }

    @Test
    public void shouldUploadFileContentToS3() {
        new S3Service(amazonS3).uploadFileContent("Bucket", "key", "content");
        verify(amazonS3).putObject("Bucket", "key", "content");
    }

    @Test
    public void shouldUploadFileToS3() {
        File file = new File("/tmp/test");
        new S3Service(amazonS3).uploadFile("Bucket", "key", file);
        verify(amazonS3).putObject("Bucket", "key", file);
    }

}