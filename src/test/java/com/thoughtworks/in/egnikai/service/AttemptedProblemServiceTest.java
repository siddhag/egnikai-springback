package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.persistence.model.*;
import com.thoughtworks.in.egnikai.persistence.repository.CandidateRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AttemptedProblemServiceTest {

    private AttemptedProblemService attemptedProblemService;

    @Mock
    private CandidateRepository candidateRepository;

    @Captor
    private ArgumentCaptor<Candidate> candidateArgumentCaptor;

    @Before
    public void setup() {
        attemptedProblemService = new AttemptedProblemService(candidateRepository);
    }

    @Test
    public void shouldAddNewAttemptedProblemForACandidateIfCandidateHasNoAttemptedProblem() {
        Problem problem = new Problem();
        Operation operation = generateOperation(OperationStatus.FAILED, problem, new LinkedList<AttemptedProblem>());


        attemptedProblemService.updateAttemptedProblemInDB(operation);

        verify(candidateRepository).save(candidateArgumentCaptor.capture());

        final Candidate capturedCandidate = candidateArgumentCaptor.getValue();
        final List<AttemptedProblem> attemptedProblems = capturedCandidate.getAttemptedProblems();
        final AttemptedProblem attemptedProblem = attemptedProblems.get(0);

        assertEquals(1, attemptedProblems.size());
        assertSame(problem, attemptedProblem.getProblem());
        assertFalse(attemptedProblem.isPassed());
    }

    @Test
    public void shouldAppendNewAttemptedProblemForACandidateIfCandidateHasNotAttemptedTheSameProblemEarlier() {
        Problem newlyAttemptedProblem = new Problem();
        newlyAttemptedProblem.setProblemId(3);
        final LinkedList<AttemptedProblem> alreadyAttemptedProblems = createAttemptedProblems(false, 1);

        Operation operation = generateOperation(OperationStatus.FAILED, newlyAttemptedProblem, alreadyAttemptedProblems);


        attemptedProblemService.updateAttemptedProblemInDB(operation);

        verify(candidateRepository).save(candidateArgumentCaptor.capture());

        final Candidate capturedCandidate = candidateArgumentCaptor.getValue();
        final List<AttemptedProblem> capturedCandidateAttemptedProblems = capturedCandidate.getAttemptedProblems();
        final AttemptedProblem attemptedProblem = capturedCandidateAttemptedProblems.get(1);

        assertEquals(2, capturedCandidateAttemptedProblems.size());
        assertSame(newlyAttemptedProblem, attemptedProblem.getProblem());
        assertFalse(attemptedProblem.isPassed());
    }

    @Test
    public void shouldUpdateTheExistingAttemptedProblemIfCandidateHasAttemptedTheSameProblemEarlier_ForFailedScenario() {
        Problem newlyAttemptedProblem = new Problem();
        newlyAttemptedProblem.setProblemId(2);
        final LinkedList<AttemptedProblem> alreadyAttemptedProblems = createAttemptedProblems(false, 1, 2);

        Operation operation = generateOperation(OperationStatus.FAILED, newlyAttemptedProblem, alreadyAttemptedProblems);


        attemptedProblemService.updateAttemptedProblemInDB(operation);

        verify(candidateRepository).save(candidateArgumentCaptor.capture());

        final Candidate capturedCandidate = candidateArgumentCaptor.getValue();
        final List<AttemptedProblem> capturedCandidateAttemptedProblems = capturedCandidate.getAttemptedProblems();
        final AttemptedProblem attemptedProblem = capturedCandidateAttemptedProblems.get(1);

        assertEquals(2, capturedCandidateAttemptedProblems.size());
        assertEquals(newlyAttemptedProblem, attemptedProblem.getProblem());
        assertFalse(attemptedProblem.isPassed());
    }

    @Test
    public void shouldUpdateTheExistingAttemptedProblemIfCandidateHasAttemptedTheSameProblemEarlier_ForSuccessScenario() {
        Problem newlyAttemptedProblem = new Problem();
        newlyAttemptedProblem.setProblemId(2);
        final LinkedList<AttemptedProblem> alreadyAttemptedProblems = createAttemptedProblems(false, 1, 2);

        Operation operation = generateOperation(OperationStatus.COMPLETED, newlyAttemptedProblem, alreadyAttemptedProblems);


        attemptedProblemService.updateAttemptedProblemInDB(operation);

        verify(candidateRepository).save(candidateArgumentCaptor.capture());

        final Candidate capturedCandidate = candidateArgumentCaptor.getValue();
        final List<AttemptedProblem> capturedCandidateAttemptedProblems = capturedCandidate.getAttemptedProblems();
        final AttemptedProblem attemptedProblem = capturedCandidateAttemptedProblems.get(1);

        assertEquals(2, capturedCandidateAttemptedProblems.size());
        assertEquals(newlyAttemptedProblem, attemptedProblem.getProblem());
        assertTrue(attemptedProblem.isPassed());
    }

    private LinkedList<AttemptedProblem> createAttemptedProblems(boolean isPassed, int... alreadyAttemptedProblemIds) {
        final LinkedList<AttemptedProblem> attemptedProblems = new LinkedList<>();

        for (int problemId : alreadyAttemptedProblemIds) {
            final AttemptedProblem attemptedProblem1 = new AttemptedProblem();
            attemptedProblem1.setPassed(isPassed);
            attemptedProblem1.setScore(0);

            final Problem alreadyAttemptedProblem = new Problem();
            alreadyAttemptedProblem.setProblemId(problemId);
            attemptedProblem1.setProblem(alreadyAttemptedProblem);

            attemptedProblems.add(attemptedProblem1);
        }

        return attemptedProblems;
    }

    private Operation generateOperation(OperationStatus operationStatus, Problem problem, LinkedList<AttemptedProblem> attemptedProblems) {
        Operation operation = new Operation();
        operation.setOperationStatus(operationStatus.name());
        Candidate candidate = new Candidate();
        candidate.setAttemptedProblems(attemptedProblems);
        operation.setCandidate(candidate);
        operation.setProblem(problem);
        return operation;
    }
}