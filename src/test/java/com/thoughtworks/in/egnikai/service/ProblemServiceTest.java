package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.dto.GenericResponseDTO;
import com.thoughtworks.in.egnikai.dto.ProblemDetailDTO;
import com.thoughtworks.in.egnikai.dto.SourceDTO;
import com.thoughtworks.in.egnikai.dto.TestDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

import static com.thoughtworks.in.egnikai.testHelpers.CandidateTestHelper.*;
import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProblemServiceTest {

    private static final String FILECONTENT = "filecontent";
    @Mock
    private UserService userService;
    @Mock
    private CandidateService candidateService;
    @Mock
    private S3Service s3Service;

    private ModelMapper mapper = new ModelMapper();
    private ProblemService problemService;
    private Candidate candidate;
    private static final String CANDIDATE_SUBMISSIONS_BUCKET = "CANDIDATE_SUBMISSIONS";

    @Before
    public void setup() throws ForbiddenResourceException, ResourceNotFoundException {
        candidate = setUpCandidate("Java");
        when(candidateService.getCurrentCandidate()).thenReturn(candidate);
        when(userService.isCandidate(any())).thenReturn(true);

        problemService = new ProblemService(userService,
                candidateService,
                s3Service,
                false,
                CANDIDATE_SUBMISSIONS_BUCKET);
    }

    @Test
    public void shouldGetProblem2WithCredit0WhenIsCreditVisibleFlagFalse()
            throws ResourceNotFoundException, ForbiddenResourceException {

        SourceDTO sourceDTO1 = new SourceDTO();
        mapper.map(candidate.getChallenge().getProblems().get(0).getSourceCodes().get(1), sourceDTO1);

        TestDTO testDTO = new TestDTO();
        mapper.map(candidate.getChallenge().getProblems().get(0).getTestCases().get(1), testDTO);

        List<SourceDTO> sourceDTOs = new ArrayList<>();
        sourceDTOs.add(mapper.map(candidate.getChallenge().getProblems().get(0).getSourceCodes().get(0), SourceDTO.class));
        sourceDTOs.add(mapper.map(candidate.getChallenge().getProblems().get(0).getSourceCodes().get(1), SourceDTO.class));
        List<TestDTO> testDTOs = new ArrayList<>();
        testDTOs.add(testDTO);

        ProblemDetailDTO expectedProblemDetailDTO = new ProblemDetailDTO();
        mapper.map(candidate.getChallenge().getProblems().get(0), expectedProblemDetailDTO);
        expectedProblemDetailDTO.setSourceDTOs(sourceDTOs);
        expectedProblemDetailDTO.setTestDTOs(testDTOs);
        expectedProblemDetailDTO.setCredit(null);

        ProblemDetailDTO problemDetailDTO = problemService.getProblem(PROBLEM_ID);

        assertEquals(expectedProblemDetailDTO, problemDetailDTO);
    }

    @Test
    public void shouldGetProblem2WithCreditWhenIsCreditVisibleFlagIsTrue()
            throws ResourceNotFoundException, ForbiddenResourceException {
        problemService = new ProblemService(userService,
                candidateService,
                s3Service, true,
                CANDIDATE_SUBMISSIONS_BUCKET);

        TestDTO testDTO = new TestDTO();
        mapper.map(candidate.getChallenge().getProblems().get(0).getTestCases().get(1), testDTO);

        List<SourceDTO> sourceDTOs = new ArrayList<>();
        sourceDTOs.add(mapper.map(candidate.getChallenge().getProblems().get(0).getSourceCodes().get(0), SourceDTO.class));
        sourceDTOs.add(mapper.map(candidate.getChallenge().getProblems().get(0).getSourceCodes().get(1), SourceDTO.class));
        List<TestDTO> testDTOs = new ArrayList<>();
        testDTOs.add(testDTO);

        ProblemDetailDTO expectedProblemDetailDTO = new ProblemDetailDTO();
        mapper.map(candidate.getChallenge().getProblems().get(0), expectedProblemDetailDTO);
        expectedProblemDetailDTO.setSourceDTOs(sourceDTOs);
        expectedProblemDetailDTO.setTestDTOs(testDTOs);
        expectedProblemDetailDTO.setCredit(CREDIT);

        ProblemDetailDTO problemDetailDTO = problemService.getProblem(PROBLEM_ID);

        assertEquals(expectedProblemDetailDTO, problemDetailDTO);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowExceptionWhenProblemWithIdNotFound()
            throws ResourceNotFoundException, ForbiddenResourceException {
        int invalidProblemId = 999;
        problemService.getProblem(invalidProblemId);
    }

    @Test(expected = ForbiddenResourceException.class)
    public void shouldThrowExceptionWhenAdminGetsProblem() throws ResourceNotFoundException, ForbiddenResourceException {
        when(userService.isCandidate(any())).thenReturn(false);
        problemService.getProblem(PROBLEM_ID);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldReturnHttpStatusNotAcceptedWhenUserIsNotActive() throws ResourceNotFoundException, ForbiddenResourceException {
        User user = new User();
        user.setUserId("1");
        when(userService.getCurrentUser()).thenReturn(user);
        when(candidateService.getCurrentCandidate()).thenThrow(new ResourceNotFoundException(""));
        problemService.saveProblem(PROBLEM_ID, SOURCE_ID, "filecontent");
    }

    @Test
    public void shouldReturnHttpStatusOkWhenUserIsActive() throws ForbiddenResourceException, ResourceNotFoundException {
        when(userService.getCurrentUser()).thenReturn(candidate.getUser());
        when(candidateService.getCurrentCandidate()).thenReturn(candidate);
        GenericResponseDTO genericResponseDTO = problemService.saveProblem(PROBLEM_ID, SOURCE_ID, FILECONTENT);
        assertEquals(genericResponseDTO.getHttpStatus(), HttpStatus.OK);
        verify(s3Service).uploadFileContent(CANDIDATE_SUBMISSIONS_BUCKET,
                format("saved/%d/%d/%s",
                        candidate.getCandidateId(),
                        PROBLEM_ID,
                        SOURCE_PATH),
                FILECONTENT);
    }

    @Test
    public void shouldReturnBadRequestIfJavaCodeIsAbusive() throws ForbiddenResourceException, ResourceNotFoundException {
        when(userService.getCurrentUser()).thenReturn(candidate.getUser());
        when(candidateService.getCurrentCandidate()).thenReturn(candidate);
        GenericResponseDTO genericResponseDTO = problemService.saveProblem(PROBLEM_ID, SOURCE_ID, "System.exit(0)");

        assertEquals(genericResponseDTO.getHttpStatus(), HttpStatus.BAD_REQUEST);
        verify(s3Service, never()).uploadFileContent(CANDIDATE_SUBMISSIONS_BUCKET,
                format("saved/%d/%d/%s",
                        candidate.getCandidateId(),
                        PROBLEM_ID,
                        SOURCE_PATH),
                FILECONTENT);

    }

    @Test
    public void shouldReturnBadRequestIfCSharpCodeIsAbusive() throws ForbiddenResourceException, ResourceNotFoundException {
        Candidate cSharpCandidate = setUpCandidate("CSharp");
        when(candidateService.getCurrentCandidate()).thenReturn(cSharpCandidate);
        when(userService.getCurrentUser()).thenReturn(cSharpCandidate.getUser());
        GenericResponseDTO genericResponseDTO = problemService.saveProblem(PROBLEM_ID, SOURCE_ID, "using System.Web;");

        assertEquals(genericResponseDTO.getHttpStatus(), HttpStatus.BAD_REQUEST);
        verify(s3Service, never()).uploadFileContent(CANDIDATE_SUBMISSIONS_BUCKET,
                format("saved/%d/%d/%s",
                        cSharpCandidate.getCandidateId(),
                        PROBLEM_ID,
                        SOURCE_PATH),
                FILECONTENT);
    }

}


