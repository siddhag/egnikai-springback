package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.persistence.model.AttemptedProblem;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.persistence.model.Problem;
import com.thoughtworks.in.egnikai.persistence.repository.CandidateRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ScoreServiceTest {

    private ScoreService scoreService;

    @Mock
    private CandidateRepository candidateRepository;

    @Before
    public void setUp() {
        scoreService = new ScoreService(candidateRepository);
    }

    @Test
    public void shouldCalculateScoreOfAGivenCandidate() {
        Candidate candidate = getCandidate();

        scoreService.calculateScore(candidate);

        assertEquals(10, candidate.getScore());
        verify(candidateRepository).save(candidate);
    }


    @Test
    public void shouldCalculateScoreOfAGivenCandidate1() {

        Candidate candidate = getAnotherCandidateWithTwoSolvedProblems();

        scoreService.calculateScore(candidate);

        assertEquals(20, candidate.getScore());
        verify(candidateRepository).save(candidate);
    }


    private Candidate getCandidate() {
        Candidate candidate = new Candidate();
        candidate.setCandidateId(1);
        candidate.setAttemptedProblems(Arrays.asList(getAttemptedProblem()));

        return candidate;
    }

    private Candidate getAnotherCandidateWithTwoSolvedProblems() {
        Candidate candidate = new Candidate();
        candidate.setCandidateId(1);
        candidate.setAttemptedProblems(Arrays.asList(getAttemptedProblem(), getAttemptedProblem()));

        return candidate;
    }


    private AttemptedProblem getAttemptedProblem() {
        Problem problem = new Problem();
        problem.setProblemId(1);
        problem.setCredit(10);

        AttemptedProblem attemptedProblem = new AttemptedProblem();
        attemptedProblem.setPassed(true);
        attemptedProblem.setProblem(problem);

        return attemptedProblem;
    }
}