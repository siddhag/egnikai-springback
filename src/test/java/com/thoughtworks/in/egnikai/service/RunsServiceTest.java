package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.dto.SourceDTO;
import com.thoughtworks.in.egnikai.dto.TestDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.persistence.model.Operation;
import com.thoughtworks.in.egnikai.persistence.model.OperationName;
import com.thoughtworks.in.egnikai.persistence.model.OperationStatus;
import com.thoughtworks.in.egnikai.util.FileUtilService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.thoughtworks.in.egnikai.testHelpers.CandidateTestHelper.setUpCandidate;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class RunsServiceTest {

    public static final int OPERATION_ID = 1;
    public static final String CANDIDATE_SUBMISSIONS_BUCKET = "CANDIDATE_SUBMISSIONS_BUCKET";
    public static final String[] CONTENT_OF_SOURCE_CODE_ARRAY = new String[]{"1", "2"};
    @Mock
    private OperationService operationService;
    @Mock
    private S3Service s3Service;
    @Mock
    private SourceCodeService sourceCodeService;
    @Mock
    private TestCaseService testCaseService;
    @Mock
    private FileUtilService fileUtilService;
    @Mock
    private File tgzFile;

    private Path tempDirectory;
    @Captor
    private ArgumentCaptor<Operation> operationArgumentCaptor;

    private RunsService runsService;
    private Candidate candidate;

    @Before
    public void setup() throws IOException {
        candidate = setUpCandidate("java");
        doAnswer(invocationOnMock -> {
            Operation operation = invocationOnMock.getArgumentAt(0, Operation.class);
            operation.setOperationId(OPERATION_ID);
            return null;
        }).when(operationService).saveOperation(any(Operation.class));

        runsService = new RunsService(operationService,
                s3Service,
                sourceCodeService,
                testCaseService,
                fileUtilService,
                CANDIDATE_SUBMISSIONS_BUCKET);

        tempDirectory = Files.createTempDirectory("prefix");
        SourceDTO sourceDTO1 = new SourceDTO();
        sourceDTO1.setSourceCode(CONTENT_OF_SOURCE_CODE_ARRAY[0]);
        SourceDTO sourceDTO2 = new SourceDTO();
        sourceDTO2.setSourceCode(CONTENT_OF_SOURCE_CODE_ARRAY[1]);
        when(sourceCodeService.getSourceCode(any(), anyLong(), anyLong()))
                .thenReturn(sourceDTO1)
                .thenReturn(sourceDTO2);
        when(fileUtilService.createTempDirectory()).thenReturn(tempDirectory);

        TestDTO testDTO1 = new TestDTO();
        testDTO1.setTestCode(CONTENT_OF_SOURCE_CODE_ARRAY[0]);
        TestDTO testDTO2 = new TestDTO();
        testDTO2.setTestCode(CONTENT_OF_SOURCE_CODE_ARRAY[1]);
        when(testCaseService.getTest(any(), anyLong(), anyLong(), eq(true)))
                .thenReturn(testDTO1)
                .thenReturn(testDTO2);
        when(fileUtilService.createTempDirectory()).thenReturn(tempDirectory);

        when(fileUtilService.createTarGzip(any(Path.class))).thenReturn(tgzFile);
    }

    @Test
    public void shouldSaveSourceCodeInTempDirectory() throws ResourceNotFoundException, ForbiddenResourceException {
        runsService.scheduleRunProblemForCandidate(candidate, candidate.getChallenge().getProblems().get(0));

        InOrder inOrder = inOrder(fileUtilService);
        inOrder.verify(fileUtilService).createTempDirectory();
        String firstFilePath = candidate.getChallenge().getProblems().get(0).getSourceCodes().get(0).getFilePath();
        String secondFilePath = candidate.getChallenge().getProblems().get(0).getSourceCodes().get(1).getFilePath();
        inOrder.verify(fileUtilService).writeToFile(tempDirectory.resolve(firstFilePath).toFile(), CONTENT_OF_SOURCE_CODE_ARRAY[0]);
        inOrder.verify(fileUtilService).writeToFile(tempDirectory.resolve(secondFilePath).toFile(), CONTENT_OF_SOURCE_CODE_ARRAY[1]);
    }

    @Test
    public void shouldSaveTestCodeInTempDirectory() throws ResourceNotFoundException, ForbiddenResourceException {
        runsService.scheduleRunProblemForCandidate(candidate, candidate.getChallenge().getProblems().get(0));

        InOrder inOrder = inOrder(fileUtilService);
        inOrder.verify(fileUtilService).createTempDirectory();
        String firstFilePath = candidate.getChallenge().getProblems().get(0).getTestCases().get(0).getFilePath();
        String secondFilePath = candidate.getChallenge().getProblems().get(0).getTestCases().get(1).getFilePath();
        inOrder.verify(fileUtilService).writeToFile(tempDirectory.resolve(firstFilePath).toFile(), CONTENT_OF_SOURCE_CODE_ARRAY[0]);
        inOrder.verify(fileUtilService).writeToFile(tempDirectory.resolve(secondFilePath).toFile(), CONTENT_OF_SOURCE_CODE_ARRAY[1]);
    }

    @Test
    public void shouldTgzSourceAndTestCode() {
        runsService.scheduleRunProblemForCandidate(candidate, candidate.getChallenge().getProblems().get(0));
        verify(fileUtilService).createTarGzip(tempDirectory);
    }

    @Test
    public void shouldUploadTgzToS3() {
        runsService.scheduleRunProblemForCandidate(candidate, candidate.getChallenge().getProblems().get(0));
        verify(s3Service).uploadFile(CANDIDATE_SUBMISSIONS_BUCKET,
                format("runs/%s/%d/%d.tgz",
                        candidate.getChallenge().getPlatformName(),
                        candidate.getCandidateId(),
                        OPERATION_ID),
                tgzFile);
    }

    @Test
    public void shouldSaveOperationAgainSayingItIsInProgress()  {
        runsService.scheduleRunProblemForCandidate(candidate, candidate.getChallenge().getProblems().get(0));

        verify(operationService, times(2)).saveOperation(operationArgumentCaptor.capture());

        Operation value = operationArgumentCaptor.getAllValues().get(1);
        assertThat(value.getOperationName()).isEqualTo(OperationName.RUN.toString());
        assertThat(value.getOperationStatus()).isEqualTo(OperationStatus.IN_PROGRESS.toString());
        assertThat(value.getCandidate()).isSameAs(candidate);
        assertThat(value.getProblem()).isSameAs(candidate.getChallenge().getProblems().get(0));
    }


}