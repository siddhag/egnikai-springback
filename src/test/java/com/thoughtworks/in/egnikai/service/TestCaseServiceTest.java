package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.dto.TestDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.persistence.model.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import java.io.IOException;
import java.util.Optional;

import static com.thoughtworks.in.egnikai.testHelpers.CandidateTestHelper.*;
import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCaseServiceTest {
    @Mock
    private UserService userService;
    @Mock
    private CandidateService candidateService;
    @Mock
    private S3Service s3Service;

    private TestCaseService testCaseService;

    private ModelMapper mapper = new ModelMapper();
    private Candidate candidate;

    @Before
    public void setup() {
        testCaseService = new TestCaseService(
                userService,
                s3Service,
                CODE_PROBLEMS_BUCKET);
        candidate = setUpCandidate("java");

        when(candidateService.getCurrentCandidate()).thenReturn(candidate);
        when(userService.getCurrentUser()).thenReturn(candidate.getUser());
        when(userService.isCandidate(candidate.getUser())).thenReturn(true);

        when(s3Service.getFileContent(CODE_PROBLEMS_BUCKET,
                format("%s/%s", CHALLENGE_BASE_S_3_KEY, TEST_PATH)))
                .thenReturn(Optional.of("this is the file content"));
    }

    @Test
    public void shouldGetTestFileForProblemFromSubmissionBucket() {
        TestCase testCase2 = candidate.getChallenge().getProblems().get(0).getTestCases().get(1);

        TestDTO expectedTestDTO = new TestDTO();
        mapper.map(testCase2, expectedTestDTO);
        expectedTestDTO.setTestCode("this is the file content");

        TestDTO testDTO = testCaseService.getTest(candidate, PROBLEM_ID, PUBLIC_TEST_ID);

        assertEquals(expectedTestDTO, testDTO);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowTestNotFoundExceptionWhenTestIdNotFound() {
        int invalidTestId = 999;
        testCaseService.getTest(candidate, PROBLEM_ID, invalidTestId);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowTestNotFoundExceptionForPrivateTest() {
        testCaseService.getTest(candidate, PROBLEM_ID, PRIVATE_TEST_ID);
    }

    @Test
    public void shouldGetTestFileForProblemFromSubmissionBucketEvenIfHidden() {
        TestCase testCase1 = candidate.getChallenge().getProblems().get(0).getTestCases().get(0);

        TestDTO expectedTestDTO = new TestDTO();
        mapper.map(testCase1, expectedTestDTO);
        expectedTestDTO.setTestCode("this is the file content");

        when(s3Service.getFileContent(eq(CODE_PROBLEMS_BUCKET),
                anyString()))
                .thenReturn(Optional.of("this is the file content"));

        TestDTO testDTO = testCaseService.getTest(candidate, PROBLEM_ID, PRIVATE_TEST_ID, true);

        assertEquals(expectedTestDTO, testDTO);
    }

}