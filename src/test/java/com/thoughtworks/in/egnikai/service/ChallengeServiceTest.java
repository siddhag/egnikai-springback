package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.dto.ChallengeBaseDTO;
import com.thoughtworks.in.egnikai.dto.ChallengeDTO;
import com.thoughtworks.in.egnikai.dto.ChallengeDetailDTO;
import com.thoughtworks.in.egnikai.dto.ProblemSummaryDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.persistence.model.Challenge;
import com.thoughtworks.in.egnikai.persistence.model.Problem;
import com.thoughtworks.in.egnikai.persistence.repository.ChallengeRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ChallengeServiceTest {

    @Mock
    private ChallengeRepository challengeRepository;

    @Mock
    private CandidateService candidateService;

    private ChallengeService challengeService;

    @Before
    public void setUp() {
        challengeService = new ChallengeService(challengeRepository, candidateService);
    }

    @Test
    public void shouldReturnListOfProblemsGivenChallengeId() {
        Challenge challenge = new Challenge();
        challenge.setProblems(Arrays.asList(new Problem(), new Problem()));
        when(challengeRepository.findOne((long) 1)).thenReturn(challenge);

        List<ProblemSummaryDTO> problemList = challengeService.getProblems(1);

        assertEquals(problemList.size(), 2);
    }


    @Test
    public void shouldReturnListOfChallenges() {
        Challenge challenge = getChallenge();
        List<Challenge> expectedChallenges = Arrays.asList(challenge);
        when(challengeRepository.findAll()).thenReturn(expectedChallenges);

        List<ChallengeBaseDTO> actualChallenges = challengeService.getChallenges();

        assertEquals(1, actualChallenges.size());
        assertEquals(expectedChallenges.get(0).getChallengeTitle(), actualChallenges.get(0).getChallengeTitle());
        assertEquals(expectedChallenges.get(0).getChallengeId(), actualChallenges.get(0).getChallengeId());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowResourceNotFoundExceptionWhenChallengeNotFound() throws ForbiddenResourceException, ResourceNotFoundException {
        Candidate candidate = new Candidate();
        candidate.setCandidateId(1);

        when(candidateService.getCurrentCandidate()).thenReturn(candidate);

        challengeService.getChallenge();
    }

    @Test
    public void shouldReturnChallengeDTO() throws ForbiddenResourceException, ResourceNotFoundException {
        Challenge challenge = this.getChallenge();
        Candidate candidate = new Candidate();
        candidate.setCandidateId(1);
        candidate.setChallenge(challenge);
        when(candidateService.getCurrentCandidate()).thenReturn(candidate);

        ChallengeDTO actualChallenge = challengeService.getChallenge();

        assertEquals(actualChallenge.getChallengeTitle(), challenge.getChallengeTitle());
        assertEquals(actualChallenge.getDescription(), challenge.getDescription());
        assertEquals(actualChallenge.getProblemSummaryDTOs().size(), challenge.getProblems().size());
    }

    public Challenge getChallenge() {
        Challenge challenge = new Challenge();
        challenge.setChallengeId(1);
        challenge.setDescription("description");
        challenge.setChallengeTitle("title");
        challenge.setProblems(Arrays.asList());
        return challenge;
    }
}