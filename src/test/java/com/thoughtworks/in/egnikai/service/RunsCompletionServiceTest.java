package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.persistence.model.AttemptedProblem;
import com.thoughtworks.in.egnikai.persistence.model.Operation;
import com.thoughtworks.in.egnikai.persistence.model.OperationName;
import com.thoughtworks.in.egnikai.persistence.model.OperationStatus;
import com.thoughtworks.in.egnikai.persistence.repository.OperationRepository;
import com.thoughtworks.in.egnikai.testHelpers.CandidateTestHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RunsCompletionServiceTest {

    public static final int OPERATION_ID = 1;
    @Mock
    private S3Service s3Service;
    private static final String candidateCodeSubmissionsBucket = "code_bucket";
    @Mock
    private OperationRepository operationRepository;
    @Mock
    private ScoreService scoreService;
    @Mock
    private AttemptedProblemService attemptedProblemService;
    @Captor
    private ArgumentCaptor<AttemptedProblem> attemptedProblemArgumentCaptor;

    private Operation operation;
    private RunsCompletionService runsCompletionService;

    private static final String FAILED_TEST_RESULT = "{\"success\":false,\"errorType\":\"TESTS_FAILED\",\"message\":\"There were test failures\\nshouldDetectThreeAsPrimeNumber(P1.PrimeCheckerTest)\\n\"}";
    private static final String SUCCESS_RESULT = "{\"success\":true}";

    @Before
    public void setup() {
        runsCompletionService = new RunsCompletionService(s3Service,
                candidateCodeSubmissionsBucket,
                operationRepository,
          attemptedProblemService, scoreService);
        operation = new Operation();
        operation.setOperationId(OPERATION_ID);
        operation.setCandidate(CandidateTestHelper.setUpCandidate("java"));
        operation.setOperationName(OperationName.RUN.toString());
        operation.setProblem(operation.getCandidate().getChallenge().getProblems().get(0));
    }

    @Test
    public void shouldNotUpdateOperationOrScoreIfNoResultJsonPresent() {
        when(s3Service.getFileContent(anyString(), anyString())).thenReturn(Optional.empty());

        runsCompletionService.checkAndProcessRunCompletion(operation);

        verifyZeroInteractions(operationRepository);
        verifyZeroInteractions(scoreService);
    }

    @Test
    public void shouldUpdateCandidateScoreAndRunOperationAsFailureIfResultWasAFailureinS3() {
        when(s3Service.getFileContent(candidateCodeSubmissionsBucket,
                format("runs/%s/%d/%d.json",
                        operation.getCandidate().getChallenge().getPlatformName(),
                        operation.getCandidate().getCandidateId(),
                        operation.getOperationId())))
                .thenReturn(Optional.of(FAILED_TEST_RESULT));

        runsCompletionService.checkAndProcessRunCompletion(operation);

        verify(operationRepository).save(operation);
        verify(attemptedProblemService).updateAttemptedProblemInDB(operation);
        verify(scoreService).calculateScore(operation.getCandidate());

        assertThat(operation.getErrorInformation()).isEqualTo(FAILED_TEST_RESULT);
        assertThat(operation.getOperationStatus()).isEqualTo(OperationStatus.FAILED.toString());
    }

    @Test
    public void shouldUpdateRunOperationAsSuccessIfResultWasASuccessInS3() {
        when(s3Service.getFileContent(candidateCodeSubmissionsBucket,
                format("runs/%s/%d/%d.json",
                        operation.getCandidate().getChallenge().getPlatformName(),
                        operation.getCandidate().getCandidateId(),
                        operation.getOperationId())))
                .thenReturn(Optional.of(SUCCESS_RESULT));

        runsCompletionService.checkAndProcessRunCompletion(operation);

        verify(operationRepository).save(operation);
        verify(attemptedProblemService).updateAttemptedProblemInDB(operation);
        verify(scoreService).calculateScore(operation.getCandidate());

        assertThat(operation.getErrorInformation()).isEqualTo(SUCCESS_RESULT);
        assertThat(operation.getOperationStatus()).isEqualTo(OperationStatus.COMPLETED.toString());
    }




}