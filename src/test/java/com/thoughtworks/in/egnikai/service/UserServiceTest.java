package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.Candidate;
import com.thoughtworks.in.egnikai.persistence.repository.UserRepository;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.context.SecurityContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityContextLogoutHandler.class)
public class UserServiceTest {

    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private Authentication authentication;

    @Mock
    private User springSecurityUser;

    @Mock
    private SecurityContext securityContext;

    @Before
    public void setUp() throws Exception {
        this.userService = new UserService(userRepository);
    }

    @Test
    public void shouldReturnTrueForCandidate() throws ResourceNotFoundException {
        SecurityContextHolder.setContext(securityContext);

        when(securityContext.getAuthentication()).thenReturn(authentication);

        when(authentication.getPrincipal()).thenReturn(springSecurityUser);

        when(userRepository.findOne(springSecurityUser.getUsername())).thenReturn(getUser("candidate"));

        assertTrue(userService.isCandidate(userService.getCurrentUser()));
    }

    @Test
    public void shouldReturnFalseForNonCandidateUser() throws ResourceNotFoundException {
        SecurityContextHolder.setContext(securityContext);

        when(securityContext.getAuthentication()).thenReturn(authentication);

        when(authentication.getPrincipal()).thenReturn(springSecurityUser);

        when(userRepository.findOne(springSecurityUser.getUsername())).thenReturn(getUser("admin"));

        assertFalse(userService.isCandidate(userService.getCurrentUser()));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowExceptionWhenUserNotFound() throws ResourceNotFoundException {
        SecurityContextHolder.setContext(securityContext);

        when(securityContext.getAuthentication()).thenReturn(authentication);

        when(authentication.getPrincipal()).thenReturn(springSecurityUser);

        when(userRepository.findOne(springSecurityUser.getUsername())).thenReturn(null);

        userService.getCurrentUser();

    }

    @Test
    public void shouldReturnTrueForAdmin() throws ResourceNotFoundException {
        SecurityContextHolder.setContext(securityContext);

        when(securityContext.getAuthentication()).thenReturn(authentication);

        when(authentication.getPrincipal()).thenReturn(springSecurityUser);

        when(userRepository.findOne(springSecurityUser.getUsername())).thenReturn(getUser("admin"));

        assertTrue(userService.isAdmin(userService.getCurrentUser()));
    }

    @Test
    public void isAdminshouldReturnFalseForNonAdmin() throws ResourceNotFoundException {
        SecurityContextHolder.setContext(securityContext);

        when(securityContext.getAuthentication()).thenReturn(authentication);

        when(authentication.getPrincipal()).thenReturn(springSecurityUser);

        when(userRepository.findOne(springSecurityUser.getUsername())).thenReturn(getUser("candidate"));

        assertFalse(userService.isAdmin(userService.getCurrentUser()));
    }

//    TODO: Need to verify securityContextLogoutHandler logout method call
    @Test
    public void logoutShouldLogoutTheUser() throws Exception {
        SecurityContextHolder.setContext(securityContext);

        SecurityContextLogoutHandler securityContextLogoutHandler = PowerMockito.mock(SecurityContextLogoutHandler.class);
        HttpServletRequest mockedRequest = mock(HttpServletRequest.class);
        HttpServletResponse mockedResponse = mock(HttpServletResponse.class);

        PowerMockito.whenNew(SecurityContextLogoutHandler.class).withNoArguments().thenReturn(securityContextLogoutHandler);
        userService.logOut(mockedRequest,mockedResponse,authentication);
        PowerMockito.verifyNew(SecurityContextLogoutHandler.class);

    }

    @Test
    public void shouldSaveCandidate(){
        List<com.thoughtworks.in.egnikai.persistence.model.User> users = asList(getUser("CANDIDATE"));

        userService.saveUsers(users);
        verify(userRepository).save(users);
    }

    private com.thoughtworks.in.egnikai.persistence.model.User getUser(String role){
        com.thoughtworks.in.egnikai.persistence.model.User user = new com.thoughtworks.in.egnikai.persistence.model.User();
        user.setUserId("userId");
        user.setEnabled(true);
        user.setPassword("password");
        user.setRole(role);
        user.setUserName("userName");

        return user;
    }

}