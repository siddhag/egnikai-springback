package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.dto.OperationStatusDTO;
import com.thoughtworks.in.egnikai.persistence.model.Operation;
import com.thoughtworks.in.egnikai.persistence.repository.OperationRepository;
import com.thoughtworks.in.egnikai.util.ErrorParserUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

@Ignore
public class OperationServiceTest {
//    @InjectMocks
//    private OperationService operationService;
//    @Mock
//    private OperationRepository operationRepository;
//
//    private ErrorParserUtil errorParserUtil;
//
//    @Before
//    public void setUp()  {
//        PowerMockito.mockStatic(ErrorParserUtil.class);
//
////        errorParserUtil = Mockito   (ErrorParserUtil.class);
//    }
//
//    @Test
//    public void shouldReturnOperationDetailsForGivenOperationIds() {
//        Operation operation = new Operation();
//        operation.setOperationId(1);
//        operation.setOperationName("RUN");
//        operation.setOperationStatus("COMPLETED");
//        OperationStatusDTO operationStatusDTO1 = OperationStatusDTO.createOperationStatusDTO(operation);
//        PowerMockito.when(operationRepository.findByOperationId(operation.getOperationId())).thenReturn(operation);
//        List<OperationStatusDTO> actualOperationDetails = operationService.getOperationDetails(Arrays.asList(operation.getOperationId()));
//
//        Assert.assertEquals(operationStatusDTO1.getOperationName(),actualOperationDetails.get(0).getOperationName());
//
//    }
//
//    @Test
//    public void shouldInvokeErrorParserWhenRunOperationFailsForGivenOperationIds() {
//        Operation operation = new Operation();
//        operation.setOperationId(1);
//        operation.setOperationName("RUN");
//        operation.setOperationStatus("FAILED");
//        operation.setErrorInformation("something went wrong");
//        StringBuilder stringBuilder = new StringBuilder(operation.getErrorInformation());
//        OperationStatusDTO operationStatusDTO1 = OperationStatusDTO.createOperationStatusDTO(operation);
//
//        PowerMockito.when(operationRepository.findByOperationId(operation.getOperationId())).thenReturn(operation);
//        when(ErrorParserUtil.javaFailedTestCasesErrorParser(operation.getErrorInformation(),"")).thenReturn(stringBuilder);
//
//        List<OperationStatusDTO> actualOperationDetails = operationService.getOperationDetails(Arrays.asList(operation.getOperationId()));
//
//        Assert.assertEquals(operationStatusDTO1.getOperationName(),actualOperationDetails.get(0).getOperationName());
//        Assert.assertEquals(operationStatusDTO1.getErrorInformation(),actualOperationDetails.get(0).getErrorInformation());
//    }
//
//    @Test
//    public void shouldNotInvokeErrorParserWhenOtherThanRunOperationForGivenOperationIds() {
//        Operation operation = new Operation();
//        operation.setOperationId(1);
//        operation.setOperationName("CLONE");
//        operation.setOperationStatus("FAILED");
//        operation.setErrorInformation("something went wrong");
//        StringBuilder stringBuilder = new StringBuilder(operation.getErrorInformation());
//        OperationStatusDTO operationStatusDTO1 = OperationStatusDTO.createOperationStatusDTO(operation);
//
//        PowerMockito.when(operationRepository.findByOperationId(operation.getOperationId())).thenReturn(operation);
//
//        List<OperationStatusDTO> actualOperationDetails = operationService.getOperationDetails(Arrays.asList(operation.getOperationId()));
//
//        PowerMockito.verifyNoMoreInteractions(ErrorParserUtil.class);
//        Assert.assertEquals(operationStatusDTO1.getOperationName(),actualOperationDetails.get(0).getOperationName());
//        Assert.assertEquals(operationStatusDTO1.getErrorInformation(),actualOperationDetails.get(0).getErrorInformation());
//
//    }

}