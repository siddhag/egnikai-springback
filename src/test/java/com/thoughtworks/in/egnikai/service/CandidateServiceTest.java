package com.thoughtworks.in.egnikai.service;

import com.thoughtworks.in.egnikai.dto.AttemptedProblemDTO;
import com.thoughtworks.in.egnikai.dto.OperationIdDTO;
import com.thoughtworks.in.egnikai.dto.OperationStatusDTO;
import com.thoughtworks.in.egnikai.exception.ForbiddenResourceException;
import com.thoughtworks.in.egnikai.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikai.persistence.model.*;
import com.thoughtworks.in.egnikai.persistence.repository.CandidateRepository;
import com.thoughtworks.in.egnikai.testHelpers.CandidateTestHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CandidateServiceTest {
    private CandidateService candidateService;

    @Mock
    private CandidateRepository candidateRepository;

    @Mock
    private UserService userService;

    @Mock
    private RunsService runsService;

    @Mock
    private OperationService operationService;

    Candidate candidate;

    @Before
    public void setUp() {
        candidateService = new CandidateService(candidateRepository, userService, operationService, runsService);
        candidate = CandidateTestHelper.setUpCandidate("java");
    }

    @Test(expected = ForbiddenResourceException.class)
    public void shouldThrowRestrictedAccessException() throws ForbiddenResourceException, ResourceNotFoundException {
        when(userService.isCandidate(any())).thenReturn(false);

        candidateService.getCurrentCandidate();

    }

    @Test
    public void shouldReturnCandidateGivenUser() throws ForbiddenResourceException, ResourceNotFoundException {
        when(userService.isCandidate(any())).thenReturn(true);

        when(userService.getCurrentUser()).thenReturn(getOneUser());

        when(candidateRepository.findOneByUser_UserIdAndIsActive(getOneUser().getUserId(), true)).thenReturn(new Candidate());

        assertThat(candidateService.getCurrentCandidate()).isEqualToComparingFieldByField(new Candidate());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowExceptionIfCandidateNotFoundInDB() throws ForbiddenResourceException, ResourceNotFoundException {
        when(userService.isCandidate(any())).thenReturn(true);

        when(userService.getCurrentUser()).thenReturn(getOneUser());

        when(candidateRepository.findOneByUser_UserIdAndIsActive(getOneUser().getUserId(), true)).thenReturn(null);

        candidateService.getCurrentCandidate();
    }

    @Test
    public void shouldGetCandidateCloneOperationDetails() throws ResourceNotFoundException, ForbiddenResourceException {
        when(userService.isCandidate(any())).thenReturn(true);

        Candidate candidate = getCandidate();
        when(userService.getCurrentUser()).thenReturn(getOneUser());
        when(candidateRepository.findOneByUser_UserIdAndIsActive(anyString(), anyBoolean())).thenReturn(getCandidate());

        Operation operation = new Operation();
        operation.setOperationId(1);
        operation.setCandidate(candidate);
        operation.setOperationName(OperationName.CLONE.name());
        operation.setOperationStatus(OperationStatus.NOT_STARTED.name());
        when(operationService.findByCandidateId(candidate.getCandidateId())).thenReturn(asList(operation));

        OperationStatusDTO expectedOperationStatusDTO = OperationStatusDTO.createOperationStatusDTO(operation);

        OperationStatusDTO operationStatusDTO = candidateService.getCloningOperationDetails();

        assertEquals(expectedOperationStatusDTO, operationStatusDTO);
        verify(operationService).findByCandidateId(candidate.getCandidateId());
    }


    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowExceptionWhenOperationNotFoundForCandidate()
            throws ResourceNotFoundException, ForbiddenResourceException {
        when(userService.isCandidate(any())).thenReturn(true);
        when(userService.getCurrentUser()).thenReturn(getOneUser());

        Candidate candidate = new Candidate();
        candidate.setCandidateId(1);
        when(operationService.findByCandidateId(candidate.getCandidateId())).thenReturn(null);

        when(candidateRepository.findOneByUser_UserIdAndIsActive(getOneUser().getUserId(), true)).thenReturn(candidate);

        candidateService.getCloningOperationDetails();
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowExceptionWhenOperationNotClone() throws ResourceNotFoundException, ForbiddenResourceException {
        when(userService.isCandidate(any())).thenReturn(true);
        when(userService.getCurrentUser()).thenReturn(getOneUser());
        when(candidateRepository.findOneByUser_UserIdAndIsActive(userService.getCurrentUser().getUserId(), true)).thenReturn(getCandidate());
        Candidate candidate = getCandidate();

        Operation operation = new Operation();
        operation.setOperationId(1);
        operation.setCandidate(candidate);
        operation.setOperationName(OperationName.RUN.name());
        operation.setOperationStatus(OperationStatus.COMPLETED.name());

        when(operationService.findByCandidateId(candidate.getCandidateId())).thenReturn(asList(operation));

        candidateService.getCloningOperationDetails();
    }


    public User getOneUser() {
        User user = new User();
        user.setUserId("userId");
        user.setEnabled(true);
        user.setPassword("password");
        user.setRole("user");
        user.setUserName("userName");

        return user;
    }


    @Test
    public void shouldReturnSuccessMsgForValidProblemId() throws ResourceNotFoundException {
        when(userService.isCandidate(any())).thenReturn(true);
        when(userService.getCurrentUser()).thenReturn(getOneUser());
        when(candidateRepository.findOneByUser_UserIdAndIsActive(anyString(), anyBoolean())).thenReturn(candidate);
        when(runsService.scheduleRunProblemForCandidate(candidate, candidate.getChallenge().getProblems().get(0))).thenReturn(new Operation());
        OperationIdDTO actualResponse = candidateService.runProblem(1);

        assertEquals(actualResponse.getOperationId(), 0);

    }

    @Test
    public void shouldCreateOperationWithRunOperationNameWhenRunProblemIsCalled() throws ResourceNotFoundException {
        when(userService.isCandidate(any())).thenReturn(true);
        when(userService.getCurrentUser()).thenReturn(getOneUser());
        when(candidateRepository.findOneByUser_UserIdAndIsActive(anyString(), anyBoolean())).thenReturn(candidate);

        Operation operation = new Operation();
        operation.setOperationId(1);
        operation.setCandidate(candidate);
        operation.setOperationName(OperationName.RUN.name());
        operation.setOperationStatus(OperationStatus.NOT_STARTED.name());

        when(runsService.scheduleRunProblemForCandidate(candidate, candidate.getChallenge().getProblems().get(0))).thenReturn(operation);
        when(operationService.findByCandidateId(candidate.getCandidateId())).thenReturn(asList(operation));

        candidateService.runProblem(1);
        assertEquals(operation.getOperationStatus(),OperationStatus.NOT_STARTED.name());
    }

    @Test
    public void shouldInsertOperationStatusAsNotStartedWhenRunRequestIsReceivedAndGetRunningDetails() throws ResourceNotFoundException, ForbiddenResourceException {
        when(userService.isCandidate(any())).thenReturn(true);
        when(userService.getCurrentUser()).thenReturn(getOneUser());
        when(candidateRepository.findOneByUser_UserIdAndIsActive(userService.getCurrentUser().getUserId(), true)).thenReturn(getCandidate());
        Candidate candidate = getCandidate();

        Operation operation = new Operation();
        operation.setOperationId(1);
        operation.setCandidate(candidate);
        operation.setOperationName(OperationName.RUN.name());
        operation.setOperationStatus(OperationStatus.NOT_STARTED.name());

        when(operationService.findByCandidateId(candidate.getCandidateId())).thenReturn(asList(operation));

        OperationStatusDTO expectedOperationStatusDTO = OperationStatusDTO.createOperationStatusDTO(operation);

        OperationStatusDTO operationStatusDTO = candidateService.getRunningOperationDetails();

        assertEquals(expectedOperationStatusDTO, operationStatusDTO);
    }

    @Test
    public void shouldSaveCandidate(){
        List<Candidate> candidatesToBeSaved = asList(new Candidate());
        candidateService.saveCandidates(candidatesToBeSaved);
        verify(candidateRepository).save(candidatesToBeSaved);
    }

    @Test
    public void shouldFetchAttemptedProblemDetails() throws ResourceNotFoundException, ForbiddenResourceException {
        when(userService.isCandidate(any())).thenReturn(true);
        when(userService.getCurrentUser()).thenReturn(getOneUser());

        List<AttemptedProblem> attemptedProblems = new ArrayList<>();
        AttemptedProblem attemptedProblem = new AttemptedProblem();
        attemptedProblem.setAttemptedProblemId(2);
        attemptedProblem.setPassed(true);
        attemptedProblem.setScore(10);
        Problem problem = new Problem();
        problem.setProblemId(1);
        attemptedProblem.setProblem(problem);


        attemptedProblems.add(attemptedProblem);
        Candidate candidate = getCandidate();
        candidate.setAttemptedProblems(attemptedProblems);

        when(candidateRepository.findOneByUser_UserIdAndIsActive(userService.getCurrentUser().getUserId(), true)).thenReturn(candidate);

        List<AttemptedProblemDTO> expectedAttemptedProblemDTOs = new ArrayList<>();
        expectedAttemptedProblemDTOs.add(AttemptedProblemDTO.createAttemptedProblemDTO(attemptedProblem));

        assertEquals(expectedAttemptedProblemDTOs, candidateService.getAttemptedProblems());
    }


    private Candidate getCandidate() {
        Problem problem = new Problem();
        problem.setProblemId(1);
        Challenge challenge = new Challenge();
        challenge.setProblems(asList(problem));
        Candidate candidate = new Candidate();
        candidate.setCandidateId(4);
        candidate.setChallenge(challenge);
        candidate.setActive(true);
        return candidate;
    }

    @Test
    public void shouldLockCandidate() throws ResourceNotFoundException,ForbiddenResourceException{
        User user=getOneUser();
        user.setRole("CANDIDATE");
        Candidate candidate=new Candidate();
        candidate.setCandidateId(5);
        candidate.setUser(user);
        candidate.setLocked(false);
        when(userService.getCurrentUser()).thenReturn(user);
        when(userService.isCandidate(any())).thenReturn(true);
        when(candidateRepository.findOneByUser_UserIdAndIsActive(anyString(), anyBoolean())).thenReturn(candidate);
        candidateService.lockCandidate();
        verify(candidateRepository).save(candidate);
    }
}