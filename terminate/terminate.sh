#!/usr/bin/env bash

if [ "$#" -ne 3 ]; then
    echo "usage sh terminate.sh <aws cli profile name> <securityKeyName> <db password> <environment> <aws default region>"
    exit 1
fi

profileName=$1
region=$2
environment=$3

function deleteSpringBackStackIfExists() {
    echo \$1 is $1
    aws cloudformation describe-stacks \
        --stack-name egnikai-springback-deployment-$1 \
        --profile ${profileName} \
        --region ${region}
    doesStackExist=$?
    if [ "$doesStackExist" -eq 0 ]; then
      echo "stack exists going to delete it now"
      aws cloudformation delete-stack \
        --stack-name egnikai-springback-deployment-$1 \
        --profile ${profileName} \
        --region ${region} || exit 1
      aws cloudformation wait stack-delete-complete \
        --stack-name egnikai-springback-deployment-$1 \
        --profile ${profileName} \
        --region ${region} || exit 1
    else
      echo "stack does not exist yet"
    fi
}


function deleteNonProdDBStackIfExists() {
    aws cloudformation describe-stacks \
        --stack-name egnikai-non-prod-mysql-db \
        --profile ${profileName} \
        --region ${region}
    doesStackExist=$?
    if [ "$doesStackExist" -eq 0 ]; then
      echo "stack exists going to delete it now"
      aws cloudformation delete-stack \
        --stack-name egnikai-non-prod-mysql-db \
        --profile ${profileName} \
        --region ${region} || exit 1
      aws cloudformation wait stack-delete-complete \
        --stack-name egnikai-non-prod-mysql-db \
        --profile ${profileName} \
        --region ${region} || exit 1
    else
      echo "stack does not exist yet"
    fi
}

deleteSpringBackStackIfExists ${environment}
deleteNonProdDBStackIfExists


