#!/usr/bin/env bash

secret=root
export EGNIKAI_DB_CONTAINER_NAME=egnikaidb
export EGNIKAI_DB_IMAGE_NAME=regsethu/egnikaidb-image:latest
export EGNIKAI_DB_NAME=egnikaidb

echo "Stopping existing Database containers..."
sudo docker stop ${EGNIKAI_DB_CONTAINER_NAME}

echo "Removing Database Container: $EGNIKAI_DB_CONTAINER_NAME..."
sudo docker rm ${EGNIKAI_DB_CONTAINER_NAME}
sudo docker rmi ${EGNIKAI_DB_IMAGE_NAME}

echo "Running the Database Container: $EGNIKAI_DB_CONTAINER_NAME..."
sudo docker run --name $EGNIKAI_DB_CONTAINER_NAME -e MYSQL_ROOT_PASSWORD=${secret} -d -p 3306:3306 ${EGNIKAI_DB_IMAGE_NAME}

