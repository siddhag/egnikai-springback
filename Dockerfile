FROM openjdk:8u171-jre-alpine3.8

WORKDIR /app

COPY target/egnikai.jar /app

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "egnikai.jar"]