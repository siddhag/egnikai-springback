# egnikai

egnikai is an interview automation tool built in-house here at ThoughtWorks!

This repository contains the backend REST API which encapsulates all major operations such as running testCases and scoring candidates.

### Tech Stack

* Java 8
* Springboot
* Docker
* MySQL
* Maven

### How To Run
You need to have Java 8 and Maven installed in your machine. Also, most of the development scripts are written for POSIX systems and we encourage you to use Linux.

```bash
mvn clean compile install
```

To run [egnikai](), use the following commands:

```bash
bin/setupdb-container.sh
```
Run the application providing the below program arguments
Provide AWS User Access Key and Secret Key as Program arguments 


```bash
--spring-back-user-access-key=<AWS USER ACCESS KEY> 
--spring-back-user-access-secret-key=<AWS SECRET KEY>
--spring.flyway.locations=classpath:db/migration/default,classpath:db/migration/environment/dev
```

Now you should be able to hook up to port 8080 from postman.



